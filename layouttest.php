<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Bootstrap Example</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="/css/bootstrap.css">
		<link rel="stylesheet" href="/css/mdb.css">
		<link rel="stylesheet" href="/css/common.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<script src="js/bootstrap.js"></script>
		<script src="js/mdb.min.css"></script>
	</head>
	<body>
		<div id="boardHeader">
			<img src="https://cdn.discordapp.com/attachments/415686309219401730/572946333275652096/unknown.png" style="max-width: 70%; max-height: 150px; padding-top: 10px; padding-left: 10px; padding-right: 10px;">

			<div id="headerNavigation">
				<div id="navigationContainer">
					<a class="navigationLinks">Commands</a>
					<a class="navigationLinks">Invite Bot</a>
				</div>
			</div>
		</div>
		<div style="height: 5px; background-color: #FFDA00"></div>
		<div class="jumbotron text-center">
			<h1>My First Bootstrap Page</h1>
			<p>Resize this responsive page to see the effect!</p> 
		</div>
		<div class="container">
			<div class="row">
				<div class="col-sm-4">
					<h3>Column 1</h3>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
					<p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris...</p>
				</div>
				<div class="col-sm-4">
					<h3>Column 2</h3>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
					<p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris...</p>
				</div>
				<div class="col-sm-4">
					<h3>Column 3</h3>        
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>
					<p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris...</p>
				</div>
			</div>
		</div>
	</body>
</html>
