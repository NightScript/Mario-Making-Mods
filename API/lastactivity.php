<?php

define('BLARG', 1);
require __DIR__.'/../lib/common.php';

$markdown = $_GET['markdown'];

$viewableforums = ForumsWithPermission('forum.viewforum');

// timestamp => data
$lastActivity = [];
$maxitems = 10;

$lastposts = Query(
    '	SELECT
							t.(title,forum,lastpostdate,lastpostid),
							u.(_userfields)
						FROM
							{threads} t
							LEFT JOIN {forums} f ON f.id=t.forum
							LEFT JOIN {users} u ON u.id=t.lastposter
						WHERE f.id IN ({0c}) AND f.offtopic=0
						ORDER BY t.lastpostdate DESC
						LIMIT {1u}', $viewableforums, $maxitems
);

while ($lp = Fetch($lastposts)) {
    $user = getDataPrefix($lp, 'u_');
    $tags = ParseThreadTags($lp['t_title']);

    $fmtdate = relativedate($lp['t_lastpostdate']);
    if ($markdown) {
        $lastActivity[$lp['t_lastpostdate']] = ['user' => $user['name'], 'title' => $tags[0], 'link' => 'https://mariomods.net/'.actionLink('post', $lp['t_lastpostid']), 'formattedDate' => strip_tags($fmtdate)];
    } else {
        $desc = UserLink($user).__(' posted in ').$tags[1].actionLinkTag($tags[0], 'post', $lp['t_lastpostid']);
        $lastActivity[$lp['t_lastpostdate']] = ['user' => UserLink($user), 'title' => $tags[0], 'link' => actionLink('post', $lp['t_lastpostid']), 'formattedDate' => strip_tags($fmtdate)];
    }
}

krsort($lastActivity);
$lastActivity = array_slice($lastActivity, 0, $maxitems);

echo json_encode($lastActivity);
