<?php

//One problem with this method is that it requires JS.
//TODO: Make a <noscript> option
$plusOne = ($acmlmboardLayout ? '<span class="postplusone">' : '<button class="btn btn-primary">');

if ($loguserid && $poster['id'] !== $loguserid) {
    $url = pageLink('plusone', ['id' => $post['id']], 'key='.$loguser['token']);

    $vote = $db->row('postplusones', ['post' => $post['id'], 'user' => $loguserid]);
    $icon = 'star';
    if (!$vote) {
        $icon .= '-o';
    }

    $plusOne .= "<span onclick=\"$(this.parentElement).load('$url'); return false;\"><span class=\"fa fa-$icon\"></span></span> ";
} elseif (!$loguserid) {
    $plusOne .= '<span class="fa fa-star-o" onclick="alert(\'You need to sign in to star posts\')"></span> ';
} elseif ($poster['id'] == $loguserid) {
    $plusOne .= '<span class="fa fa-star-o" onclick="alert(\'You may not star your own posts\')"></span> ';
}

$plusOne .= $post['postplusones'];
$plusOne .= ($acmlmboardLayout ? '</span>' : '</button>');

$extraLinks[] = $plusOne;
