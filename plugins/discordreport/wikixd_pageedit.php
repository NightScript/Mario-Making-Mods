<?php

$thename = $loguser['name'];
if ($loguser['displayname']) {
    $thename = $loguser['displayname'];
}

$link = getServerDomainNoSlash().pageLink('wikiPage', ['page' => $page['id']]);

if ($page['new'] == 2) {
    GeneralReport('New wiki page: '.url2title($page['id'])." created by {$thename} -- {$link}");
} else {
    GeneralReport('Wiki page '.url2title($page['id'])." edited by {$thename} (rev. {$rev}) -- {$link}");
}
