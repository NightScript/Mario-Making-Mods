<?php
if (!defined('BLARG')) die();
if ($isHidden)         return;


$thename = ($loguser['displayname'] ? $loguser['displayname'] : $loguser['name']);

$postlink = getServerDomainNoSlash().pageLink('post', ['id' => $pid]);
$userlink = getServerDomainNoSlash().pageLink('profile', ['id' => $loguser['id'], 'name' => slugify($thename)]);
$forumlink = getServerDomainNoSlash().pageLink('forum', ['id' => $forum['id'], 'name' => slugify($forum['title'])]);

$desc = parseBBCode($_POST['text']);
$desc = strip_html_tags($desc);
$desc = preg_replace('@\[.*?\]@s', '', $desc);
$desc = preg_replace('@\s+@', ' ', $desc);

$desc = explode(' ', $desc);
if (count($desc) > 30) {
    $desc = array_slice($desc, 0, 30);
    $desc[29] .= '...';
}
$desc = implode(' ', $desc);

if (Settings::pluginGet('embeds')) {
	$webhook = [];

	$webhook['username'] = Settings::pluginGet('username');
	$webhook['avatar_url'] = Settings::pluginGet('image');

	$webhook['embeds'] = [[
		'color' => '#00FF00',
		
		'title' => 'New reply: '.$thread['title'],
		'url' => $link,
		
		'description' => custom_echo($desc, 2000),
		
		'fields' => [
			['name' => 'Author', 'data' => "[$thename]($userlink) (#".$loguserid.')', 'inline' => true],
			['name' => 'Forum', 'data' => '['.htmlspecialchars($forum['title'])."]($forumlink) (#".$forum['id'].')', 'inline' => true],
		],
		
		'footer' => [
			'text' => Settings::get('boardname'),
			'icon_url' => getServerDomain().'apple-touch-icon.png'
		]
	]];

	PostToDiscord($webhook);
} else {
	DiscordTextReport("New reply by $thename: ".$thread['title'].' ('.$forum['title'].") -- $link");
}

function custom_echo($x, $length) {
	if(strlen($x) <= $length)	return $x;
	else						return substr($x, 0, $length).'...';
}