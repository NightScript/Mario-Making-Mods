<?php

if (!defined('BLARG')) {
    die();
}

$sidebarshow = true;
$showconsoles = false;

$title = __('Edit thread');

if (isset($_REQUEST['action']) && $loguser['token'] != $_REQUEST['key']) {
    Kill(__('No.'));
}

if (!$loguserid) { //Not logged in?
    Kill(__('You must be logged in to edit threads.'));
}

if (isset($_POST['id'])) {
    $_GET['id'] = $_POST['id'];
}

if (!isset($_GET['id'])) {
    Kill(__('Thread ID unspecified.'));
}

$tid = (int) $_GET['id'];

$thread = $db->row('threads', ['id' => $tid]);
if (!$thread) {
    Kill(__('Unknown thread ID.'));
}

$forum = $db->row('forums', ['id' => $thread['forum']]);
if ($forum && HasPermission('forum.viewforum', $forum['id'])) {
    $canClose = HasPermission('mod.closethreads', $thread['forum']);
    $canStick = HasPermission('mod.stickthreads', $thread['forum']);
    $canMove = HasPermission('mod.movethreads', $thread['forum']);
    $isclosed = $thread['closed'] && !$canClose;
    $canRename = ($thread['user'] == $loguserid && HasPermission('user.renameownthreads') && !$isclosed) || HasPermission('mod.renamethreads', $thread['forum']);
    $canMod = $canRename || $canClose || $canStick || $canMove || HasPermission('mod.trashthreads', $thread['forum']) || HasPermission('mod.deletethreads', $thread['forum']);

    if (!$canMod) {
        Kill(__('You are not allowed to edit this thread.'));
    }
} else {
    Kill(__('Unknown forum ID.'));
}

$isHidden = !HasPermission('forum.viewforum', $forum['id'], true);

$tags = ParseThreadTags($thread['title']);
$urlname = $isHidden ? '' : $tags[0];

if ($forum['id'] == 3) {
    $depoturl = 'depot/hacks';
    MakeCrumbs([pageLink('depot') => 'Super Mario Maker Hacking Depot', pageLink('entry', ['id' => $tid, 'name' => $urlname]) => $tags[0], actionLink('editdepotentry', $tid, '', $urlname) => __('Edit Depot Entry')]);
} elseif ($forum['id'] == 7) {
    $depoturl = 'depot/level';
    MakeCrumbs([pageLink('leveldepot') => 'Super Mario Maker Level Depot', pageLink('entry', ['id' => $tid, 'name' => $urlname]) => $tags[0], actionLink('editdepotentry', $tid, '', $urlname) => __('Edit Depot Entry')]);
} else {
    die(header('Location: /'.actionLink('editthread', $tid, '', $urlname)));
}

$ref = $_SERVER['HTTP_REFERER'] ?: '/'.actionLink('editdepotentry', $tid, '', $urlname);

if ($_GET['action'] == 'close' && $canClose) {
    $db->updateId('threads', ['closed' => 1], 'id', $tid);
    Report('[b]'.$loguser['name'].'[/] closed thread [b]'.$thread['title'].'[/] -> [g]#HERE#?tid='.$tid, $isHidden);

    if ($acmlmboardLayout == true) {
        OldRedirect(__('Thread closed.'), $ref, __('the thread'));
    } else {
        die(header('Location: '.$ref));
    }
} elseif ($_GET['action'] == 'open' && $canClose) {
    $rThread = Query('update {threads} set closed=0 where id={0}', $tid);
    Report('[b]'.$loguser['name'].'[/] opened thread [b]'.$thread['title'].'[/] -> [g]#HERE#?tid='.$tid, $isHidden);

    if ($acmlmboardLayout == true) {
        OldRedirect(__('Thread opened.'), $ref, __('the thread'));
    } else {
        die(header('Location: '.$ref));
    }
} elseif (($_GET['action'] == 'trash' && HasPermission('mod.trashthreads', $thread['forum']))
    || ($_GET['action'] == 'delete' && HasPermission('mod.deletethreads', $thread['forum']))) {
    if ($_GET['action'] == 'delete') {
        $trashid = Settings::get('secretTrashForum');
        $verb = 'deleted';
    } else {
        $trashid = Settings::get('trashForum');
        $verb = 'trashed';
    }

    if ($trashid > 0) {
        $db->updateId('threads', ['forum' => $trashid, 'closed' => 1], 'id', $tid);

        //Tweak forum counters
        $db->updateId('forums', ['numthreads' => pudl::_increment(-1), 'numposts' => pudl::_increment(-($thread['replies'] + 1))], 'id', $thread['forum']);
        $db->updateId('forums', ['numthreads' => pudl::increment(), 'numposts' => pudl::_increment($thread['replies'] + 1)], 'id', $trashid);

        // Tweak forum counters #2
        Query('	UPDATE {forums} LEFT JOIN {threads}
				ON {forums}.id={threads}.forum AND {threads}.lastpostdate=(SELECT MAX(nt.lastpostdate) FROM {threads} nt WHERE nt.forum={forums}.id)
				SET {forums}.lastpostdate=IFNULL({threads}.lastpostdate,0), {forums}.lastpostuser=IFNULL({threads}.lastposter,0), {forums}.lastpostid=IFNULL({threads}.lastpostid,0)
				WHERE {forums}.id={0} OR {forums}.id={1}', $thread['forum'], $trashid);

        Report('[b]'.$loguser['name']."[/] {$verb} thread [b]".$thread['title'].'[/] -> [g]#HERE#?tid='.$tid, $isHidden);

        $forumname = '';
        if (HasPermission('forum.viewforum', $thread['forum'], true)) {
            $forumname = FetchResult('SELECT title FROM {forums} WHERE id={0}', $thread['forum']);
        }

        if ($acmlmboardLayout == true) {
            OldRedirect(format(__('Thread {0}.'), $verb), actionLink('forum', $thread['forum'], '', $forumname), __('the forum'));
        } else {
            die(header('Location: /'.actionLink('forum', $thread['forum'], '', $forumname)));
        }
    } else {
        Kill(__('No trash forum set. Check board settings.'));
    }
} elseif ($_POST['actionedit']) {
    if ($thread['forum'] != $_POST['moveTo'] && $canMove) {
        $moveto = (int) $_POST['moveTo'];
        $dest = $db->row('forums', ['id' => $moveto]);
        if ($dest) {
            $isHidden = HasPermission('forum.viewforum', $moveto, true);

            //Tweak forum counters
            $db->updateId('forums', ['numthreads' => pudl::_increment(-1), 'numposts' => pudl::_increment(-($thread['replies'] + 1))], 'id', $thread['forum']);
            $db->updateId('forums', ['numthreads' => pudl::increment(), 'numposts' => pudl::_increment($thread['replies'] + 1)], 'id', $moveto);

            $db->updateId('threads', ['forum' => (int) $_POST['moveTo']], 'id', $tid);

            // Tweak forum counters #2
            Query(
                '	UPDATE {forums} LEFT JOIN {threads}
					ON {forums}.id={threads}.forum AND {threads}.lastpostdate=(SELECT MAX(nt.lastpostdate) FROM {threads} nt WHERE nt.forum={forums}.id)
					SET {forums}.lastpostdate=IFNULL({threads}.lastpostdate,0), {forums}.lastpostuser=IFNULL({threads}.lastposter,0), {forums}.lastpostid=IFNULL({threads}.lastpostid,0)
					WHERE {forums}.id={0} OR {forums}.id={1}', $thread['forum'], $moveto
            );

            Report('[b]'.$loguser['name'].'[/] moved thread [b]'.$thread['title'].'[/] -> [g]#HERE#?tid='.$tid, $isHidden);
        }
    }

    $isClosed = $canClose ? (isset($_POST['isClosed']) ? 1 : 0) : $thread['closed'];
    $isSticky = $canStick ? (int) $_POST['isSticky'] : (int) $thread['sticky'];

    $trimmedTitle = $canRename ? trim(str_replace('&nbsp;', ' ', $_POST['title'])) : 'lolnotempty';
    if (!empty($trimmedTitle)) {
        if ($canRename) {
            $thread['title'] = $_POST['title'];
            $thread['description'] = $_POST['description'];

            $thread['icon'] = $_POST['iconurl'];
            $thread['screenshot'] = $_POST['screenshot'];
            $thread['downloadtheme3ds'] = $_POST['downloadtheme3ds'];
            $thread['downloadthemewiiu'] = $_POST['downloadthemewiiu'];
            $thread['downloadlevel3ds'] = $_POST['downloadlevel3ds'];
            $thread['downloadlevelwiiu'] = $_POST['downloadlevelwiiu'];
            $thread['downloadcostumewiiu'] = $_POST['downloadcostumewiiu'];
            $thread['style'] = $_POST['style'];
            $thread['theme'] = $_POST['theme'];
            $thread['progress'] = (int) $_POST['progress'];
        }

        $db->updateId('threads', [
            'title'               => $thread['title'],
			'icon'                => $thread['icon'],
			'closed'              => $isClosed,
			'sticky'              => $isSticky,
            'description'         => $thread['description'],
			'screenshot'          => $thread['screenshot'],
            'downloadcostumewiiu' => !empty($thread['downloadcostumewiiu']) ? $thread['downloadcostumewiiu'] : '',
            'downloadlevel3ds'    => !empty($thread['downloadlevel3ds'])    ? $thread['downloadlevel3ds']    : '',
			'downloadlevelwiiu'   => !empty($thread['downloadlevelwiiu'])   ? $thread['downloadlevelwiiu']   : '',
            'downloadtheme3ds'    => !empty($thread['downloadtheme3ds'])    ? $thread['downloadtheme3ds']    : '',
			'downloadthemewiiu'   => !empty($thread['downloadthemewiiu'])   ? $thread['downloadthemewiiu']   : '',
			'progress'            => $thread['progress'],
            ], 'id', $tid
        );

        $ref = $_POST['ref'] ?: '/'.actionLink('thread', $tid, '', $urlname);
        Report('[b]'.$loguser['name'].'[/] edited thread [b]'.$thread['title']."[/] -> $ref", $isHidden);

        $tags = ParseThreadTags($thread['title']);
        $urlname = $isHidden ? '' : $tags[0];

        if ($acmlmboardLayout == true) {
            OldRedirect(__('Edited!'), $ref, __('the thread'));
        } else {
            die(header('Location: '.$ref));
        }
    } else {
        Alert(__('Your thread title is empty. Enter a title and try again.'));
    }
}

$fields = [];

if ($canRename) {
    $fields['title'] = '<input type="text" class="form-control" placeholder="'.__('Thread Title').'" id="tit" name="title" size=80 maxlength="60" value="'.htmlspecialchars($thread['title']).'">';
    $fields['description'] = '<input type="text" class="form-control" placeholder="'.__('Thread Description (optional)').'" id="des" name="description" size=80 maxlength="50" style="width: 90%;" value="'.htmlspecialchars($thread['description']).'">';
    $fields['screenshot'] = '<input type="text" class="form-control" placeholder="'.__('Depot Screenshot').'" id="sec" name="screenshot" size=80 maxlength="200" style="width: 90%;" value="'.htmlspecialchars($thread['screenshot']).'">';
    $fields['icon'] = '<input type="text" class="form-control" placeholder="'.__('Thread Icon (optional)').'" name="iconurl" size=60 maxlength="100" value="'.htmlspecialchars($thread['icon']).'">';

    if ($forum['id'] == 7) {
        $fields['3dslevel'] = '<input type="text" id="downloadlevel3ds" name="downloadlevel3ds" style="width: 90%;" maxlength="200" value="'.htmlspecialchars($thread['downloadlevel3ds']).'">';
        $fields['wiiulevel'] = '<input type="text" id="downloadlevelwiiu" name="downloadlevelwiiu" style="width: 90%;" maxlength="200" value="'.htmlspecialchars($thread['downloadlevelwiiu']).'">';
    } else {
        $fields['percent'] = '<input type="text" id="progress" name="progress" size=5 maxlength="100" value="'.htmlspecialchars($thread['progress']).'">';
    }

    if ($forum['id'] == 3) {
        $fields['3dstheme'] = '<input type="text" id="downloadtheme3ds" name="downloadtheme3ds" style="width: 90%;" maxlength="200" value="'.htmlspecialchars($thread['downloadtheme3ds']).'">';
        $fields['wiiutheme'] = '<input type="text" id="downloadthemewiiu" name="downloadthemewiiu" style="width: 90%;" maxlength="200" value="'.htmlspecialchars($thread['downloadthemewiiu']).'">';
        $fields['wiiucostume'] = '<input type="text" id="downloadcostumewiiu" name="downloadcostumewiiu" style="width: 90%;" maxlength="200" value="'.htmlspecialchars($thread['downloadcostumewiiu']).'">';
    }
}

if ($canClose) {
    $fields['closed'] = '<label><input type="checkbox" name="isClosed" '.($thread['closed'] ? ' checked="checked"' : '').'> '.__('Closed').'</label>';
}
if ($canStick) {
    $fields['sticky'] = '<label><input type="text" name="isSticky" size=3 value="'.htmlspecialchars($thread['sticky']).'"> '.__('Sticky').'</label>';
}
if ($canMove) {
    $fields['forum'] = makeForumList('moveTo', $thread['forum']);
}

$fields['btnEditThread'] = '<input type="submit" name="actionedit" value="'.__('Save Entry Settings').'">';

echo '
	<script src="'.resourceLink('js/threadtagging.js').'"></script>
	<form action="'.htmlentities(actionLink('editdepotentry')).'" method="post">';

RenderTemplate('form_editthread', ['fields' => $fields]);

echo "
		<input type=\"hidden\" name=\"id\" value=\"$tid\">
		<input type=\"hidden\" name=\"key\" value=\"".$loguser['token'].'">
		<input type="hidden" name="ref" value="'.htmlspecialchars($_SERVER['HTTP_REFERER']).'">
	</form>';
