<?php

$header = __('Welcome to our depot');
$text = __('Welcome to the Level Depot. Here, you can find the latest and best levels from our forums!');
$submissions = __('Specify the following in your submission.
				<ul>
					<li>Name of Level
					<li>Website used (SMMDB, Bookmark, etc...)
					<li>Game it replaces (SMB1/SMB3/SMW/NSMBU)
					<li>Screenshots/Video</ul>');

Alert(__('Here, you can find the latest and best levels from our forums!'), __('Welcome to our Level Depot'));

$getArgs = [];

$console = '';
$style = '';
$smmtheme = '';
$command = '';

if ($http->get('console')) {
    switch ($http->get('console')) {
        case '3ds':
            $console = '3ds';
            $command = " AND t.downloadlevel3ds <> '' ";
            break;
        case 'wiiu':
            $console = 'wiiu';
            $command = " AND t.downloadlevelwiiu <> '' ";
            break;
    }
    $getArgs[] = 'console='.$console;
}

if ($http->get('style')) {
    switch ($http->get('style')) {
        case 'smb1':
            $style = 'smb1';
            $command .= " AND t.style = 'smb1' ";
            break;
        case 'smb3':
            $style = 'smb3';
            $command .= " AND t.style = 'smb3' ";
            break;
        case 'smw':
            $style = 'smw';
            $command .= " AND t.style = 'smw' ";
            break;
        case 'nsmbu':
            $style = 'nsmbu';
            $command .= " AND t.style = 'nsmbu' ";
            break;
        case 'custom':
            $style = 'custom';
            $command .= " AND t.style = 'custom' ";
            break;
    }
    $getArgs[] = 'style='.$style;
}

if ($http->get('theme')) {
    switch ($http->get('theme')) {
        case 'grass':
            $smmtheme = 'grass';
            $command .= " AND t.theme = 'grass' ";
            break;
        case 'under':
            $smmtheme = 'under';
            $command .= " AND t.theme = 'under' ";
            break;
        case 'water':
            $smmtheme = 'water';
            $command .= " AND t.theme = 'water' ";
            break;
        case 'castle':
            $smmtheme = 'castle';
            $command .= " AND t.theme = 'castle' ";
            break;
        case 'ghost':
            $smmtheme = 'ghost';
            $command .= " AND t.theme = 'ghost' ";
            break;
        case 'airship':
            $smmtheme = 'airship';
            $command .= " AND t.theme = 'airship' ";
            break;
    }
    $getArgs[] = 'theme='.$smmtheme;
}

if ($http->get('hackname')) {
    $command .= " AND t.title like '%".htmlspecialchars($http->get('hackname'))."%' ";
    $getArgs[] = 'hackname='.$http->get('hackname');
    $prevfield['hackname'] = $http->get('hackname');
} else {
    $prevfield['hackname'] = '';
}

$sidebarshow = true;

$rFora = Query('SELECT * FROM {forums} WHERE id = 7');
if (NumRows($rFora)) {
    if (!HasPermission('forum.viewforum', $forum['id'])) {
        Kill('You do not have the permission to view the depot.');
    } else {
        $forum = Fetch($rFora);
    }
} else {
    Kill('Whoops. Seems like there were no results for the fields you selected. Why not try different fields?');
}

$showconsoles = true;
$depoturl = 'depot/level';

$numThemes = FetchResult('SELECT COUNT(*) FROM {threads} t WHERE forum = 7 AND depothide=0 '.$command);

$fid = $forum['id'];

$total = $forum['numthreads'];

if (isset($_GET['depotpage'])) {
    $depotpage = (int) $_GET['depotpage'];
} else {
    $depotpage = 0;
}

$tpp = $loguser['depotperpage'];
if (!$tpp) {
    $tpp = 12;
}

$rThreads = Query('	SELECT 
						t.id, t.icon, t.depothide, t.title, t.closed, t.replies, t.lastpostid, t.screenshot, t.description, t.downloadlevelwiiu, t.downloadlevel3ds,
						p.id pid, p.date,
						pt.text,
						su.(_userfields),
						lu.(_userfields)
					FROM 
						{threads} t
						LEFT JOIN {posts} p ON p.thread=t.id AND p.id=t.firstpostid
						LEFT JOIN {posts_text} pt ON pt.pid=p.id AND pt.revision=p.currentrevision
						LEFT JOIN {users} su ON su.id=t.user
						LEFT JOIN {users} lu ON lu.id=t.lastposter
					WHERE t.forum={0} AND p.deleted=0 AND t.depothide=0 '.$command.'
					ORDER BY p.date DESC LIMIT {1u}, {2u}', $fid, $depotpage, $tpp);

$numonpage = NumRows($rThreads);

$getArgs[] = 'depotpage=';
$pagelinks = PageLinks(pageLink('leveldepot', [], implode('&', $getArgs)), $tpp, $depotpage, $numThemes);

$links = [];
if ($loguserid) {
    if (HasPermission('forum.postthreads', $fid)) {
        $links[] = actionLinkTag(__('Post new submission'), 'newthread', $fid, '', $urlname);
    }
}

MakeCrumbs([pageLink('leveldepot') => 'Super Mario Maker Level Depot'], $links);

$posts = [];
while ($thread = Fetch($rThreads)) {
    $pdata = [];

    $starter = getDataPrefix($thread, 'su_');
    $last = getDataPrefix($thread, 'lu_');

    $pdata['text'] = $thread['text'];

    $pdata['screenshots'] = $thread['screenshot'];

    if (startsWith($pdata['screenshots'], 'https://www.youtube.com/')
    || startsWith($pdata['screenshots'], 'https://youtu.be/')) {
        // Check if the screenshot thing starts with a YouTube link

        $pdata['screenshot'] = cleanUpPost(htmlspecialchars($pdata['screenshots']), '', false, false, true);
    } elseif (endsWith($pdata['screenshots'], '.mp4')) {
        // Now check if it ends with a .mp4

        $pdata['screenshot'] = '<video width="280" height="157" controls><source src="'.htmlspecialchars($pdata['screenshots']).'" type="video/mp4"></video>';
    } elseif (endsWith($pdata['screenshots'], '.mp3') || endsWith($pdata['screenshots'], '.wav')) {
        // Check if it's a music file
        // If so, check the extention and load the correct file accordingly

        if (endsWith($pdata['screenshots'], '.mp3')) {
            $type = 'type="audio/mpeg"';
        } elseif (endsWith($pdata['screenshots'], '.wav')) {
            $type = 'type="audio/wav"';
        }

        $pdata['screenshot'] = '<audio controls><source src="'.htmlspecialchars($pdata['screenshots'])."\" $type></audio>";
    } elseif (!empty($pdata['screenshots'])) {
        // Alright, now check if it still isn't empty
        // If it aint, handle it via BBCode images

        $pdata['screenshot'] = cleanUpPost('[imgs]'.$pdata['screenshots'].'[/imgs]');
    } elseif (preg_match('(\[img\](.*?)\[\/img\])', $pdata['text'])
        || preg_match('(\[imgs\](.*?)\[\/imgs\])', $pdata['text'])
        || preg_match('/\[youtube\](.*?)\[\/youtube\]/', $pdata['text'])
        || preg_match("/\<img.+src\=(?:\"|\')(.+?)(?:\"|\')(?:.+?)\>/", $pdata['text'])) {
        // Now, since there's nothing in the actual screenshot field, check in the post itself

        $pdata['screenshots'] = true;
        if (preg_match('/\[youtube\](.*?)\[\/youtube\]/', $pdata['text'])) {
            preg_match('(\[youtube\](.*?)\[\/youtube\])', $pdata['text'], $match);
            $pdata['screenshot'] = cleanUpPost(htmlspecialchars($match[0]), '', false, false, true);
        } elseif (preg_match('(\[img\](.*?)\[\/img\])', $pdata['text'])) {
            preg_match('(\[img\](.*?)\[\/img\])', $pdata['text'], $match);
            $pdata['screenshot'] = parseBBCode('[imgs]'.htmlspecialchars($match[1]).'[/imgs]');
        } elseif (preg_match('(\[imgs\](.*?)\[\/imgs\])', $pdata['text'])) {
            preg_match('(\[imgs\](.*?)\[\/imgs\])', $pdata['text'], $match);
            $pdata['screenshot'] = parseBBCode('[imgs]'.htmlspecialchars($match[1]).'[/imgs]');
        } elseif (preg_match("/\<img.+src\=(?:\"|\')(.+?)(?:\"|\')(?:.+?)\>/", $pdata['text'])) {
            preg_match("/\<img.+src\=(?:\"|\')(.+?)(?:\"|\')(?:.+?)\>/", $pdata['text'], $match);
            $pdata['screenshot'] = parseBBCode(htmlspecialchars($match[1]));
        }
    }
    $pdata['description'] = parseBBCode($thread['description']);

    $tags = ParseThreadTags($thread['title']);

    $pdata['download'] = '';

    //SMMDB & BookMark detection
    if (!empty($thread['downloadlevel3ds'])
     && !empty($thread['downloadlevelwiiu'])
     && substr($thread['downloadlevel3ds'], 0, 22) === 'https://smmdb.ddns.net'
     && strpos($thread['downloadlevelwiiu'], '://') == false) {
        $pdata['download'] = '<a href="'.$thread['downloadlevel3ds'].'">Download 3DS Level (SMMDB)</a><br>
								<a href="'.str_ireplace('3ds', 'zip', $thread['downloadlevel3ds']).'">Download WiiU Level (SMMDB)</a><br>
								<a href="https://supermariomakerbookmark.nintendo.net/courses/'.$thread['downloadlevelwiiu'].'">Super Mario Maker Bookmark URL</a>';
    } else {
        if (!empty($thread['downloadlevel3ds'])) {
            $pdata['download'] .= '<a href="'.$thread['downloadlevel3ds'].'">Download 3DS Level</a>';
        }
        if (!empty($thread['downloadlevel3ds']) && !empty($thread['downloadlevelwiiu'])) {
            $pdata['download'] .= ' | ';
        }
        if (!empty($thread['downloadlevelwiiu'])) {
            if (!strpos($thread['downloadlevelwiiu'], '://')) {
                $pdata['download'] .= '<a href="'.$thread['downloadlevelwiiu'].'">Download WiiU Level</a>';
            } else {
                $pdata['download'] .= '<a href="https://supermariomakerbookmark.nintendo.net/courses/'.$thread['downloadlevelwiiu'].'">Super Mario Maker Bookmark URL</a>';
            }
        }
    }

    $pdata['title'] = '<img src="'.$thread['icon'].'"><a href="'.pageLink('lvlentry', [
                'id'   => $thread['id'],
                'name' => slugify($tags[0]),
            ]).'">'.$tags[0].'</a><br>'.$tags[1];

    $pdata['formattedDate'] = formatdate($thread['date']);
    $pdata['userlink'] = UserLink($starter);

    if ($loguser['layout'] == 'acmlm') {
        if (!$thread['replies']) {
            $comments = 'No comments yet';
        } elseif ($thread['replies'] < 2) {
            $comments = '1 comment';
        } else {
            $comments = $thread['replies'].' comments';
        }
    } else {
        if (!$thread['replies']) {
            $comments = 'No comments yet';
        } elseif ($thread['replies'] < 2) {
            $comments = actionLinkTag('1 comment', 'depost', $thread['lastpostid']).' (by '.UserLink($last).')';
        } else {
            $comments = actionLinkTag($thread['replies'].' comments', 'depost', $thread['lastpostid']).' (last by '.UserLink($last).')';
        }
    }
    $pdata['comments'] = $comments;

    if ($thread['closed']) {
        $newreply = __('Comments closed.');
    } elseif (!$loguserid) {
        $newreply = actionLinkTag(__('Log in'), 'login').__(' to post a comment.');
    } elseif (HasPermission('forum.postthreads', $forum['id'])) {
        $newreply = actionLinkTag(__('Post a comment'), 'newcomment', $thread['id']);
    }
    $pdata['replylink'] = $newreply;

    $posts[] = $pdata;
}

RenderTemplate('depot', ['posts' => $posts, 'pagelinks' => $pagelinks]);
