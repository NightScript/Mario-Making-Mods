<style type="text/css">
#wikidiff ins, #wikidiff del { text-decoration: none; }
#wikidiff ins { background: #060; color: #cfc; }
#wikidiff del { background: #600; color: #fcc; }
</style>
<?php
if (!defined('BLARG')) {
    die();
}

require 'lib/vendor/autoload.php';

$rev = (int) $_GET['rev'];
$page = getWikiPage($pageParams['page'], $rev);
$rev = min($page['revision'], $rev);

$urltitle = $page['id']; //urlencode($page['id']);
$nicetitle = htmlspecialchars(url2title($page['id']));
$title = 'Wiki &raquo; Diff: '.$nicetitle;

$istalk = (strtolower(substr($urltitle, 0, 5)) == 'talk:');

$links = [];

$links[pageLink('wikichanges')] = ['text' => __('Recent changes')];
if ($canedit) {
    $links[pageLink('wikicreate')] = ['text' => __('Create page')];
}

MakeCrumbs([pageLink('wiki') => 'Wiki', actionLink('wiki', $urltitle) => $nicetitle, actionLink('wikidiff', $urltitle, 'rev='.$rev) => 'Diff'], $links);

if ($page['canedit']) {
    if ($page['new']) {
        Kill(format(__('This page has not been created yet.<br><br>{0}'), pageLinkTag('Create page', 'wikicreate')));
    } elseif ($page['revision'] <= 1) {
        Kill(format(__('This page has not been edited since its creation.<br><br>{0}'), actionLinkTag('Edit', 'wikiedit', $urltitle)));
    }
} else {
    if ($page['new']) {
        Kill(__('This page has not been created yet.'));
    } elseif ($page['revision'] <= 1) {
        Kill(__('This page has not been edited since its creation.'));
    }
}
if ($page['flags'] & WIKI_PFLAG_DELETED) {
    Kill(__('This page has been deleted.'));
}

if ($page['canedit']) {
    $links[actionLink('wikiedit', $urltitle)] = ['text' => __('Edit')];
}

$previous = Fetch(Query('SELECT revision, text FROM {wiki_pages_text} WHERE id={0} AND revision<{1} ORDER BY revision DESC LIMIT 1', $urltitle, $rev));
if (!$previous) {
    Kill('Previous revision missing.');
}

$revList = '';

if ($rev > 0) {
    $revs = Query('SELECT pt.revision r FROM {wiki_pages_text} pt WHERE pt.id={0} AND pt.revision>1 ORDER BY r ASC', $urltitle);
    while ($therev = Fetch($revs)) {
        if ($therev['r'] == $rev) {
            $revList .= '&nbsp;'.$therev['r'].'&nbsp;';
        } else {
            $revList .= '&nbsp;'.actionLinkTag($therev['r'], 'wikidiff', $urltitle, 'rev='.$therev['r']).'&nbsp;';
        }
    }
}

RenderTemplate(
    'wikidiff', [
    'nicetitle' => $nicetitle,
    'previous'  => $previous['revision'],
    'current'   => $rev,
    'revList'   => $revList,
    'diff'      => dodiff($page['text'], $previous['text']),
    ]
);

function dodiff($cur, $prev)
{
    $cur = str_replace("\r", '', $cur);
    $prev = str_replace("\r", '', $prev);

    $diff = new cogpowered\FineDiff\Diff(new cogpowered\FineDiff\Granularity\Word());
    $stuff = nl2br($diff->render($prev, $cur));

    return '<div style="font-family:\'Consolas\',\'Courier New\',monospace; border:1px dashed #ccc; padding: 1em;">'.$stuff.'</div>';
}

?>
