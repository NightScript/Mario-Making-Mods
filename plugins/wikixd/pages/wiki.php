<?php

if (!defined('BLARG')) {
    die();
}

$rev = isset($_GET['rev']) ? (int) $_GET['rev'] : 0;
$page = getWikiPage($pageParams['page'], $rev);
$rev = min($rev, $page['revision']);

$urltitle = $page['id'];
$wikititle = htmlspecialchars(url2title($page['id']));

$links = [];

$links[pageLink('wikichanges')] = ['text' => __('Recent changes')];
if ($canedit) {
    $links[pageLink('wikicreate')] = ['text' => __('Create page')];
}

if ($page['canedit']) {
    $links[pageLink('wikiedit', ['page' => $urltitle])] = ['icon' => 'pencil', 'text' => __('Edit')];
}

if ($page['flags'] & WIKI_PFLAG_DELETED) {
    $wikicontents = __('This page has been deleted.');
} elseif ($page['new']) {
    $wikicontents = __('This page does not exist.');

    if ($page['canedit']) {
        $wikicontents .= '<br><br>'.pageLinkTag(__('Create it now'), 'wikicreate');
    }
} else {
    $revInfo = '';
    $revList = '';

    if ($rev > 0) {
        $revs = Query('SELECT pt.revision r FROM {wiki_pages_text} pt WHERE pt.id={0} ORDER BY r ASC', $urltitle);
        while ($therev = Fetch($revs)) {
            if ($therev['r'] == $rev) {
                $revList .= '&nbsp;'.$therev['r'].'&nbsp;';
            } else {
                $revList .= '&nbsp;'.pageLinkTag($therev['r'], 'wiki', ['page' => $urltitle], 'rev='.$therev['r']).'&nbsp;';
            }
        }

        $user = Fetch(Query('SELECT u.(_userfields) FROM {users} u WHERE u.id={0}', $page['user']));
        $user = getDataPrefix($user, 'u_');
        $revInfo = format(__('Viewing revision {0} (by {1} on {2})<br>(revisions: {3})<br><br>'), $rev, userLink($user), formatdate($page['date']), $revList);
    }

    $wikicontents = $revInfo.wikiFilter($page['text'], $page['flags'] & WIKI_PFLAG_NOCONTBOX);
}

if ($page['istalk']) {
    $istalk = true;

	$title = substr($wikititle, 5).' &raquo; Discussion &raquo; Wiki';
	$CrumbArray = [
		pageLink('wiki') => __('Wiki'),
		pageLink('wikiPage', ['page' => substr($pageParams['page'], 5)]) => substr($wikititle, 5),
		pageLink('wikiPage', ['page' => $urltitle]) => __('Discussion'),
	];
    $wikipagetitle = substr($wikititle, 5);
} else {
	$istalk = false;
	$title = $wikititle.' &raquo; Wiki';

	if ($page['ismain']) {
		$CrumbArray = [pageLink('wiki') => __('Wiki')];
	} else {
		$CrumbArray = [pageLink('wiki') => __('Wiki'), pageLink('wikiPage', ['page' => $urltitle]) => $wikititle];
	}

	$links[pageLink('wikiPage', ['page' => 'Talk:'.$urltitle])] = ['text' => __('Discuss')];

    $wikipagetitle = $wikititle;
}

$links[pageLink('wikidiff', ['page' => $urltitle])] = ['text' => __("Page Revisions")];

MakeCrumbs($CrumbArray, $links);

RenderTemplate(
    'wiki', [
    'wikititle'          => $wikititle,
    'wikicontents'       => $wikicontents,
    'istalk'             => $istalk,
    'wikipagelink'       => $wikipagelink,
    'wikidiscussionlink' => $wikidiscussionlink,
    'wikipagetitle'      => $wikipagetitle,
    ]
);
