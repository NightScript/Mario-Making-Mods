<?php

if (!defined('BLARG')) {
    die();
}

$title = __('Last posts');

$allowedforums = ForumsWithPermission('forum.viewforum');

$time = $http->get('time');
if ($time != 'new') {
    $time = (int) $time;
}
if (!$time) {
    $time = 86400;
}
$show = $http->get('show');
if ($show != 'threads' && $show != 'posts') {
    $show = 'threads';
}

$from = (int) $http->get('from');
$fparam = $from ? '&from='.$from : '';

$links = [];
$links[pageLink('lastposts', [], 'time='.$time.'&show=threads'.$fparam)] = ['icon' => 'reorder', 'text' => __('List threads')];
$links[pageLink('lastposts', [], 'time='.$time.'&show=posts'.$fparam)] = ['icon' => 'square-o', 'text' => __('Show posts')];

$spans = [3600 => __('1 hour'), 86400 => __('1 day'), 259200 => __('3 days'), 604800 => __('1 week'), 'new' => __('New posts')];
$dropdownlinks = [];
foreach ($spans as $span=>$desc) {
    $dropdownlinks['Posts within'][pageLink('lastposts', [], 'time='.$span.'&show='.$show.$fparam)] = ['text' => $desc, 'active' => ($span == $time ? true : false)];
}

MakeCrumbs([pageLink('board') => __('Forums'), pageLink('lastposts') => __('Last posts')], $links, $dropdownlinks);

$mindate = ($time == 'new') ? ($loguserid ? 'IFNULL(tr.date,0)' : '{1}') : time() - $time;
$total = FetchResult(
    'SELECT
						COUNT('.($show == 'threads' ? 'DISTINCT p.thread' : '*').')
					FROM {posts} p
					LEFT JOIN {threads} t ON t.id=p.thread '.
    (($loguserid && $time == 'new') ? 'LEFT JOIN {threadsread} tr ON tr.thread = p.thread AND tr.id = {0} WHERE t.forum IN ({3c})' : 'WHERE p.date > {2} AND t.forum IN ({3c})'), $loguserid, time() - 900, $mindate, $allowedforums
);

if (!$total) {
    Kill($time == 'new' ? __('No unread posts.') : __('No posts have been made during this timespan.'), __('Notice'));
}

$perpage = ($show == 'posts') ? $loguser['postsperpage'] : $loguser['threadsperpage'];
$pagelinks = PageLinks(actionLink('lastposts', '', "time={$time}&show={$show}&from="), $perpage, $from, $total);

if ($show == 'threads') {
    $mindate = ($time == 'new') ? ($loguserid ? 'IFNULL(tr.date,0)' : time() - 900) : time() - $time;
    $rThreads = Query(
        '	SELECT
							t.*,
							f.(id,title),
							'.($loguserid ? 'tr.date readdate,' : '').'
							su.(_userfields),
							lu.(_userfields)
						FROM
							{threads} t
							'.($loguserid ? 'LEFT JOIN {threadsread} tr ON tr.thread=t.id AND tr.id={0}' : '')."
							LEFT JOIN {forums} f ON f.id=t.forum
							LEFT JOIN {users} su ON su.id=t.user
							LEFT JOIN {users} lu ON lu.id=t.lastposter
						WHERE t.forum IN ({3c}) AND t.lastpostdate>{$mindate}
						ORDER BY t.lastpostdate DESC LIMIT {1u}, {2u}", $loguserid, $from, $perpage, $allowedforums
    );

    makeThreadListing($rThreads, $pagelinks, false, true);
} else {
    $mindate = ($time == 'new') ? ($loguserid ? 'IFNULL(tr.date,0)' : time() - 900) : time() - $time;
    $rPosts = Query(
        '	SELECT
							p.*,
							pt.text, pt.revision, pt.user AS revuser, pt.date AS revdate,
							u.(_userfields), u.(rankset,title,picture,posts,postheader,signature,signsep,lastposttime,lastactivity,regdate,globalblock,fulllayout,postplusones,location,id),
							ru.(_userfields),
							du.(_userfields),
							t.id thread, t.title threadname,
							f.id fid
						FROM
							{posts} p
							LEFT JOIN {posts_text} pt ON pt.pid = p.id AND pt.revision = p.currentrevision
							LEFT JOIN {users} u ON u.id = p.user
							LEFT JOIN {users} ru ON ru.id=pt.user
							LEFT JOIN {users} du ON du.id=p.deletedby
							LEFT JOIN {threads} t ON t.id=p.thread
							'.(($loguserid && ($time == 'new')) ? 'LEFT JOIN {threadsread} tr ON tr.thread=t.id AND tr.id={0}' : '')."
							LEFT JOIN {forums} f ON f.id=t.forum
							LEFT JOIN {categories} c ON c.id=f.catid
						WHERE p.date>{$mindate} AND f.id IN ({3c})
						ORDER BY date DESC LIMIT {1u}, {2u}", $loguserid, $from, $perpage, $allowedforums
    );

    RenderTemplate('pagelinks', ['pagelinks' => $pagelinks, 'position' => 'top']);

    while ($post = Fetch($rPosts)) {
        MakePost($post, POST_NORMAL, ['threadlink'=>1, 'tid'=>$post['thread'], 'fid'=>$post['fid'], 'noreplylinks'=>1], htmlspecialchars($post['threadname']), $post['id']);
    }

    RenderTemplate('pagelinks', ['pagelinks' => $pagelinks, 'position' => 'bottom']);
}
