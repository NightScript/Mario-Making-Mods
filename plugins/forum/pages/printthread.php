<?php
if (!defined('BLARG')) {
    die();
}

if (isset($_GET['pid'])) {
    header('HTTP/1.1 301 Moved Permanently');
    header('Status: 301 Moved Permanently');
    die(header('Location: '.actionLink('post', $_GET['pid'])));
}

$tid = (int) $_GET['id'];
if (!$tid) {
    Kill(__('Enter a thread ID, and try again'), __('Unspecified thread ID'));
}

$thread = $db->row('threads', ['id' => $tid]);
if (!$thread) {
    Kill(__('Unknown thread ID.'));
}

$forum = $db->row('forums', ['id' => $thread['forum']]);
if ($forum) {
    if (!HasPermission('forum.viewforum', $thread['forum'])) {
        Kill(__('You may not access this forum.'));
    }
} else {
    Kill(__('Unknown forum ID.'));
}

$tags = ParseThreadTags($thread['title']);
$thread['title'] = strip_tags($thread['title']);
$title = $thread['title'];

?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv=\"Content-Type\" content=\"text/html; CHARSET=utf-8\" />
	<title><?=$title; ?> - <?=htmlspecialchars(Settings::get('boardname')); ?></title>
</head>
<body>
	<h1><?=$title; ?></h1>
	<p><?=formatdatenow(); ?></p>

<?php

if ($thread['poll']) {
    $rPoll = Query('select * from {poll} where id={0}', $thread['poll']);
    if (NumRows($rPoll)) {
        $poll = Fetch($rPoll);

        $rCheck = Query('select * from {pollvotes} where poll={0} and user={1}', $thread['poll'], $loguserid);
        if (NumRows($rCheck)) {
            while ($check = Fetch($rCheck)) {
                $pc[$check['choice']] = '&#x2714; ';
            }
        }

        $totalVotes = FetchResult('select count(*) from {pollvotes} where poll={0}', $thread['poll']);

        $rOptions = Query('select * from {poll_choices} where poll={0}', $thread['poll']);
        $pops = 0;
        $options = [];
        $voters = [];
        while ($option = Fetch($rOptions)) {
            $options[] = $option;
        }

        foreach ($options as $option) {
            $option['choice'] = htmlspecialchars($option['choice']);

            $rVotes = Query('select * from {pollvotes} where poll={0} and choice={1}', $thread['poll'], $pops);
            $votes = NumRows($rVotes);
            while ($vote = Fetch($rVotes)) {
                if (!in_array($vote['user'], $voters)) {
                    $voters[] = $vote['user'];
                }
            }

            $label = format('{0} {1}', $pc[$pops], $option['choice']);

            if ($totalVotes > 0) {
                $width = 100 * ($votes / $totalVotes);
                $alt = format('{0} votes, {2}%', $votes, $totalVotes, floor($width));
            }

            $pollLines .= '
		<tr>
			<td>
				<b>'.$label.'</b><br>
				'.$alt.'
			</td>
		</tr>';
            $pops++;
        }
        $voters = count($voters); ?>
	<table border="2">
		<thead><tr><th>Poll: <?=htmlspecialchars($poll['question'])?></th></tr></thead>
		<tbody><?=$pollLines?></tbody>
		<tfoot><tr><td>
			<?=format($voters == 1 ? __('{0} user has voted so far.') : __('{0} users have voted so far.'), $voters)?>
		</td></tr></tfoot>
	</table>
<?php
    }
}

$rPosts = Query(
    '
				SELECT
					{posts}.id, {posts}.date, {posts}.deleted, {posts}.options, {posts}.num,
					{posts_text}.text, {posts_text}.revision,
					{users}.name, {users}.displayname, {users}.rankset, {users}.posts
				FROM
					{posts}
					LEFT JOIN {posts_text} on {posts_text}.pid = {posts}.id and {posts_text}.revision = {posts}.currentrevision
					LEFT JOIN {users} on {users}.id = {posts}.user
				WHERE thread={0} 
				ORDER BY date ASC', $tid
);

if (NumRows($rPosts)) {
    while ($post = Fetch($rPosts)) {
        $noSmiles = $post['options'] & 2;
        $noBr = $post['options'] & 4;
        $text = $post['text'];

        $text = preg_replace("'\[spoiler\](.*?)\[/spoiler\]'si", '&laquo;Spoiler&raquo;', $text);
        $text = preg_replace("'\[video\](.*?)\[/video\]'si", '&laquo;HTML5 video&raquo;', $text);
        $text = preg_replace("'\[youtube\](.*?)\[/youtube\]'si", '&laquo;YouTube video&raquo;', $text);
        $text = preg_replace("'\[youtube/loop\](.*?)\[/youtube\]'si", '&laquo;YouTube video&raquo;', $text);
        $text = preg_replace("'\[swf ([0-9]+) ([0-9]+)\](.*?)\[/swf\]'si", '&laquo;Flash video&raquo;', $text);
        $text = preg_replace("'\[svg ([0-9]+) ([0-9]+)\](.*?)\[/svg\]'si", '&laquo;SVG image&raquo;', $text);

        $text = CleanUpPost($text, $post['name'], $noSmiles, $noBr);
        $text = preg_replace("'<div class=\"geshi\">(.*?)</div>'si", '<div class="geshi"><code>\\1</code></div>', $text);
        $text = preg_replace("'<table class=\"outline\">'si", '<table border="2">', $text);
        $text = preg_replace("'<td (.*?)style=\"(.*?)\"(.*?)>'si", '<td \\1\\3>', $text);
        $text = preg_replace("'<a (.*?)style=\"(.*?)\"(.*?)>'si", '<a \\1\\3>', $text);

        $tags = [];
        $rankHax = $post['posts'];
        $post['posts'] = $post['num'];
        $tags = [
            'numposts' => $post['num'],
            '5000'     => 5000 - $post['num'],
            '20000'    => 20000 - $post['num'],
            '30000'    => 30000 - $post['num'],
        ];
        $post['posts'] = $rankHax;
        $text = ApplyTags($text, $tags);

        $_page_contents .=
        '
	<hr />
	<p>
		<strong>
			'.$post['name'].'
		</strong>
		<small>
			&mdash; '.formatdate($post['date']).', post #'.$post['id'].'
		</small>
	</p>
	<p>
		'.$text.'
	</p>
';
    }
}

die();
