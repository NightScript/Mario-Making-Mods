<?php

//  AcmlmBoard XD - Post editing page
//  Access: users
if (!defined('BLARG')) {
    die();
}

$title = __('Edit post');

if (!$loguserid) {
    Kill(__('You must be logged in to edit posts.'));
}

if (isset($ckeditor_color)) {
    echo '<script src="/ckeditor/ckeditor.js"></script>';
}

require BOARD_ROOT.'lib/upload.php';

$pid = (int) $_REQUEST['id'];

$rPost = Query(
    '
	SELECT
		{posts}.*,
		{posts_text}.text
	FROM {posts}
		LEFT JOIN {posts_text} ON {posts_text}.pid = {posts}.id AND {posts_text}.revision = {posts}.currentrevision
	WHERE id={0}', $pid
);

if (NumRows($rPost)) {
    $post = Fetch($rPost);
    $tid = $post['thread'];
} else {
    Kill(__('Unknown post ID.'));
}

checknumeric($pid);

$rUser = Query('select * from {users} where id={0}', $post['user']);
if (NumRows($rUser)) {
    $user = Fetch($rUser);
} else {
    Kill(__('Unknown user ID.'));
}

$rThread = Query('select * from {threads} where id={0}', $tid);
if (NumRows($rThread)) {
    $thread = Fetch($rThread);
} else {
    Kill(__('Unknown thread ID.'));
}

$rFora = Query('select * from {forums} where id={0}', $thread['forum']);
if (NumRows($rFora)) {
    if (HasPermission('forum.viewforum', $forum['id'])) {
        $forum = Fetch($rFora);
    } else {
        Kill(__('You may not access this forum.'));
    }
} else {
    Kill(__('Unknown forum ID.'));
}

$fid = $forum['id'];
$OnlineUsersFid = $fid;

$isHidden = !HasPermission('forum.viewforum', $forum['id'], true);

$isFirstPost = ($thread['firstpostid'] == $post['id']);
$isLastPost = ($thread['lastpostid'] == $post['id']);

if ($thread['closed'] && !HasPermission('mod.closethreads', $fid)) {
    Kill(__('This thread is closed.'));
}

if ((int) $_GET['delete'] == 1) {
    if ($_GET['key'] != $loguser['token']) {
        Kill(__('No.'));
    }

    if ($isFirstPost) {
        Kill(__("You may not delete a thread's first post."));
    }

    if (!HasPermission('mod.deleteposts', $fid) && ($post['user'] == $loguserid && !HasPermission('forum.deleteownposts', $fid))) {
        Kill(__('You are not allowed to delete this post.'));
    }

    Report($loguser['name']." has deleted a post ($pid) in ".$thread['name'].': '.$_GET['reason'], 1);
    $db->updateId('posts', ['deleted' => 1, 'deletedby' => $loguserid, 'reason' => $_GET['reason']], 'id', $pid);

    if ($acmlmboardLayout == true) {
        OldRedirect(__('Deleted!'), actionLink('post', $pid), __('the thread'));
    } else {
        die(header('Location: /'.actionLink('post', $pid)));
    }
} elseif ((int) $_GET['delete'] == 2) {
    if ($_GET['key'] != $loguser['token']) {
        Kill(__('No.'));
    }

    if (!HasPermission('mod.deleteposts', $fid)) {
        Kill(__("You're not allowed to undelete posts."));
    }

    Report($loguser['name']." has restored a post ($pid) in ".$thread['name'], 1);
    $db->updateId('posts', ['deleted' => 0, 'deletedby' => '', 'reason' => ''], 'id', $pid);

    if ($acmlmboardLayout == true) {
        OldRedirect(__('Restored!'), actionLink('post', $pid), __('the thread'));
    } else {
        die(header('Location: /'.actionLink('post', $pid)));
    }
} elseif ((int) $_GET['delete'] == 3) {
    if ($_GET['key'] != $loguser['token']) {
        Kill(__('No.'));
    }

    if (!$loguserid == 1) {
        Kill(__('You are not allowed to delete this post.'));
    }

    $rPosts = Query('DELETE FROM {posts} where id={0} limit 1', $pid);

    Kill(__('This post has been wiped successfully.'));
}

if ($post['deleted']) {
    Kill(__('This post has been deleted.'));
}

if ($isFirstPost) {
    if (($post['user'] != $loguserid || !HasPermission('user.editownposts')) && !HasPermission('mod.editfirstpost', $fid)) {
        Kill(__('You are not allowed to edit the first post of this thread.'));
    }
} else {
    if (($post['user'] != $loguserid || !HasPermission('user.editownposts')) && !HasPermission('mod.editposts', $fid)) {
        Kill(__('You are not allowed to edit this post.'));
    }
}

$tags = ParseThreadTags($thread['title']);
MakeCrumbs(forumCrumbs($forum) + [actionLink('thread', $tid, '', $isHidden ? '' : $tags[0]) => $tags[0], actionLink('editpost', $pid) => __('Edit post')]);

if (!isset($ckeditor_color)) {
    LoadPostToolbar();
}

$attachs = [];
if ($post['has_attachments']) {
    $res = Query(
        'SELECT id,filename 
		FROM {uploadedfiles}
		WHERE parenttype={0} AND parentid={1} AND deldate=0
		ORDER BY filename',
        'post_attachment', $pid
    );
    while ($a = Fetch($res)) {
        $attachs[$a['id']] = $a['filename'];
    }
}

if (isset($_POST['saveuploads'])) {
    $attachs = HandlePostAttachments(0, false);
} elseif (isset($_POST['actionpreview'])) {
    $attachs = HandlePostAttachments(0, false);

    $previewPost['text'] = $_POST['text'];
    $previewPost['num'] = $post['num'];
    $previewPost['id'] = 0;
    $previewPost['options'] = 0;
    if ($_POST['nopl']) {
        $previewPost['options'] |= 1;
    }
    if ($_POST['nosm']) {
        $previewPost['options'] |= 2;
    }
    $previewPost['mood'] = (int) $_POST['mood'];
    $previewPost['has_attachments'] = !empty($attachs);
    $previewPost['preview_attachs'] = $attachs;

    foreach ($user as $key => $value) {
        $previewPost['u_'.$key] = $value;
    }
    MakePost($previewPost, POST_SAMPLE);
} elseif (isset($_POST['actionpost'])) {
    if ($_POST['key'] != $loguser['token']) {
        Kill(__('No.'));
    }

    $rejected = false;

    if (!trim($_POST['text'])) {
        Alert(__('Enter a message and try again.'), __('Your post is empty.'));
        $rejected = true;
    }

    if (!$rejected) {
        $bucket = 'checkPost';
        include BOARD_ROOT.'lib/pluginloader.php';
    }

    if (!$rejected) {
        $_POST['text'] = utfmb4String($_POST['text']);

        $options = 0;
        if ($_POST['nopl']) {
            $options |= 1;
        }
        if ($_POST['nosm']) {
            $options |= 2;
        }

        $attachs = HandlePostAttachments($pid, true);
        $db->updateId('posts', ['options' => $options, 'mood' => (int) $_POST['mood'], 'has_attachments' => !empty($attachs) ? 1 : 0], 'id', $pid);

        if ($_POST['text'] != $post['text']) {
            $rev = fetchResult('select max(revision) from {posts_text} where pid={0}', $pid);
            $rev++;

            $db->insert('posts_text', ['pid' => $pid, 'text' => $_POST['text'], 'revision' => $rev, 'user' => $loguserid, 'date' => $db->time()]);
            $db->updateId('posts', ['currentrevision' => pudl::increment()], 'id', $pid);

            // mark the thread as new if we edited the last post
            // all we have to do is update the thread's lastpostdate
            if ($isLastPost) {
                $db->updateId('threads', ['lastpostdate' => $db->time()], 'id', $tid);
                $db->updateId('forums', ['lastpostdate' => $db->time()], 'id', $fid);
            }
        }

        Report('Post edited by [b]'.$loguser['name'].'[/] in [b]'.$thread['title'].'[/] ('.$forum['title'].') -> [g]#HERE#?pid='.$pid, $isHidden);
        $bucket = 'editpost';
        include BOARD_ROOT.'lib/pluginloader.php';

        if ($acmlmboardLayout == true) {
            OldRedirect(__('Edited!'), actionLink('post', $pid), __('the thread'));
        } else {
            die(header('Location: /'.actionLink('post', $pid)));
        }
    } else {
        $attachs = HandlePostAttachments(0, false);
    }
}

if (isset($_POST['actionpreview']) || isset($_POST['actionpost'])) {
    $prefill = $_POST['text'];
    if ($_POST['nopl']) {
        $nopl = true;
    }
    if ($_POST['nosm']) {
        $nosm = true;
    }
} else {
    $prefill = $post['text'];
    if ($post['options'] & 1) {
        $nopl = true;
    }
    if ($post['options'] & 2) {
        $nosm = true;
    }
    $_POST['mood'] = $post['mood'];
}

$moodSelects = [];
if ($_POST['mood']) {
    $moodSelects[(int) $_POST['mood']] = 'selected="selected" ';
}
$moodOptions = Format('<option {0}value="0">'.__('[Default avatar]')."</option>\n", $moodSelects[0]);
$rMoods = Query('SELECT mid, name FROM {moodavatars} WHERE uid={0} order by mid asc', $post['user']);
while ($mood = Fetch($rMoods)) {
    $moodOptions .= Format("<option {0}value=\"{1}\">{2}</option>\n", $moodSelects[$mood['mid']], $mood['mid'], htmlspecialchars($mood['name']));
}

if (isset($ckeditor_color)) {
    $textfield = htmlspecialchars(nl2br($prefill));
} else {
    $textfield = htmlspecialchars($prefill);
}

$fields = [
    'text' => '<textarea name="text" id="text" rows="16" class="form-control">'.$textfield.'</textarea>',
    'mood' => '<select size=1 name="mood">'.$moodOptions.'</select>',
    'nopl' => checkbox('nopl', __('Disable post layout', 1), $nopl),
    'nosm' => checkbox('nosm', __('Disable smilies', 1), $nosm),

    'btnPost'    => '<input type="submit" class="btn btn-primary" name="actionpost" value="'.__('Save').'">',
    'btnPreview' => '<input type="submit" class="btn btn-default" name="actionpreview" value="'.__('Preview').'">',
];

if (isset($ckeditor_color)) {
    $fields['ckeditor'] = "<script>CKEDITOR.replace( 'text', {lang: 'en', uiColor: '$ckeditor_color'});</script>";
    $fields['noscript'] = __('Please enable Javascript in order to view the WYSIWYG editor.');
} else {
    $fields['noscript'] = __('Please enable Javascript in order to view the BBCode Toolbar.');
}

echo '
	<form name="postform" action="'.htmlentities(actionLink('editpost', $pid)).'" method="post" enctype="multipart/form-data">';

RenderTemplate('form_editpost', ['fields' => $fields]);

PostAttachForm($attachs);

echo "
		<input type=\"hidden\" name=\"key\" value=\"{$loguser['token']}\">
	</form>
	<script type=\"text/javascript\">
		document.postform.text.focus();
	</script>
";

doThreadPreview($tid, $post['date']);
