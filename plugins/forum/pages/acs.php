<?php
$title = __('Active users (server time day blocks)');
MakeCrumbs([actionLink('acs') => __('Active Users')]);

$time = (int)$_GET['time'];
$past = (int)$_GET['past'];
$easymode = $_GET['easymode'];

if ($time < 1) {
    $time = 86400;
}

$allowedforums = ForumsWithPermission('forum.viewforum');

$lowerthan = $db->time() - (dtime() % 86400) - $past * 86400;
$higherthan = $db->time() - (dtime() % 86400) - ($past - 1) * 86400;

if (isset($easymode)) {
    $users = Query('SELECT u.*, COUNT(*) posnum
					FROM {users} u
					LEFT JOIN {posts} p ON p.user=u.id
					LEFT JOIN {threads} t ON t.id=p.thread
					LEFT JOIN {forums} f ON f.id=t.forum
					WHERE p.date > {0} AND p.date < {1}
					AND t.forum IN ({2c}) AND p.deleted=0 
					GROUP BY u.id 
					ORDER BY num DESC', $lowerthan, $higherthan, $allowedforums);
} else {
	$users = Query('SELECT *, SUM(num) posnum
					FROM (
						SELECT u.*, CASE WHEN COUNT(*) >10 THEN 10 ELSE COUNT(*) END num 
						FROM {users} u 
						LEFT JOIN {posts} p ON p.user=u.id 
						LEFT JOIN {threads} t ON t.id=p.thread 
						LEFT JOIN {forums} f ON f.id=t.forum 
						WHERE p.date > {0} AND p.date < {1} 
						AND t.forum IN ({2c}) AND p.deleted=0 
						GROUP BY p.thread, u.id
					) inter
					GROUP BY id
					ORDER BY num DESC', $lowerthan, $higherthan, $allowedforums);
}

    $fields = [
        'submit'   => '<input type="submit" value="'.__('Submit').'">',
        'past'     => "<input type=\"text\" name=\"past\" size=\"3\" maxlength=\"4\" value=\"$past\">",
        'easymode' => '<input type="checkbox" name="easymode" '.(isset($easymode) ? 'checked' : '').'>',
    ];

    echo '<form action="'.pageLink('acs').'" method="get">';
    RenderTemplate('form_acs', ['fields' => $fields]);
    echo '</form>';

    if ($past < 0) {
        Kill(__('You need to set an actual time'));
    }

    echo    'Active users on '.cdate($loguser['dateformat'], time() - $past * 86400).':
			<table class="table table-bordered table-striped">
				<tr class="header1">
					<th class="b h" width="30">#</th>
					<th class="b h">Username</th>
					<th class="b h" width="150">Registered on</th>
					<th class="b h" width="50">Posts</th>
					<th class="b h" width="50">Total</th>';

    $q = 1;
    $p = -1;
    for ($i = 1; $user = fetch($users); $i++) {
        if ($p != $user['num']) {
            if ($q <= 5 && $i > 5) {
                echo '
					<tr class="n1" align="center">
						<td class="b" colspan="5" style="height:3px"><div style="height:1px"></div></td>
					</tr>';
            }
            $q = $i;
        }

        echo "<tr align=\"center\">
					<td class=\"b\">$i.</td>
					<td class=\"b\" align=\"left\">".userlink($user).'</td>
					<td class="b">'.cdate($loguser['dateformat'], $user['regdate'])."</td>
					<td class=\"b\"><b>{$user['posnum']}</b></td>
					<td class=\"b\">{$user['posts']}</b></td>";

        $p = $user['num'];
    }
    echo '</table>';

    function dtime()
    {
        return $db->time() + 7200;
    }
