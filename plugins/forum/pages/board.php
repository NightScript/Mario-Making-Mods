<?php

if (!defined('BLARG')) {
    die();
}

$board = $pageParams['id'] || $_GET['id'];
if (!$board || !isset($forumBoards[$board])) {
    $board = '';
}

$category = '';
if (!empty((int) $_GET['category'])) {
    $category = (int) $_GET['category'];
}

if ($loguserid && isset($_GET['action']) && $_GET['action'] == 'markallread') {
    Query(
        'REPLACE INTO {threadsread} (id,thread,date) SELECT {0}, t.id, {1} FROM {threads} t'.(!empty($board) ? ' LEFT JOIN {forums} f ON f.id=t.forum WHERE f.board={2}' : ''),
        $loguserid,
        time(),
        $board
    );

    die(header('Location: '.actionLink('board', $board)));
}

$links = [];
$links[actionLink('postsbytime')] = ['text' => __('Posts by time')];
$links[actionLink('postsbyforum')] = ['text' => __('Posts by forum')];
if ($loguserid) {
    $links[actionLink('board', $board, 'action=markallread')] = ['icon' => 'ok', 'text' => __('Mark all forums read')];
}

MakeCrumbs(forumCrumbs(['board' => $board]), $links);

if (empty($board)) {
    $statData = Fetch(Query('SELECT
							(SELECT COUNT(*) FROM {threads}) AS numThreads,
							(SELECT COUNT(*) FROM {posts}) AS numPosts,
							(SELECT COUNT(*) FROM {users}) AS numUsers,
							(SELECT COUNT(*) FROM {posts} WHERE date > {0}) AS newPostToday,
							(SELECT COUNT(*) FROM {threads} WHERE lastpostdate > {0}) AS newThreadToday,
							(SELECT COUNT(*) FROM {posts} WHERE date > {1}) AS newPostLastHour,
							(SELECT COUNT(*) FROM {threads} WHERE lastpostdate > {1}) AS newThreadLastHour,
							(SELECT COUNT(*) FROM {posts} WHERE date > {2}) AS newPostLastWeek,
							(SELECT COUNT(*) FROM {threads} WHERE lastpostdate > {2}) AS newThreadLastWeek,
							(SELECT COUNT(*) FROM {users} WHERE lastposttime > {3}) AS numActive',
							time() - 86400,
							time() - 3600,
							time() - 604800,
							time() - 2592000
    ));

    $statData['pctActive'] = $statData['numUsers'] ? ceil((100 / $statData['numUsers']) * $statData['numActive']) : 0;
    $statData['birthday'] = getBirthdaysText();
	$statData['online'] = getOnlineUsersText();

    $lastUser = Query('SELECT u.(_userfields) FROM {users} u ORDER BY u.regdate DESC LIMIT 1');
    if (NumRows($lastUser)) {
        $lastUser = getDataPrefix(Fetch($lastUser), 'u_');
        $statData['lastUserLink'] = UserLink($lastUser);
    }

    RenderTemplate('boardstats', ['stats' => $statData]);
}

makeAnncBar($board);
makeForumListing(0, $board, 'forumlist', $category);
