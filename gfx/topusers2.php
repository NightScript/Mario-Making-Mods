<?php

    define('BLARG', '1');
    require __DIR__.'/../lib/common.php';

if ($_GET['order']) {
    $orderby = '`cnt`';
} else {
    $orderby = '`u`.`posts`';
}

    $startdate = floor((time() - (6 * 60 * 60)) / 86400) * 86400 + (6 * 60 * 60);
    $enddate = $startdate + 86400;

    $users = query(
        'SELECT COUNT(*) as `cnt`, `u`.`name`, `u`.`displayname`, `u`.`posts` '.
                            'FROM `posts` `p` '.
                            'LEFT JOIN `users` `u` ON `p`.`user` = `u`.`id` '.
                            "WHERE `p`.`date` >= '$startdate' AND `p`.`date` < '$enddate' ".
                            'GROUP BY `p`.`user` '.
                            "ORDER BY $orderby DESC ".
        'LIMIT 20'
    );

    $img = imagecreatetruecolor(512, 192);
    $urlcolor = $_GET['color'];
    if ($urlcolor) {
        if (file_exists(__DIR__.'/lib/themes/'.$urlcolor.'.php')) {
            include __DIR__.'/lib/themes/'.$urlcolor.'.php';
        } else {
            include __DIR__.'/lib/themes/default.php';
        }
    } else {
        include __DIR__.'/lib/themes/default.php';
    }

    $urlfont = $_GET['font'];
    $pickfont = '';
    if ($urlfont && file_exists(__DIR__.'/lib/font/font'.$urlfont.'.png')) {
        $pickfont = $urlfont;
    }

    $c['gridline'] = imagecolorallocatealpha($img, 200, 110, 100, 100);
    $c['alternate'] = imagecolorallocatealpha($img, 0, 0, 0, 110);

    box(0, 1, 64, 23, 0);

    box(1, 0, 11, 3, 0);
    twrite($c['font']['W'], 2, 1, 0, 'User', $pickfont);

    box(13, 0, 28, 3, 0); //44*8=352
    twrite($c['font']['W'], 14, 1, 0, 'Total', $pickfont);

    box(42, 0, 21, 3, 0); //44*8=352
    twrite($c['font']['W'], 43, 1, 0, 'Today', $pickfont);

    $sc[1] = 1;
    $sc[2] = 5;
    $sc[3] = 25;
    $sc[4] = 100;
    $sc[5] = 250;
    $sc[6] = 500;
    $sc[7] = 1000;
    $sc[8] = 2500;
    $sc[9] = 5000;
    $sc[10] = 10000;
    $sc[11] = 100000;
    $sc[12] = 1000000;
    $sc[13] = 10000000;
    $sc[14] = 100000000;
    $sc[15] = 1000000000;

    for ($i = 4; $i <= 23; $i += 2) {
        imagefilledrectangle($img, 8, $i * 8, 504, $i * 8 + 7, $c['alternate']);
    }

    for ($i = 152; $i <= 332; $i += 10) {
        imageline($img, $i, 3 * 8, $i, 23 * 8, $c['gridline']);
    }
    imageline($img, 152, 23 * 8, 332, 23 * 8, $c['gridline']);

    for ($i = 384; $i <= 504; $i += 10) {
        imageline($img, $i, 3 * 8, $i, 23 * 8, $c['gridline']);
    }
    imageline($img, 384, 23 * 8, 504, 23 * 8, $c['gridline']);

    for ($i = 0; $x = fetch($users); $i++) {
        $userdat[$i] = $x;
    }

    if (!$userdat) {
        $userdat = [];
    }

    $userdat2 = $userdat;
    foreach ($userdat2 as $key => $row) {
        $postcounts[$key] = $row['posts'];
        $dailycounts[$key] = $row['cnt'];
        $xxx++;
    }

    if ($xxx) {
        $maxp = max($postcounts);
        $maxd = max($dailycounts);
    }

    for ($s = 1; ($maxp / $sc[$s]) > 176; $s++) {
    }
    if (!$sc[$s]) {
        $sc[$s] = 1;
    }

    for ($s2 = 1; ($maxd / $sc[$s2]) > 120; $s2++) {
    }
    if (!$sc[$s2]) {
        $sc[$s2] = 1;
    }

    $i = -1;

    foreach ($userdat as $i => $user) {
        $name = mb_convert_encoding(htmlspecialchars($user['displayname'] ? $user['displayname'] : $user['name']), 'ISO-8859-1');
        $posts = $user['posts'];
        $daily = $user['cnt'];
        $vline = $i + 3;

        twrite($c['font']['R'], 1, $vline, 0, substr($name, 0, 12), $pickfont);
        twrite($c['font']['W'], 13, $vline, 5, $posts, $pickfont);

        twrite($c['font']['W'], 42, $vline, 5, $daily, $pickfont);

        imagefilledrectangle($img, 153, $vline * 8 + 1, 152 + $posts / $sc[$s], $vline * 8 + 7, $c['bxb0']);
        imagefilledrectangle($img, 152, $vline * 8, 151 + $posts / $sc[$s], $vline * 8 + 6, $c['bar'][$s]);
        imagefilledrectangle($img, 385, $vline * 8 + 1, 385 + $daily / $sc[$s2], $vline * 8 + 7, $c['bxb0']);
        imagefilledrectangle($img, 384, $vline * 8, 384 + $daily / $sc[$s2], $vline * 8 + 6, $c['bar'][$s2]);
    }

    header('Content-type:image/png');
    imagepng($img);
    imagedestroy($img);
