<?php

if (!defined('BLARG')) {
    die();
}

$stat = ['HP', 'MP', 'Atk', 'Def', 'Int', 'MDf', 'Dex', 'Lck', 'Spd'];
function basestat($p, $d, $stat, $ext = 0)
{
    $e = calcexp($p, $d);
    $l = calclvl($e);
    if ($l == NAN) {
        return 1;
    }
    switch ($stat) {
    case 0:
        return (pow($p, 0.26) * pow($d, 0.08) * pow($l, 1.11) * 0.95) + 20; //HP
    case 1:
        return (pow($p, 0.22) * pow($d, 0.12) * pow($l, 1.11) * 0.32) + 10; //MP
    case 2:
        return (pow($p, 0.18) * pow($d, 0.04) * pow($l, 1.09) * 0.29) + 2; //Str
    case 3:
        return (pow($p, 0.16) * pow($d, 0.07) * pow($l, 1.09) * 0.28) + 2; //Atk
    case 4:
        return (pow($p, 0.15) * pow($d, 0.09) * pow($l, 1.09) * 0.29) + 2; //Def
    case 5:
        return (pow($p, 0.14) * pow($d, 0.10) * pow($l, 1.09) * 0.29) + 1; //Shl
    case 6:
        return (pow($p, 0.17) * pow($d, 0.05) * pow($l, 1.09) * 0.29) + 2; //Lck
    case 7:
        return (pow($p, 0.19) * pow($d, 0.03) * pow($l, 1.09) * 0.29) + 1; //Int
    case 8:
        return (pow($p, 0.21) * pow($d, 0.02) * pow($l, 1.09) * 0.25) + 1; //Spd
    }
}

function getstats($u, $items = 0)
{
    global $stat;

    $p = $u['posts'];
    $d = (time() - $u['regdate']) / 86400;
    for ($i = 0; $i < 9; $i++) {
        $m[$i] = 1;
    }
    for ($i = 1; $i < 7; $i++) {
        $item = $items[$u['eq'.$i]];
        for ($k = 0; $k < 9; $k++) {
            $is = $item['s'.$stat[$k]];

            if (substr($item['stype'], $k, 1) == 'm') {
                $m[$k] *= $is / 100;
            } else {
                $a[$k] += $is;
            }
        }
    }
    for ($i = 0; $i < 9; $i++) {
        $stats[$stat[$i]] = max(1, floor(basestat($p, $d, $i) * $m[$i]) + $a[$i]);
    }
    $stats['GP'] = coins($p, $d) - $u['spent'];
    $stats['exp'] = calcexp($p, $d);
    $stats['lvl'] = calclvl($stats['exp']);
    $stats['gcoins'] = $u['gcoins'];

    return $stats;
}

function coins($posts, $days)
{
    if ($posts < 0 || $days < 0) {
        return 0;
    } else {
        return floor(pow($posts, 1.3) * pow($days, 0.4) + $posts * 10);
    }
}

function calcexpgainpost($posts, $days)
{
    return floor(1.5 * sqrt($posts * $days));
}
function calcexpgaintime($posts, $days)
{
    if ($posts == 0) {
        return '0.000';
    } else {
        return sprintf('%01.3f', 172800 * (sqrt($days / $posts) / $posts));
    }
}

function calcexpleft($exp)
{
    return calclvlexp(calclvl($exp) + 1) - $exp;
}
function lvlexp($lvl)
{
    return calclvlexp($lvl + 1) - calclvlexp($lvl);
}

function calclvlexp($lvl)
{
    if ($lvl == 1) {
        return 0;
    } else {
        return floor(pow(abs($lvl), 3.5)) * ($lvl > 0 ? 1 : -1);
    }
}

function calclvl($exp)
{
    if ($exp === 'NaN') {
        $lvl = $exp;
    } elseif ($exp >= 0) {
        $lvl = floor(pow($exp, 2 / 7));
        if (calclvlexp($lvl + 1) == $exp) {
            $lvl++;
        } elseif (!$lvl) {
            $lvl = 1;
        }
    } else {
        $lvl = -floor(pow(-$exp, 2 / 7));
    }

    return $lvl;
}

function calcexp($posts, $days, $extra = 0)
{
    if (!$days || !$posts) {
        return $extra;
    } elseif ($posts / $days > 0) {
        return floor($posts * sqrt($posts * $days)) + $extra;
    } else {
        return 'NaN';
    }
}

function getstats2($u)
{
    $user = fetch(
        Query(
            'SELECT
							u.name, u.posts, u.regdate,
							r.*
						FROM
							{users} u
						LEFT JOIN {usersrpg} r ON r.id=u.id
						WHERE u.id = {0}', $u
        )
    );

    $p = $user['posts'];
    $d = (time() - $user['regdate']) / 86400;

    $it = 0;

    $eqitemsR = Query('SELECT * FROM {usersrpg} WHERE id={0}', $u);
    if (NumRows($eqitemsR)) {
        $eq = fetch($eqitemsR);
        $eqitems = Query('SELECT * FROM {items} WHERE id IN ({0c})', $eq);

        while ($item = fetch($eqitems)) {
            $items[$item['id']] = $item;
        }
    }

    $st = getstats($user, $items);
    $st['GP'] += $GPdif;
    if ($st['lvl'] > 0) {
        $pct = 1 - calcexpleft($st['exp']) / lvlexp($st['lvl']);
    }

    return $st;
}

function drawrpglevelbar($totallvlexp, $altsize = 0)
{
    global $theme;

    if ($totallvlexp <= 0) {
        return '&nbsp;';
    }

    if ($altsize != 0) {
        $totalwidth = $altsize;
    } else {
        $totalwidth = 100;
    }

    if ($theme == 'yule') {
        $path = '/gfx/lib/bar/jul/';
    } else {
        $path = '/gfx/lib/bar/acmlm/';
    }

    $expleft = calcexpleft($totallvlexp);
    $expdone = lvlexp(calclvl($totallvlexp));
    $barwidth = $totalwidth - round(($expleft / $expdone) * $totalwidth);
    if ($barwidth < 1) {
        $barwidth = 0;
    }
    if ($barwidth > 0) {
        $baron = '<img src="'.$path.'bar-on.png" width="'.$barwidth.'" height="8" />';
    }
    if ($barwidth < $totalwidth) {
        $baroff = '<img src="'.$path.'bar-off.png" width="'.($totalwidth - $barwidth).'" height="8" />';
    }
    $bar = '<img src="'.$path.'barleft.png" width="2" height="8" />'.$baron.$baroff.'<img src="'.$path.'barright.png" width="2" height="8" />';

    return $bar;
}

function twrite($font, $x, $y, $l, $text)
{
    global $img, $pickfont;
    $text .= '';
    if (strlen($text) < $l) {
        $x += ($l - strlen($text));
    }

    if ($pickfont == 'slim') {
        for ($i = 0; $i < strlen($text); $i++) {
            imagecopy($img, $font, $i * 5 + $x, $y, (ord($text[$i]) % 16) * 8, floor(ord($text[$i]) / 16) * 8, 6, 8);
        }
    } else {
        for ($i = 0; $i < strlen($text); $i++) {
            imagecopy($img, $font, $i * 8 + $x, $y, (ord($text[$i]) % 16) * 8, floor(ord($text[$i]) / 16) * 8, 8, 8);
        }
    }
}

function fontc($r1, $g1, $b1, $r2, $g2, $b2, $r3, $g3, $b3)
{
    global $pickfont;

    $font = imagecreatefrompng(__DIR__.'/font/'.$pickfont.'.png');
    imagecolortransparent($font, 1);
    imagecolorset($font, 6, $r1, $g1, $b1);
    imagecolorset($font, 5, ($r1 * 2 + $r2) / 3, ($g1 * 2 + $g2) / 3, ($b1 * 2 + $b2) / 3);
    imagecolorset($font, 4, ($r1 + $r2 * 2) / 3, ($g1 + $g2 * 2) / 3, ($b1 + $b2 * 2) / 3);
    imagecolorset($font, 3, $r2, $g2, $b2);
    imagecolorset($font, 0, $r3, $g3, $b3);

    return $font;
}
function box($x, $y, $w, $h)
{
    global $img, $c;

    imagerectangle($img, $x + 0, $y + 0, $x + $w - 1, $y + $h - 1, $c['bxb0']);
    imagerectangle($img, $x + 1, $y + 1, $x + $w - 2, $y + $h - 2, $c['bxb3']);
    imagerectangle($img, $x + 2, $y + 2, $x + $w - 3, $y + $h - 3, $c['bxb1']);
    imagerectangle($img, $x + 3, $y + 3, $x + $w - 4, $y + $h - 4, $c['bxb2']);
    imagerectangle($img, $x + 4, $y + 4, $x + $w - 5, $y + $h - 5, $c['bxb0']);
    for ($i = 5; $i < $h - 5; $i++) {
        $n = (1 - $i / $h) * 100;
        imageline($img, $x + 5, $y + $i, $x + $w - 6, $y + $i, $c[$n]);
    }
}

function sqlexpval() {
	return 'posts*pow(posts*('.time().'-regdate)/86400,1/2)';
}
function sqlexp() {
	return sqlexpval().' exp';
}

