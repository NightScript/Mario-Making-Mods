<?php
    define('BLARG', '1');
    require __DIR__.'/../lib/common.php';

if ($_GET['u']) {
    $u = (int) $_GET['u'];
} else {
    if (!$loguserid) {
        die('No user specified! Please either specify a user, or log onto the board.');
    } else {
        $u = $loguserid;
    }
}
    checknumeric($u);

    $user = fetch(
        Query(
            'SELECT u.posts, u.regdate, r.*
						 FROM users u
						 LEFT JOIN usersrpg r ON r.id=u.id
						 WHERE u.id = {0}', $u
        )
    );

    $p = $user['posts'];
    $d = (time() - $user['regdate']) / 86400;

    $it = $_GET['it'];
    checknumeric($it);

    $eqitemsR = Query('SELECT * FROM {usersrpg} WHERE id={0}', $user['id']);
    if (NumRows($eqitemsR)) {
        $eq = fetch($eqitemsR);
        $eqitems = Query('SELECT * FROM {items} WHERE id IN ({0c}) OR id = {1}', $eq, $it);

        while ($item = fetch($eqitems)) {
            $items[$item['id']] = $item;
        }
    }

    $ct = $_GET['ct'];
    if ($ct) {
        $GPdif = floor($items[$user['eq'.$ct]]['coins'] * 0.6) - $items[$it]['coins'];
        $user['eq'.$ct] = $it;
    }

    $st = getstats($user, $items);
    $st['GP'] += $GPdif;
    if ($st['lvl'] > 0) {
        $pct = 1 - calcexpleft($st['exp']) / lvlexp($st['lvl']);
    }

    if ($urlfont && file_exists(__DIR__.'/lib/font/'.$_GET['font'].'.png')) {
        $pickfont = $urlfont;
    } else {
        $pickfont = 'acmlmboard';
    }

    $img = imagecreate(520, 88);
    $urlcolor = $_GET['color'];
    if ($urlcolor && file_exists(__DIR__.'/lib/themes/'.$urlcolor.'.php')) {
        include __DIR__.'/lib/themes/'.$urlcolor.'.php';
    } else {
        include __DIR__.'/lib/themes/default.php';
    }

    box(0, 40, 144, 48, 0);
    box(152, 40, 104, 48, 0);
    box(264, 0, 256, 72, 0);

    box(0, 0, 256, 32, 0);
    twrite($c['font']['B'],  8,  8,  0, 'HP:      /');
    twrite($c['font']['R'], 16,  8, 28, $st['HP'] - $user['hp']);
    twrite($c['font']['Y'], 80,  8, 20, $st['HP']);
    twrite($c['font']['B'],  8, 16,  0, 'MP:      /');
    twrite($c['font']['R'], 16, 16, 28, $st['MP'] - $st['mp']);
    twrite($c['font']['Y'], 80, 16, 20, $st['MP']);

    for ($i = 2; $i < 9; $i++) {
        twrite($c['font']['B'], 272, (-1 + $i) * 8, 0, $stat[$i].':');
        twrite($c['font']['Y'], 296, (-1 + $i) * 8, 48, $st[$stat[$i]]);
    }

    twrite($c['font']['B'],   8, 48,  0, 'Level');
    twrite($c['font']['Y'], 104, 48, 32, $st['lvl']);
    twrite($c['font']['B'],   8, 64,  0, 'EXP:');
    twrite($c['font']['Y'],  64, 64, 72, $st['exp']);
    twrite($c['font']['B'],   8, 72,  0, 'Next:');
    twrite($c['font']['Y'],  64, 72, 72, calcexpleft($st['exp']));

    twrite($c['font']['B'], 160, 48, 0, 'Coins:');
    twrite($c['font']['Y'], 160, 64, 0, chr(0));
    twrite($c['font']['Y'], 176, 64, 60, $st['GP']);

    $sc[1] = 1;
    $sc[2] = 5;
    $sc[3] = 25;
    $sc[4] = 100;
    $sc[5] = 250;
    $sc[6] = 500;
    $sc[7] = 1000;
    $sc[8] = 2500;
    $sc[9] = 5000;
    $sc[10] = 10000;
    $sc[11] = 100000;
    $sc[12] = 1000000;
    $sc[13] = 10000000;
    $sc[14] = 100000000;
    $sc[15] = 1000000000;

    bars_sig();

    header('Content-type:image/png');
    imagepng($img);
    imagedestroy($img);

    function bars_sig()
    {
        global $st, $img, $c, $sc, $pct, $stat;

        for ($s = 1; @(max($st['HP'], $st['MP']) / $sc[$s]) > 113; $s++) {
        }
        if (!$sc[$s]) {
            $sc[$s] = 1;
        }
        imagefilledrectangle($img, 137, 9, 136 + $st['HP'] / $sc[$s], 15, $c['bxb0']);
        imagefilledrectangle($img, 137, 17, 136 + $st['MP'] / $sc[$s], 23, $c['bxb0']);
        imagefilledrectangle($img, 136, 8, 135 + $st['HP'] / $sc[$s], 14, $c['bar'][$s]);
        imagefilledrectangle($img, 136, 16, 135 + $st['MP'] / $sc[$s], 22, $c['bar'][$s]);

        for ($i = 2; $i < 9; $i++) {
            $st2[$i] = $st[$stat[$i]];
        }
        for ($s = 1; @(max($st2) / $sc[$s]) > 161; $s++) {
        }
        if (!$sc[$s]) {
            $sc[$s] = 1;
        }
        for ($i = 2; $i < 9; $i++) {
            imagefilledrectangle($img, 361, -7 + $i * 8, 360 + $st[$stat[$i]] / $sc[$s], -1 + $i * 8, $c['bxb0']);
            imagefilledrectangle($img, 360, -8 + $i * 8, 359 + $st[$stat[$i]] / $sc[$s], -2 + $i * 8, $c['bar'][$s]);
        }

        $e1 = 128 * $pct;
        imagefilledrectangle($img, 8, 58, 7 + 128, 60, $c['bxb0']);
        imagefilledrectangle($img, 8, 58, 7 + 128, 60, $c['barE2']);
        if ($e1) {
            imagefilledrectangle($img, 8, 58, 7 + $e1, 60, $c['barE1']);
        }
    }
