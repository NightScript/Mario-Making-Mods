<?php
    define('BLARG', '1');
    require __DIR__.'/../lib/common.php';

if ($_GET['u']) {
    $u = (int) $_GET['u'];
} else {
    if (!$loguserid) {
        die('No user specified! Please either specify a user, or log onto the board.');
    } else {
        $u = $loguserid;
    }
}

    checknumeric($u);

    $regdaye = Fetch(Query('SELECT regdate FROM {users} WHERE id = {0}', $u));
    $regdate = $regdaye['regdate'];

    $vd = date('m-d-y', $regdate);
    $dd = mktime(0, 0, 0, substr($vd, 0, 2), substr($vd, 3, 2), substr($vd, 6, 2));
    $dd2 = mktime(0, 0, 0, substr($vd, 0, 2), substr($vd, 3, 2) + 1, substr($vd, 6, 2));

    $regday = floor($regdate / 86400);    // Day number
    $regdate_ts = $regday * 86400;                        // Day timestamp

    $days = floor((time() - $regdate) / 86400); // Account age (in days)
if (!$days) {
    die('Account age too low.');
}

    $postdts = Query(
        "
		SELECT
			FROM_unixtime(date, '%Y%m%d') ymd,
			FLOOR(date / 86400) day,
			COUNT(*) c, 
			max(num) m
		FROM {posts} 
		WHERE user = {0} 
		GROUP BY ymd 
		ORDER BY ymd", $u
    );

    if (!$postdts) {
        die('No posts for this user.');
    }

    while ($n = Fetch($postdts)) {
        $p[$n[$d]] = $n['c'];
        $t[$n[$d]] = $n['m'];
    }

    // Place the results into an array with a saner format [<day number> => <count>]
    foreach ($postdts as $k_day => $posts) {
        $day = $k_day - $regday;
        $postdb[$day] = $posts;
    }

    for ($i = 0; $regdate + $i * 86400 < time(); $i++) {
        $ps = Query(
            '
				SELECT
					COUNT(*),
					max(num)
				FROM {posts}
				WHERE user = {3}
				AND date >= {0} + {1} * 86400
				AND date < {2} + {1} * 86400', $regdate, $i, $dd2, $u
        );
        $p[$i] = Result($ps, 0, 0);
        $t[$i] = Result($ps, 0, 1);
    }
    unset($postdts, $k_day, $posts);

    $maxposts = max($p);
    $m = $maxposts;
    $img = imagecreatetruecolor($days, $maxposts);

    if ($_GET['theme'] == 'Jul') {
        $c['bg'] = imagecolorallocate($img, 0, 0, 0);
        $c['bg1'] = imagecolorallocate($img, 0, 0, 80); // Month colors
        $c['bg2'] = imagecolorallocate($img, 0, 0, 130); //
        $c['bg3'] = imagecolorallocate($img, 80, 80, 250); // (New year)
        $c['mk1'] = imagecolorallocate($img, 110, 110, 160); // Horizontal Rulers
        $c['mk2'] = imagecolorallocate($img, 70, 70, 130); //
        $c['bar'] = imagecolorallocate($img, 240, 190, 40); // Post COUNT bar
        $c['pt1'] = imagecolorallocate($img, 250, 250, 250); // Average
        $c['pt2'] = imagecolorallocate($img, 240, 230, 220); // Average (over top of post bar)
    } else {
        imagesavealpha($img, true);
        $c['bk'] = imagecolorallocatealpha($img, 0, 0, 0, 127);
        $c['bg1'] = imagecolorallocatealpha($img, 0, 0, 0, 127);
        $c['bg2'] = imagecolorallocatealpha($img, 0, 0, 0, 100);
        $c['bg3'] = imagecolorallocatealpha($img, 0, 0, 0, 64);
        $c['mk1'] = imagecolorallocate($img, 110, 110, 160);
        $c['mk2'] = imagecolorallocate($img, 70, 70, 130);
        $c['bar'] = imagecolorallocatealpha($img, 250, 190, 40, 64);
        $c['pt'] = imagecolorallocate($img, 250, 190, 40);
        imagefill($img, 0, 0, $c['bk']);
    }

    for ($i = 0; $i < $days; $i++) {
        $md = date('m-d', $regdate_ts + $i * 86400);

        if ($md == '01-01') { // New year?
            $num = 3;
        } else {
            $num = substr($md, 0, 2) % 2 + 1;
        } // Alternate between months

        imageline($img, $i, $maxposts, $i, 0, $c['bg'.$num]);
    }

    if ($_GET['theme'] == 'Jul') {
        // Draw the horizontal rulers (in offsets of Y 50px; alternate lines between 2 colors)
        for ($y = 50, $ct = 1; $y <= $maxposts; $y += 50, $ct++) {
            imageline($img, 0, $maxposts - $y, $days, $maxposts - $y, (($ct & 1) ? $c['mk2'] : $c['mk1']));
        }

        $total = 0;
        for ($i = 0; $i < $days; $i++) {
            // Draw the number of posts (as vertical bars starting from the bottom)
            if (isset($postdb[$i])) {
                imageline($img, $i, $maxposts, $i, $maxposts - $postdb[$i], $c['bar']);
                $total += $postdb[$i];
            } else {
                $postdb[$i] = 0;
            }
            // Draw post average in relation to time
            $avg = $total / ($i + 1);
            imagesetpixel($img, $i, $maxposts - $avg, (($postdb[$i] >= $avg) ? $c['pt2'] : $c['pt1']));
        }
    } else {
        for ($i = 0; $i < $days; $i++) {
            imageline($img, $i, $m, $i, $m - $p[$i], $c['bar']);
            imagesetpixel($img, $i, $m - $t[$i] / ($i + 1), $c['pt']);
        }
    }

    header('Content-type:image/png');
    imagepng($img);
    imagedestroy($img);
