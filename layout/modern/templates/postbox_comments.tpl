<table class="table table-bordered table-striped">
	<thead><tr><th>Comments</th></tr></thead>
	<tbody>
		{foreach $posts as $post}
			<tr class="cell1"><td>
				<div style="display:inline-block;">{$post.userlink} Posted on {$post.formattedDate}</div>
				<div style="display:inline-block;float:right;" class="btn-group btn-group-sm">
					<a class="btn btn-primary" role="button" title="Link this post" href="{actionLink page='post' id=$post.id}">
						<i class="fa fa-link"></i>
					</a>
					{if $post.links.twitter}
						<a class="btn btn-primary" role="button" href='{$post.links.twitter}' title="Share this post on Twitter" target="_blank">
							<i class="fa fa-twitter"></i>
						</a>
					{/if}
					{if $post.links.quote}
						<a class="btn btn-primary" role="button" href="{$post.links.quote}" title="Quote this post">
							<i class="fa fa-quote-left"></i>
						</a>
					{/if}
					{if $post.links.edit}
						<a class="btn btn-primary" role="button" href="{$post.links.edit}" title="Edit this post">
							<i class="fa fa-pencil"></i>
						</a>
					{/if}
					{if $post.links.delete}
						<a class="btn btn-primary" role="button" href="{$post.links.delete}" {$post.links.deleteclick} title="Delete this post">
							<i class="fa fa-remove"></i>
						</a>
					{/if}
					{if $post.links.wipe}
						<a class="btn btn-primary" role="button" href="{$post.links.wipe}" onclick="if(!confirm('Really wipe this post? You will not be able to retrieve it afterwards'))return false;" title="Wipe this post">
							<i class="fa fa-bomb"></i>
						</a>
					{/if}
					{if $post.links.report}
						<a class="btn btn-primary" role="button" href="{$post.links.report}" title="Report this post">
						<i class="fa fa-flag"></i></a>
					{/if}
					{foreach $post.links.extra as $link}
						{$link}
					{/foreach}
					{if $post.id}<button class="btn btn-primary">#{$post.id}</button>{/if}
					{if $post.ip}<button class="btn btn-primary">{$post.ip}</button>{/if}
				</div>
			<div style="height: 4px;"></div>
			<div style="padding-left: 12px;">{$post.contents}</div>
		</td></tr>{/foreach}
	</tbody>
</table>