	<table class="table table-bordered table-striped" id="post{$post.id}">
		<tr class="cell0">
			<td>
				<small>
					<span id="meta_{$post.id}">
					{if $post.type == $smarty.const.POST_SAMPLE}
						Preview
					{else}
						Posted on {$post.formattedDate} by {$post.userlink}
						{if $post.revdetail} ({$post.revdetail}){/if}
					{/if}
					</span>
					<span style="text-align:left; display: none;" id="dyna_{$post.id}">
						blarg
					</span>
				</small>
			</td>
		</tr>
		<tr class="cell1">
			<td id="post_{$post.id}">
				{$post.contents}
			</td>
		</tr>
		{if count($post.links)>0}
		<tr class="cell0">
			<td class="right">
				<div class="btn-group btn-group-sm">
				{if $post.type == $smarty.const.POST_DEPOT_MAIN}
					{foreach $post.links as $link}
						<a class="btn btn-{if $link.btn}{$link.btn}{else}primary{/if}" role="button" {if $link.onclick}onclick="{$link.onclick}"{/if} {if $link.link}href="{$link.link}"{/if} title="{$link.text}">
								{if $link.icon}<i class="fa fa-{$link.icon}"></i>{/if}
								{if $link.showtext}{$link.showtext}{/if}
						</a>
					{/foreach}
					{foreach $post.links_extra as $link}
						{$link}
					{/foreach}
				{else if $post.type == $smarty.const.POST_DELETED_SNOOP}
					Post deleted
					{if $post.links.undelete}{$post.links.undelete}{/if}
					{if $post.links.close}{$post.links.close}{/if}
				{/if}
					{if $post.id}<button class="btn btn-primary">#{$post.id}</button>{/if}
					{if $post.ip}<button class="btn btn-primary">{$post.ip}</button>{/if}
				</div>
			</td>
		</tr>
		{/if}
	</table>