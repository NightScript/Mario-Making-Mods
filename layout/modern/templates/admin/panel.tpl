	<table class="table table-striped table-bordered">
		<thead><tr><th>Administration tools</th></tr>
		<tbody><tr style="text-align: center;"><td>
			{foreach $adminLinks as $link=>$text} <a href="{$link|escape}" class="btn btn-default">{$text}</a> {/foreach}
		</td></tr></tbody>
	</table>

	<table class="table table-striped table-bordered ">
		<thead><tr><th colspan="2">Information</th></tr></thead>
		<tbody>
			{foreach $adminInfo as $label=>$contents}
				<tr>
					<td style="text-align: center;">{$label}</td>
					<td>{$contents}</td>
				</tr>
			{/foreach}
		</tbody>
	</table>