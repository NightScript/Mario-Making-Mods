	<table class="table table-bordered table-striped">
		<tr class="header1">
			<th class="center" style="width:20px;"></th>
			{if $qid}<th style="width:100px;"></th>{/if}
			<th class="center">Forum</th>
			<th class="center" style="width:60px;">Posts</th>
			<th class="center" style="width:100px;">Forum total</th>
			<th class="center" style="width:60px;">Percentage</th>
		</tr>
		{foreach $rows as $row}
			<tr>
				<td class="cell2 center">{$row.i}</td>
				{if $qid}<td class="cell2 center">{$row.viewall}</td>{/if}
				<td class="cell1">{$row.link}</td>
				<td class="cell1 center"><b>{$row.cnt}</b></td>
				<td class="cell1 center">{$row.numposts}</td>
				<td class="cell1 center">{$row.percentage}%</td>
			</tr>
		{/foreach}
	</table>

	<div class="smallFonts margin">
		Total: {$userposts} posts
	</div>