	<div class="alert alert-info alert-dismissible fade show" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
		{$annc.new} {$annc.poll}{$annc.links.tags}<a href="{$annc.links.link}" class="alert-link">{$annc.links.name}</a> &mdash; Posted by {$annc.user} on {$annc.date}
	</div>