	{if $pagelinks}{$pagelinks}{/if}

	<div class="grid-container">
		{foreach $posts as $post}
			<table class="table table-bordered table-striped" style="margin: auto; padding: 20px;">
				<thead>
					<tr><th style="font-size:125%;">{$post.title}</th></tr>
				</thead>
				<tbody>
					<tr><td>
						{if $post.description}{$post.description}<br><br>{/if}
						{if $post.screenshots}{$post.screenshot}<br><br>{/if}
						{if $post.download}{$post.download}<br><br>{/if}
						<small>	Posted on {$post.formattedDate} by {$post.userlink}</small>
					</td></tr>
					<tr><td>{$post.comments}. {$post.replylink}</td></tr>
				</tbody>
			</table>
		{/foreach}
	</div>
	
	{if $pagelinks}{$pagelinks}{/if}