	<table class="table table-bordered">
		<tr style="overflow: auto;">
			<td>
				<div class="row">
					<div class="col-sm-4"></div>
					<div class="col-sm-4" style="text-align: center;">
						{plural num=$stats.numThreads what='thread'} and {plural num=$stats.numPosts what='post'} total<br>
						{plural num=$stats.newPostToday what='post'} today, {$stats.newPostLastHour} last hour, and {$stats.newPostLastWeek} for the past week<br>
						{plural num=$stats.newThreadToday what='active thread'} today, {$stats.newPostLastHour} last hour, and {$stats.newThreadLastWeek} for the past week
					</div>
					<div class="col-sm-4" style="text-align: right;">
						{if $stats.numUsers}
							{plural num=$stats.numUsers what='registered user'}, {$stats.numActive} active ({$stats.pctActive}%)<br>
							Newest user: {$stats.lastUserLink}
						{else}
							No registered users
						{/if}
					</div>
				</div>
			</td>
		</tr>
	</table>
