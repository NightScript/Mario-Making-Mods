	{foreach $settingfields as $cat=>$settings}
		{if $settings}
			<table class="table table-bordered table-striped">
				<thead><tr>
					<th{if !$htmlfield} colspan=2{/if}>
						{if $cat}{$cat}{else}Settings{/if}
					</th>
				</tr></thead>
				<tbody>
					{foreach $settings as $setting}
						{if $setting.field}
							<tr>
								{if $htmlfield}<td>{$setting.field}</td>
								{else}
									<td class="center" style="width: 20%;">{$setting.name}</td>
									<td>{$setting.field}</td>
								{/if}
							</tr>
						{/if}
					{/foreach}
				</tbody>
			</table>
		{/if}
	{/foreach}

	<div class="center btn-group">{$fields.btnSaveExit}{$fields.btnSave}</div>