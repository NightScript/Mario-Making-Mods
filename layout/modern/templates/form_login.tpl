	<table class="table table-bordered table-striped">
		<thead><tr><th>Log in</th></tr></thead>
		<tbody>
			<tr><td>
				<div class="input-group">
					<div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-user-circle-o fa-fw"></i></span></div>
					{$fields.username}
				</div>
			</td></tr>
			<tr><td>
				<div class="input-group">
					<div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-key fa-fw"></i></span></div>
					{$fields.password}
				</div>
				<small>{$fields.btnForgotPass}</small>
			</td></tr>
			<tr><td>
				{$fields.btnLogin} {$fields.session}
				<input type="hidden" name="action" value="login">
			</td></tr>
		</tbody>
	</table>