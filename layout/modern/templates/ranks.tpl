
	<table class="table table-bordered table-striped">
		<thead><tr>
			<th>Rank</th>
			<th>Posts</th>
			<th colspan=2>Users</th>
		</tr></thead>

		<tbody>
			{foreach $ranks as $rank}
				<tr>
					<td>{$rank.rank}</td>
					<td class="center">{$rank.posts}</td>
					<td class="center">{$rank.numUsers}</td>
					<td>{$rank.users}</td>
				</tr>
			{/foreach}
		</tbody>
		
	</table>
