	<table class="table table-striped" id="post{$post.id}">
		<tr>
			<td class="side" rowspan=2 style="width: 250px;">
				<span class="card">
					{if $post.sidebar.avatar}{$post.sidebar.avatar}{/if}
					<div class="card-body">
						<span class="card-title">{$post.userlink}{$post.sidebar.isonline}</span>
						{if $post.sidebar.title}<br><small class="card-subtitle">{$post.sidebar.title}</small>{/if}
					</div>
					<ul class="list-group list-group-flush"><li class="list-group-item">{$post.sidebar.group}</li></ul>
					<div class="card-body">
						Since: {$post.sidebar.since}<br>
						{if $post.sidebar.from}From: {$post.sidebar.from}<br>{/if}
						{foreach $post.sidebar.extra as $item}
							{if $item}{$item}<br>{/if}
						{/foreach}
					</div>
					<div class="card-footer d-flex justify-content-between">
						{foreach $post.sidebarlinks as $sblink}
							<a href="{$sblink.link}" title="{$sblink.text}"><i class="fa fa-{$sblink.icon}"></i></a>
						{/foreach}
					</div>
				</span>
			</td>
			<td class="post{if $post.fulllayout} mainbar{$post.u_id}{else if $post.haslayout} haslayout{/if}" id="post_{$post.id}">
				{$post.contents}
			</td>
		</tr>
		<tr><td class="meta right">
			<div style="float: left;" id="meta_{$post.id}">
				{if $post.type == $smarty.const.POST_SAMPLE}
					Preview
				{else}
					{if $post.type == $smarty.const.POST_PM}Sent{else}Posted{/if} on {$post.formattedDate}
					{if $post.threadlink} in {$post.threadlink}{/if}
					{if $post.revdetail} ({$post.revdetail}){/if}
				{/if}
			</div>
			<div style="float: left; text-align:left; display: none;" id="dyna_{$post.id}">
				blarg
			</div>
			<div class="btn-group btn-group-sm">
				{if $post.type == $smarty.const.POST_NORMAL}
					{foreach $post.links as $link}
						<a 	class="btn btn-{if $link.btn}{$link.btn}{else}primary{/if}"
							role="button"
							{if $link.onclick}onclick="{$link.onclick}"{/if}
							{if $link.link}href="{$link.link}"{/if}
							title="{$link.text}"
						>
							{if $link.icon}<i class="fa fa-{$link.icon}"></i>{/if}
							{if $link.showtext}{$link.showtext}{/if}
						</a>
					{/foreach}
					{foreach $post.links_extra as $link}
						{$link}
					{/foreach}
				{else if $post.type == $smarty.const.POST_DELETED_SNOOP}
					Post deleted
					{if $post.links.undelete}{$post.links.undelete}{/if}
					{if $post.links.close}{$post.links.close}{/if}
				{/if}
				{if $post.id}<button class="btn btn-primary">#{$post.id}</button>{/if}
				{if $post.ip}<button class="btn btn-primary">{$post.ip}</button>{/if}
			</div>
		</td></tr>
	</table>