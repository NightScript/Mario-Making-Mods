	<table class="table table-bordered table-striped">
		<tr><th>Send a Private Message</th></tr></thead>
		<tbody>
			<tr><td>
				{$fields.to}

				<datalist id="nameList">
					{foreach $mySQLnames as $mySQLname}
						<option value="{$mySQLname}">
					{/foreach}
				</datalist>

				<small>You can specify up to {$maxRecipients} recipients. Separate their names with a semicolon.</small>
			</td></tr>
			<tr><td>{$fields.title}</td></tr>
			<tr><td>{$fields.text}</td></tr>
			<tr><td class="cell2">
				<div class="btn-group">
					{$fields.btnSend}
					{$fields.btnPreview}
					{$fields.btnSaveDraft}
					{if $draftMode}{$fields.btnDeleteDraft}{/if}
				</div>
			</td></tr>
		</tbody>
	</table>
