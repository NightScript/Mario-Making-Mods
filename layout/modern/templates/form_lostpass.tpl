	<table class="table table-striped table-bordered">
		<thead><tr><th>Request password reset</th></tr></thead>
		<tbody>
			<tr><td><div class="input-group margin-bottom-sm">
					<div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-user-circle-o fa-fw"></i></span></div>
					{$fields.username}
			</div></td></tr>
			<tr><td>
				<div class="input-group margin-bottom-sm">
					<div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-envelope-o fa-fw"></i></span></div>
					{$fields.email}
				</div>
				<div class="input-group margin-bottom-sm">
					<div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-envelope-o fa-fw"></i></span></div>
					{$fields.email2}
				</div>
			</td></tr>
			<tr><td>{$fields.btnSendReset}</td></tr>
		</tbody>
		<tfoot><tr><td><small>
			If automated password reset fails, you can try contacting one of the board's administrators.
		</td></tr></tfoot>
	</table>
