<div style="min-height:77vh;">
	<div id="boardHeader">
		<span id="userArea" style="float: right; text-align: right;">
			{if $loguserid}
				
			{else}
				<li class="nav-item dropdown">
					{if $loguserid}
						<a class="nav-link nav-dropdown dropdown-toggle" id="navbardrop" data-toggle="dropdown" href="{$logusertext.link}">
							{$logusertext.text} <span class="caret"></span>
						</a>
					{else}
						<a class="nav-link nav-dropdown dropdown-toggle" id="navbardrop" data-toggle="dropdown">
							Welcome Guest <span class="caret"></span>
						</a>
					{/if}
					<div class="dropdown-menu dropdown-menu-right dropdown-info">
						{foreach $layout_userpanel as $url=>$texts}
							<a class="dropdown-item" href="{$url|escape}">{$texts.text}</a>
							{if $texts.devider}
								<div class="dropdown-divider"></div>
							{/if}
						{/foreach}
						{if $loguserid}
							<div class="dropdown-divider"></div>
							<a class="dropdown-item" onclick="$('#logout').submit(); return false;">Log out</a>
						{/if}
					</div>
				</li>
			{/if}
		</span>

		<div class="row content" class="display: inline-block;">
			<div class="col-sm-6">
				<a href="{actionLink page='home'}">
					<img id="theme_banner" src="{$layout_logopic}" alt="{$boardname}" title="{$boardname}" style="max-width:100%; height: 150px; padding: 10px;">
				</a>
			</div>
			<div class="col-sm-6">
				<div style="display: inline-block; vertical-align: middle;">
					<div class="navitab" style="text-align: center;">
						{foreach $headerlinks as $url=>$texts}
							{if $texts.dropdown}
								<span class="navbar-items dropdown {foreach $texts.link as $link}{if $currentPage === $link}navbar-active{break}{/if}{/foreach}">
									<a class="navbar-links dropdown-toggle" data-toggle="dropdown" href="{$url|escape}">
										{$texts.text}<span class="caret"></span>
									</a>
									<ul class="dropdown-menu">
										{foreach $texts.dropdown as $ddurl=>$ddtexts}
											<li class="dropdown-item"><a class="dropdown-link" href="{$ddurl|escape}">{$ddtexts.text}</a></li>
											{if $ddtexts.devider}
												<li class="dropdown-divider"></li>
											{/if}
										{/foreach}
									</ul>
								</span>
							{else}
								<span class="navbar-items {foreach $texts.link as $link}{if $currentPage === $link}navbar-active{break}{/if}{/foreach}">
									<a class="navbar-items" href="{$url|escape}">{$texts.text}</a>
								</span>
							{/if}
						{/foreach}
					</div>
				</div>
				<!--<nav class="navbar navbar-expand-md bg-dark navbar-dark stick" style="margin-bottom: 0px;">
					<input type="checkbar" id="navbar-toggle-cbox">
					<label for="navbar-toggle-cbox" class="navbar-toggler hidden-sm-up" type="button" data-toggle="collapse" data-target="#myNavbar" aria-controls="navbar-header">
						<span class="navbar-toggler-icon"></span>
					</label>
					<div class="collapse navbar-collapse" id="myNavbar">
						<ul class="mr-auto navbar-nav">
							{foreach $headerlinks as $url=>$texts}
								{if $texts.dropdown}
									<li class="nav-item dropdown {foreach $texts.link as $link}{if $currentPage === $link}active{/if}{/foreach}">
										<a class="nav-link nav-dropdown dropdown-toggle" id="navbardrop" data-toggle="dropdown" href="{$url}">
											{$texts.text} <span class="caret"></span>
										</a>
										<ul class="dropdown-menu">
											{foreach $texts.dropdown as $ddurl=>$ddtexts}
												<li class="dropdown-item"><a class="dropdown-link" href="{$ddurl|escape}">{$ddtexts.text}</a></li>
												{if $ddtexts.devider}
													<li class="dropdown-divider"></li>
												{/if}
											{/foreach}
										</ul>
									</li> 
								{else}
									 <li class="nav-item {foreach $texts.link as $link}{if $currentPage === $link}active{/if}{/foreach}">
										<a class="nav-link" href="{$url|escape}">{$texts.text}</a>
									</li>
								{/if}
							{/foreach}
						</ul>
						<ul class="navbar-nav ml-auto">
							{if $loguserid}
								{$numnotifs=count($notifications)}
								<li id="notifMenuContainer" class="nav-item dropdown {if $numnotifs}hasNotifs{else}noNotif{/if}">
									<a id="notifMenuButton" class="nav-link dropdown-toggle" data-toggle="dropdown">
										Alerts
										<span id="notifCount" class="badge {if $numnotifs}badge-danger{else}badge-secondary{/if}">{$numnotifs}</span>
										<span class="caret"></span>
									</a>
									<ul class="dropdown-menu dropdown-menu-right dropdown-info">
										{if $numnotifs}
											{foreach $notifications as $notif}
												<li id="notifText" class="dropdown-item waves-effect waves-light">
													{$notif.text}<br>
													<small>{$notif.formattedDate}</small>
												</li>
											{/foreach}
										{else}
											<li id="notifText" class="dropdown-item waves-effect waves-light">You have no new Alerts.</li>
										{/if}
									</ul>
								</li>
							{/if}
						</ul>
					</div>
				</nav>-->
			</div>
		</div>
	</div>

	{capture "breadcrumbs"}{if $layout_crumbs || $layout_actionlinks || $layout_dropdown}
		<nav aria-label="breadcrumb" class="navbar navbar-expand-sm bg-light navbar-light" id="breadBar" style="margin-bottom: 0px; border-radius: 0px;">
			{if $layout_crumbs && count($layout_crumbs)}
				<ol class="breadcrumb mr-auto navbar-nav d-none d-md-inline-flex">
							{foreach $layout_crumbs as $url=>$text}
								<li class="breadcrumb-item"><a class="breadcrumb-link" href="{$url|escape}">{$text}</a></li>
							{/foreach}
				</ol>
			{/if}
			{if ($layout_actionlinks && count($layout_actionlinks)) || ($layout_dropdown && count($layout_dropdown))}
				<ul class="ml-auto navbar-nav">
					{if $layout_actionlinks && count($layout_actionlinks)}
						{foreach $layout_actionlinks as $url=>$texts}
							<li	class="nav-item {if $texts.active}active{/if}">
								<a {if $url}href="{$url|escape}"{/if}
									class="nav-link"
									{if $texts.onclick} onclick="{$texts.onclick}"{/if}>
									{$texts.text}
								</a>
							</li>
						{/foreach}
					{/if}
					{if $layout_dropdown && count($layout_dropdown)}
						{foreach $layout_dropdown as $cat=>$links}
							<li class="nav-item dropdown">
								<a class="nav-link dropdown-toggle" id="navbardrop" data-toggle="dropdown">
									{$cat} <span class="caret"></span>
								</a>
								<div class="dropdown-menu">
									{foreach $links as $url=>$texts}
										<a class="dropdown-item {if $texts.active}active{/if}" href="{$url|escape}">{$texts.text}</a>
									{/foreach}
								</div>
							</li>
						{/foreach}
					{/if}
				</ul>
			{/if}
		</nav>
	{/if}{/capture}

	<div id="onlineUserBar" style="width: 100%; text-align:center;">
		{$layout_onlineusers}
		{if $layout_birthdays}{$layout_birthdays}{/if}
	</div>
	{$smarty.capture.breadcrumbs}<br>

	<div class="container-fluid text-center">    
		<div class="row content">
			{if $sidebar}
				<div class="col-lg-2 col-md-3 col-sm-4 sidenav">
					{$sidebar}
				</div>
			{/if}
			<div class="{if $sidebar}col-lg-10 col-md-9 col-sm-8{else}col-sm-12{/if} text-left">
				{$layout_contents}
			</div>
		</div>
	</div>
</div>


<div class="breadBarBottom">{$smarty.capture.breadcrumbs}</div>
<div class="footer" style="overflow:auto;">
	<div class="container-fluid">
		<div style="float: left;">{$layout_credits}</div>
		<div style="float: right;">
			<div class="btnGroupLinks">
				<a href="{actionLink page='memberlist'}" class="btn btn-link">Member List</a>
				<a href="{actionLink page='records'}" class="btn btn-link">Records</a>
				<a href="{actionLink page='calendar'}" class="btn btn-link">Calendar</a>
				<a href="{actionLink page='privacypolicy'}" class="btn btn-link">Privacy policy</a>
				{if HasPermission('admin.viewadminpanel')}
					<a href="{actionLink page='admin'}" class="btn btn-link">Admin</a>
				{/if}
			</div>
		</div>
	</div>
</div>