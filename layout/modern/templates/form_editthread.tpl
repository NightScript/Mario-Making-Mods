	<table class="table table-striped table-bordered">
		<thead><tr><th>Edit thread</th></tr></thead>
		<tbody>
			{if $fields.title}<tr><td id="threadTitleContainer">{$fields.title}</td></tr>{/if}
			{if $fields.description}<tr><td>{$fields.description}</td></tr>{/if}
			{if $fields.icon}<tr><td>{$fields.icon}</td></tr>{/if}
			{if $fields.closed}<tr><td>{$fields.closed}</td></tr>{/if}
			{if $fields.sticky}<tr><td>{$fields.sticky}</td></tr>{/if}
			{if $fields.displaytype}<tr><td>
				<b>Thread Display</b><br>
				{$fields.displaytype}<br>
				Reddit: Main post always shown. Categorized by stars on post & last post date, descending.<br>
				Threaded: Post organized by last post date, regardless of stars
			</td></tr>{/if}
			{if $fields.forum}<tr><td>{$fields.forum}</td></tr>{/if}
		</tbody>
		{if !$fields.screenshot}<tfoot><tr><td>{$fields.btnEditThread}</td></tr></tfoot>{/if}
	</table>

	{if $fields.screenshot}
		<table class="table table-striped table-bordered">
			<thead>
				<tr><th colspan="3">Depot Settings</th></tr>
				<tr>
					<th></th>
					<th style="width: 40%;">Wii U</th>
					<th style="width: 40%;">Nintendo 3DS</th>
			</thead>
			<tbody>
				{if $fields.screenshot}
					<tr>
						<td class="center">Screenshot</td>
						<td colspan=2>{$fields.screenshot}</td>
					</tr>
				{/if}
				{if $fields.3dslevel}
					<tr>
						<td class="center">Level Download</td>
						<td>
							{$fields.wiiulevel}<br>
							<small>(If the level is hosted on Nintendo's Server, you may input just the Level ID)</small>
						</td>
						<td>{$fields.3dslevel}</td>
					</tr>
				{else}
					{* Reason this doesn't appear on levels is because there is no reason too. Levels should be already completed *}
					<tr>
						<td class="center">Percentage</td>
						<td colspan=2>{$fields.percent}</td>
					</tr>
				{/if}
				{if $fields.wiiutheme}
					<tr>
						<td class="center">Theme Download</td>
						<td>{$fields.wiiutheme}</td>
						<td>{$fields.3dstheme}</td>
					</tr>
					<tr>
						<td class="center">Costume Download</td>
						<td>{$fields.wiiucostume}</td>
						<td></td>
					</tr>
				{/if}
				{if $fields.style}
					<tr>
						<td class="center">Game Style</td>
						<td colspan=2>{$fields.style}</td>
					</tr>
				{/if}
				{if $fields.theme}
					<tr>
						<td class="center">Game Theme</td>
						<td colspan=2>{$fields.theme}</td>
					</tr>
				{/if}
			</tbody>
		</table>
		{$fields.btnEditThread}
	{/if}