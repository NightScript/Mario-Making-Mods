	{if $pagelinks}{$pagelinks}{/if}

	{foreach $posts as $post}
				<table class="table table-bordered table-striped" style="padding: 20px;">
					<thead>
						<tr><th><span style='font-size:125%;'>{$post.title}</span></th></tr>
						{if $post.description}
							<tr><th>{$post.description}</th></tr>
						{/if}
					</thead>
					<tbody>
						<tr><td style="padding:10px; max-height:200px !important;">
								{if $post.screenshots}{$post.screenshot}<br><br>{/if}
								{if $post.download}{$post.download}<br><br>{/if}
								<small>	Posted on {$post.formattedDate} by {$post.userlink}</small>
						</td></tr>
						<tr><td>{$post.comments}. {$post.replylink}</td></tr>
					</tbody>
				</table>
	{/foreach}

	
	{if $pagelinks}{$pagelinks}{/if}