
{if $pagelinks}<div class="smallFonts pages">{$pagelinks}</div>{/if}

<div id="accordion">
	{foreach $threads as $thread}
		<div class="card">
			<div class="card-header">
				{if $dostickies && $thread.sticky}<i class="fa fa-map-pin"></i>{/if}
				{if $thread.new}{$thread.new}{/if}
				{if $thread.icon}{$thread.icon}{/if}
				{if $thread.gotonew}{$thread.gotonew}{/if}
				{if $thread.poll}{$thread.poll}{/if}
				<a href="{$thread.link.link}">{$thread.link.name}</a>

				<span style="text-align: right; float: right;">
					<a class="collapsed card-link" data-toggle="collapse" href="#collapse{$thread.id}">
						<button class="btn-floating btn-sm btn-secondary" style="border-radius: 50%;"><i class="fa fa-angle-double-down"></i></button>
					</a>
				</span>
			</div>
			<div id="collapse{$thread.id}" class="collapse" data-parent="#accordion">
				<div class="card-body">
					{if $thread.link.tags}{$thread.link.tags}<br>{/if}
					{if $thread.description}{$thread.description}<br><br>{/if}

					Posted on {$thread.postdate} by {$thread.startuser} {if $showforum}in {$thread.forumlink}{/if}<br>
					<div class="smallFonts">
						<i class="fa fa-comments-o" aria-hidden="true"></i> {plural num=$thread.replies what='reply'}
						<i class="fa fa-eye"></i> {plural num=$thread.views what='view'}
					</div>
				</div>
			</div>
		</div>
	{/foreach}
</div>

{if $pagelinks}<div class="smallFonts pages">{$pagelinks}</div>{/if}
