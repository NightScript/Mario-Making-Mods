
		<tr class="cell2">
			<td rowspan={if $post.download || $post.description}3{else}2{/if} width="1">
					{if $post.screenshots}{$post.screenshot}{/if}
			</td>
			<td>{$post.title} by {$post.userlink}</td>
		</tr>
		{if $post.download || $post.description}
		<tr class="cell1">
			<td style="padding:10px; max-height:200px !important;">
				{if $post.description}{$post.description}{/if}
				{if $post.download}{if $post.description}<br><br>{/if}{$post.download}{/if}
			</td>
		</tr>{/if}
		<tr class="cell1" style="text-align: right;">
			<td>
				{$post.comments} - Submitted on {$post.formattedDate}
			</td>
		</tr>
		<tr class="cell1" style="height: 1px;">
			<td colspan=2>
				<hr>
			</td>
		</tr>