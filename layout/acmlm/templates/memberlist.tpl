	{if $pagelinks}{$pagelinks}{/if}
	
	<table class="table table-striped table-bordered">
		<thead>
			<tr>
				<th style="width:30px;">#</th>
				<th colspan=2>Name</th>
				<th style="width: 180px;">Registered on</th>
				<th style="width: 80px;">Posts</th>
				<th style="width: 100px;">Birthday</th>
				<th style="width: 150px;">EZ Links</th>
			</tr>
			<tr><th colspan=9>Search results &mdash; {plural num=$numUsers what='user'} found</th></tr>
		</thead>

		<tbody>
			{foreach $users as $user}
				<tr>
					<td class="cell2 center">{$user.num}</td>
					<td style="width:60px; height:60px; vertical-align:middle!important;">{if $user.avatar}<img src="{$user.avatar}" alt="" style="max-width: 60px;max-height:60px;">{/if}</td>
					<td>{$user.link} (#{$user.id})</td>
					<td class="center">{$user.regdate}</td>
					<td class="center">{$user.posts} ({$user.average})</td>
					<td class="center">{$user.birthday}</td>
					<td class="center">
						{foreach $user.quicklinks as $quicklink}
							<a href="{$quicklink.link}">{$quicklink.text}</a><br>
						{/foreach}
					</td>
				</tr>
			{/foreach}
		</tbody>
	</table>
	
	{if $pagelinks}{$pagelinks}{/if}
