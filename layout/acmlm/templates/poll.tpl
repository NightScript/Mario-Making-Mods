	<table class="table table-striped table-bordered">
		<thead><tr><th colspan=2>{$poll.question}</th></tr></thead>
		<tbody>
			{foreach $poll.options as $option}
				<tr>
					<td class="cell2" style="width: 20%;">
						<strong>{$option.label}</strong>
					</td>
					<td style="width: 80%">
						<div class="pollbarContainer">
							<img src="gfx/bargraph.php?z={$option.votes}&n={$poll.voters}&r={$option.r}&g={$option.g}&b={$option.b}">
						</div>
					</td>
				</tr>
			{/foreach}
			<tr class="cell{cycle values='0,1'}"><td colspan=2><small>
				{if $poll.multivote}
					Multiple voting is allowed.
				{else}
					Multiple voting is not allowed.
				{/if}
				{if $poll.voters == 1}
					1 user has voted so far.
				{else}
					{$poll.voters} users have voted so far.
				{/if}
				{if $poll.multivote}
					Total votes: {$poll.votes}.
				{/if}
			</small></td></tr>
		</tbody>
	</table>
