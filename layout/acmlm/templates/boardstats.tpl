	<table class="table table-bordered">
		<tr><td style="text-align: right;">
			{if $stats.numUsers}
				{plural num=$stats.numUsers what='registered user'}, {$stats.numActive} active ({$stats.pctActive}%)<br>
				Newest user: {$stats.lastUserLink}
			{else}
				No registered users
			{/if}
		</td></tr>
		{if $stats.birthdays}<tr><td class="center">{$stats.birthdays}</td></tr>{/if}
		<tr><td class="center">
			{plural num=$stats.numThreads what='thread'} and {plural num=$stats.numPosts what='post'} total<br>
			{plural num=$stats.newPostToday what='post'} today, {$stats.newPostLastHour} last hour, and {$stats.newPostLastWeek} for the past week<br>
			{plural num=$stats.newThreadToday what='active thread'} today, {$stats.newPostLastHour} last hour, and {$stats.newThreadLastWeek} for the past week
		</td></tr>
		<tr><td class="center">{$stats.online}</td></tr>
	</table>
