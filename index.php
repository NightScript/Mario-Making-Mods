<?php
define('BLARG', 1);

$biglogo = false;
$sidebarshow = false;

$ajaxPage = false;
if (isset($_GET['ajax'])) {
    $ajaxPage = true;
}

require __DIR__.'/lib/common.php';

$layout_crumbs = '';
$layout_actionlinks = '';

$layout_birthdays = getBirthdaysText();

$tpl->assign('logusername', htmlspecialchars($loguser['displayname'] ?: $loguser['name']));
$tpl->assign('loguserlink', UserLink($loguser));
$tpl->assign('logusertext', UserLink($loguser, false, false, true));

$metaStuff = [
    'description' => Settings::get('metaDescription'),
    'tags'        => Settings::get('metaTags'),
];

//=======================
// Do the page

$match = $router->match();

define('CURRENT_PAGE', $page);

ob_start();
$layout_crumbs = '';

$fakeerror = false;
if ($loguser['flags'] & 0x2) {
    if (rand(0, 100) <= 75) {
        Alert('Could not load requested page: failed to connect to the database. Try again later.', 'Error');
        $fakeerror = true;
    }
}

if (file_exists(__DIR__.'/themes/'.$theme.'/info.php')) {
    include __DIR__.'/themes/'.$theme.'/info.php';
}

if (!$fakeerror) {
    try {
        try {
            // Throw the 404 page if we don't have a match already.
            if ($match === false) {
                throw new Exception(404);
            } else {
                // Set up the stuff for our page loader.
                $pageName = $match['target'];
                $pageParams = $match['params'];

                // MabiPro: Set this to Smarty for template purposes
                $tpl->assign('currentPage', $pageName);

                // Check first for plugin pages.
                if (array_key_exists($pageName, $pluginpages)) {
                    // TODO: Make this cleaner than a hack.
                    $plugin = $pluginpages[$pageName];
                    $self = $plugins[$plugin];

                    $page = __DIR__.'/plugins/'.$self['dir'].'/pages/'.$pageName.'.php';
                    if (file_exists($page)) {
                        include_once $page;
                    } else {
                        throw new Exception(404);
                    }
                } else {
                    // Check now for core pages.
                    $page = __DIR__.'/pages/'.$pageName.'.php';

                    if (file_exists($page)) {
                        include_once $page;
                    } else {
                        throw new Exception(404);
                    }
                }
            }
        } catch (Exception $e) {
            // is this used at all?
            if ($e->getMessage() != 404) {
                throw $e;
            }
            include_once __DIR__.'/pages/404.php';
        }
    } catch (KillException $e) {
    }
}

if ($ajaxPage) {
    ob_end_flush();
    die();
}

$layout_contents = ob_get_contents();
ob_end_clean();

//Do these things only if it's not an ajax page.
require __DIR__.'/lib/views.php';
setLastActivity();

//=======================
// Panels and footer

require __DIR__.'/layout/userpanel.php';
require __DIR__.'/layout/menus.php';

//=======================
// Notification bars

$notifications = getNotifications();

//=======================
// Misc stuff

$layout_time = formatdatenow();
$layout_onlineusers = getOnlineUsersText();
$layout_birthdays = getBirthdaysText();
$layout_views = '<span id="viewCount">'.number_format($misc['views']).'</span> '.__('views');

$layout_title = htmlspecialchars(Settings::get('boardname'));
if (!empty($title)) {
    $layout_title = $title.' &raquo; '.$layout_title;
}

//=======================
// Board logo and theme

checkForImage($layout_logopic, false, "themes/$theme/logo.png");
checkForImage($layout_logopic, false, "themes/$theme/logo.jpg");
checkForImage($layout_logopic, false, "themes/$theme/logo.gif");
checkForImage($layout_logopic, false, 'img/logo.png');

if (!file_exists('themes/'.$theme) && $loguserid) { //Are we using some invalid theme?
    $defaultTheme = Settings::get('defaultTheme');
    $db->updateId('users', ['theme' => $defaultTheme], 'id', $loguserid);
    $theme = $defaultTheme;
}

$favicon = resourceLink('img/minicon.png');

$themefile = 'themes/'.$theme.'/style_'.$loguser['layout'].'.php';
if (!file_exists(__DIR__.'/'.$themefile)) {
    $themefile = 'themes/'.$theme.'/style.php';
}
if (!file_exists(__DIR__.'/'.$themefile)) {
    $themefile = 'themes/'.$theme.'/style_'.$loguser['layout'].'.css';
}
if (!file_exists(__DIR__.'/'.$themefile)) {
    $themefile = 'themes/'.$theme.'/style.css';
}
if (!file_exists(__DIR__.'/'.$themefile)) {
    $themefile = '';
}

if ($sidebarshow == true) {
    $sidebar = '
                    <table class="table table-bordered table-striped table-sm">
                        <thead><tr><th>Depot Selections</th></tr></thead>
                        <tbody>
                            <tr><td>'.pageLinkTag(__('Theme Projects'), 'haxsdepot').'</td></tr>
                            <tr><td>'.pageLinkTag(__('Level Depot'), 'leveldepot').'</td></tr>
                        </tbody>
                    </table>
                    <br>
                    <table class="table table-bordered table-striped table-sm">
                        <tr class="header0"><th>Filter</th></tr>
                            <form action="/'.$depoturl.'">';
    if ($showconsoles == true) {
        $sidebar .= '
                                <tr class="header1"><th>Console</th></tr>
                                <tr class="cell0"><td><input type="radio" name="console" '.(isset($_GET['console']) ? '' : 'checked').' value=""> Both</td></tr>
                                <tr class="cell0"><td><input type="radio" name="console" '.($_GET['console'] == 'wiiu' ? 'checked' : '').' value="wiiu"><img src="https://cdn.discordapp.com/attachments/318888570691518465/394700847705227276/wii-u-games-tool.png">WiiU</a></td></tr>
                                <tr class="cell1"><td><input type="radio" name="console" '.($_GET['console'] == '3ds' ? 'checked' : '').' value="3ds"><img src="https://cdn.discordapp.com/attachments/260105243503624193/394725088735264773/600px-3DS_Icon.svg.png">3DS</a></td></tr>
                                <tr class="header1"><th>Game Style</th></tr>
                                <tr class="cell0"><td><input type="radio" name="style" '.(isset($_GET['style']) ? '' : 'checked').' value=""> All</td></tr>
                                <tr class="cell0"><td><input type="radio" name="style" '.($_GET['style'] == 'smb1' ? 'checked' : '').' value="smb1"><img src="https://cdn.discordapp.com/emojis/364096385945174016.png" height="11" width="22"> SMB1</td></tr>
                                <tr class="cell1"><td><input type="radio" name="style" '.($_GET['style'] == 'smb3' ? 'checked' : '').' value="smb3"><img src="https://cdn.discordapp.com/emojis/364096453091524610.png" height="11" width="22"> SMB3</td></tr>
                                <tr class="cell0"><td><input type="radio" name="style" '.($_GET['style'] == 'smw' ? 'checked' : '').' value="smw"><img src="https://cdn.discordapp.com/emojis/364096482539864075.png" height="11" width="22"> SMW</td></tr>
                                <tr class="cell1"><td><input type="radio" name="style" '.($_GET['style'] == 'nsmbu' ? 'checked' : '').' value="nsmbu"><img src="https://cdn.discordapp.com/emojis/364096512654966784.png" height="11" width="22"> NSMBU</td></tr>
                                <tr class="cell0"><td><input type="radio" name="style" '.($_GET['style'] == 'custom' ? 'checked' : '').' value="custom">Custom</td></tr>
                                <tr class="header1"><th>Themes Replaces</th></tr>
                                <tr class="cell0"><td><input type="radio" name="theme" '.(isset($_GET['theme']) ? '' : 'checked').' value=""> All</td></tr>
                                <tr class="cell0"><td><input type="radio" name="theme" '.($_GET['theme'] == 'grass' ? 'checked' : '').' value="grass"><img src="https://cdn.discordapp.com/attachments/346883750854131715/396187499724144640/Screenshot_2017-08-06_at_12.56.45_PM.png"> Grassland</td></tr>
                                <tr class="cell1"><td><input type="radio" name="theme" '.($_GET['theme'] == 'until' ? 'checked' : '').' value="under"><img src="https://cdn.discordapp.com/attachments/346883750854131715/396188673634467841/Screenshot_2017-08-06_at_12.56.45_PM.png"> Underground</td></tr>
                                <tr class="cell0"><td><input type="radio" name="theme" '.($_GET['theme'] == 'water' ? 'checked' : '').' value="water"><img src="https://cdn.discordapp.com/attachments/346883750854131715/396188394004283392/Screenshot_2017-08-06_at_12.56.45_PM.png"> Underwater</td></tr>
                                <tr class="cell1"><td><input type="radio" name="theme" '.($_GET['theme'] == 'castle' ? 'checked' : '').' value="castle"><img src="https://cdn.discordapp.com/attachments/346883750854131715/396189460754071553/Screenshot_2017-08-06_at_12.56.45_PM.png"> Castle</td></tr>
                                <tr class="cell0"><td><input type="radio" name="theme" '.($_GET['theme'] == 'ghost' ? 'checked' : '').' value="ghost"><img src="https://cdn.discordapp.com/attachments/346883750854131715/396189134894399508/Screenshot_2017-08-06_at_12.56.45_PM.png"> Ghost House</td></tr>
                                <tr class="cell0"><td><input type="radio" name="theme" '.($_GET['theme'] == 'airship' ? 'checked' : '').' value="airship"><img src="https://cdn.discordapp.com/attachments/346883750854131715/396188140353617920/Screenshot_2017-08-06_at_12.56.45_PM.png"> Airship</td></tr>
                                <tr class="cell0"><td><input type="radio" name="theme" '.($_GET['theme'] == 'custom' ? 'checked' : '').' value="custom">Custom</td></tr>';
    }
    $sidebar .= '
                                <tr class="header1"><th>Sort by</th></tr>
                                <tr class="cell0"><td><input type="radio" name="sort" '.(isset($_GET['sort']) ? '' : 'checked').' value="lastdate"> Last Post Date</td></tr>
                                <tr class="cell1"><td><input type="radio" name="sort" '.($_GET['sort'] == 'date' ? 'checked' : '').' value="date"> Date Published</td></tr>
                                <tr class="cell0"><td><input type="radio" name="sort" '.($_GET['sort'] == 'name' ? 'checked' : '').' value="name"> Name (alphabetical)</td></tr>
                                <tr class="header1"><th>Name of Hack</th></tr>
                                <tr class="cell0"><td><input class="form-control" type="text" name="hackname" value="'.$prevfield['hackname'].'"></td></tr>
                                <tr class="header1"><th><input type="submit" value="Submit"></th></tr>
                            </form>
                    </table>
                </td>
            </tr>
        </table>
    </td>';
}

$opengraph = [
	'type' => 'website',
	'url' => getServerURLNoSlash($ishttps)
];

if (file_exists(__DIR__.'/apple-touch-icon.png')) {
    $opengraph['image'] = resourceLink('apple-touch-icon.png');
}

if (!empty($layout_title)) {
    $opengraph['title'] = htmlspecialchars($layout_title);
}

if (!empty($metaStuff['description'])) {
    $opengraph['description'] = htmlspecialchars($metaStuff['description']);
}

if (!empty(Settings::get('boardname'))) {
    $opengraph['site_name'] = htmlspecialchars(Settings::get('boardname'));
}

?>
<!DOCTYPE HTML>
<html>
    <head>
        <!-- Mobile meta tags -->
        <meta charset="utf-8">
        <meta name="theme-color" content="#2196F3" />
        <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta http-equiv="Content-Type" content="text/html; CHARSET=utf-8" />
        <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />
        <meta name="viewport" content="width=device-width, minimum-scale=1, initial-scale=1, shrink-to-fit=no, user-scalable=yes">

        <?php echo !empty($layout_title) ? '<title>'.$layout_title.'</title>' : ''; ?>

        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo resourceLink('fonts/fork-awesome/css/fork-awesome.min.css') ?>" />
        <link rel="stylesheet" href="<?php echo resourceLink('css/bootstrap.css'); ?>">
        <link rel="stylesheet" href="<?php echo resourceLink('css/mdb.css'); ?>">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

        <link rel="shortcut icon" type="image/x-icon" href="<?php echo $favicon; ?>" />
        <link rel="apple-touch-icon" href="<?php echo resourceLink('apple-touch-icon.png'); ?>" />
        <link rel="manifest" href="<?php echo resourceLink('API/manifest.php'); ?>" />
        <meta name="description" content="<?php echo $metaStuff['description']; ?>" />
        <meta name="keywords" content="<?php echo $metaStuff['tags']; ?>" />
        <base href="<?php echo getServerURLNoSlash($ishttps)?>" />
        <!-- Open Graph general (Facebook, Pinterest & Google+) -->
		<?php
			foreach ($opengraph as $name=>$value) {
				echo '<meta property="og:'.$name.'" content="'.$value.'" /> <meta name="og:'.$name.'" content="'.$value.'" />';
			}
		?>
        <!-- Twitter information -->
        <meta name="twitter:card" content="summary" />
        <meta name="twitter:title" content="<?php echo $layout_title; ?>" />
        <meta name="twitter:image:src" content="<?php echo $layout_logopic; ?>" />
        <meta name="twitter:description" content="<?php echo $metaStuff['description']; ?>" />
        <meta name="twitter:site" content="@MarioMakingMods" />
        <meta name="twitter:creator" content="@MaorNinja322" />
        <meta name="twitter:widgets:theme" content="dark">
        <meta name="twitter:widgets:link-color" content="#2196F3">
        <meta name="twitter:widgets:border-color" content="#2196F3">

        <link rel="stylesheet" href="<?php echo resourceLink('css/common.css'); ?>" />
        <link rel="stylesheet" href="<?php echo resourceLink('css/highlight.css'); ?>" />
        <link rel="stylesheet" id="theme_css" href="<?php echo !empty($themefile) ? resourceLink($themefile) : ''; ?>" />

        <script>
            (function(document,navigator,standalone) {
                // prevents links from apps from oppening in mobile safari
                // this javascript must be the first script in your <head>
                if ((standalone in navigator) && navigator[standalone]) {
                    var curnode, location=document.location, stop=/^(a|html)$/i;
                    document.addEventListener('click', function(e) {
                        curnode=e.target;
                        while (!(stop).test(curnode.nodeName)) {
                            curnode=curnode.parentNode;
                        }
                        // Conditions to do this only on links to your own app
                        // if you want all links, use if('href' in curnode) instead.
                        if('href' in curnode && ( curnode.href.indexOf('http') || ~curnode.href.indexOf(location.host) ) ) {
                            e.preventDefault();
                            location.href = curnode.href;
                        }
                    },false);
                }
            })(document,window.navigator,'standalone');
        </script>

        <script src="<?php echo resourceLink('js/jquery-ui.js'); ?>"></script>
        <script src="<?php echo resourceLink('js/tricks.js'); ?>"></script>
        <script src="<?php echo resourceLink('js/jscolor.js'); ?>" async></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.2.0/highlight.min.js"></script>

        <script>
            hljs.initHighlightingOnLoad();
            boardroot = <?php echo json_encode(URL_ROOT); ?>;

            if ('serviceWorker' in navigator) navigator.serviceWorker.register('service-worker.js').catch();
        </script>
        <?php
            $bucket = 'pageHeader'; require __DIR__.'/lib/pluginloader.php';

        if (file_exists(__DIR__.'/themes/'.$theme.'/style.js')) {
            echo '<script src="/themes/'.$theme.'/style.js"></script>';
        }

        if (!$acmlmboardLayout) {
            echo '<link rel="stylesheet" href="'.resourceLink('css/modern.css').'" />';
        }
        ?>
    </head>
    <body>
        <form action="<?php echo htmlentities(pageLink('login')); ?>" method="post" id="logout" style="display:none;">
            <input type="hidden" name="action" value="logout">
        </form>
        <?php
            if (Settings::get('maintenance')) {
                echo '<div style="font-size:30px; font-weight:bold; color:red; background:black; padding:5px; border:2px solid red; position:absolute; top:30px; left:30px;">MAINTENANCE MODE</div>';
            }

            if (file_exists(__DIR__.'/themes/'.$theme.'/html.php')) {
                require __DIR__."/themes/$theme/html.php";
            }

            $rendertime = sprintf('%.03f', microtime(true) - $_SERVER['REQUEST_TIME_FLOAT']);
            $layout_credits = htmlspecialchars(Settings::get('boardname')).' &middot; by NightYoshi370 [url=/credits]& others[/url]
            Page rendered in '.$rendertime.' seconds (with '.sprintf('%.03f', memory_get_usage() / 1048576).' MB of RAM).';

            RenderTemplate('pagelayout', [
                'layout_contents'    => $layout_contents,
                'layout_crumbs'      => $layout_crumbs,
                'layout_actionlinks' => $layout_actionlinks,
                'layout_dropdown'    => $layout_dropdown,
                'headerlinks'        => $headerlinks,
                'sidelinks'          => $sidelinks,
                'layout_userpanel'   => $layout_userpanel,
                'notifications'      => $notifications,
                'boardname'          => Settings::get('boardname'),
                'boarddescription'   => Settings::get('metaDescription'),
                'poratitle'          => Settings::get('PoRATitle'),
                'poratext'           => Settings::get('PoRAText'),
                'layout_logopic'     => $layout_logopic,
                'homepage'           => $biglogo,
                'layout_time'        => $layout_time,
                'layout_views'       => $layout_views,
                'layout_onlineusers' => $layout_onlineusers,
                'layout_birthdays'   => $layout_birthdays,
                'layout_credits'     => parseBBCode($layout_credits),
                'sidebar'            => $sidebar,
                'chat'               => $chat
            ]);
            ?>

		<script src="<?php echo resourceLink('js/timeago.js'); ?>"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
        <script src="<?php echo resourceLink('js/bootstrap.js'); ?>"></script>
        <script src="<?php echo resourceLink('js/mdb.min.js'); ?>"></script>
    </body>
</html>
<?php
$bucket = 'finish'; require __DIR__.'/lib/pluginloader.php';
