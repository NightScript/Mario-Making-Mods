<?php

if (!defined('BLARG')) {
    die();
}

function OnlineUsers($forum = 0, $update = true)
{
    global $loguserid, $db;
    $forumClause = '';
    $browseLocation = __('online');

    if ($update) {
        if ($loguserid) {
			$db->updateId('users', ['lastforum' => $forum], 'id', $loguserid);
        } else {
			$db->updateId('guests', ['lastforum' => $forum], 'ip', $_SERVER['REMOTE_ADDR']);
        }
    }

    if ($forum) {
        $forumClause = ' and lastforum={1}';
        $forumName = FetchResult('SELECT title FROM {forums} WHERE id={0}', $forum);
        $browseLocation = format(__('browsing {0}'), $forumName);
    }

    $rOnlineUsers = Query('select u.(_userfields) from {users} u where (lastactivity > {0} or lastposttime > {0}) and loggedin = 1 '.$forumClause.' order by name', time() - 300, $forum);
    $onlineUserCt = 0;
    $onlineUsers = '';
    while ($user = Fetch($rOnlineUsers)) {
        $user = getDataPrefix($user, 'u_');
        $userLink = UserLink($user, true);
        $onlineUsers .= ($onlineUserCt ? ', ' : '').$userLink;
        $onlineUserCt++;
    }
    $onlineUsers = Plural($onlineUserCt, __('user')).' '.$browseLocation.($onlineUserCt ? ': ' : '.').$onlineUsers;

    $data = Fetch(
        Query(
            "select 
		(select count(*) from {guests} where bot=0 and date > {0} $forumClause) as guests,
		(select count(*) from {guests} where bot=1 and date > {0} $forumClause) as bots
		", (time() - 300), $forum
        )
    );
    $guests = $data['guests'];
    $bots = $data['bots'];

    if ($guests) {
        $onlineUsers .= ' | '.Plural($guests, __('guest'));
    }
    if ($bots) {
        $onlineUsers .= ' | '.Plural($bots, __('bot'));
    }

    return $onlineUsers;
}

function getOnlineUsersText()
{
    global $OnlineUsersFid;

    if (!isset($OnlineUsersFid)) {
        $OnlineUsersFid = 0;
    }

    $onlineUsers = OnlineUsers($OnlineUsersFid);

    return '<div id="onlineUsers">'.$onlineUsers.'</div>
		<script>
			onlineFID = '.$OnlineUsersFid.';
			window.addEventListener("load",  startOnlineUsers, false);
		</script>';
}
