<?php

//  AcmlmBoard XD support - Login support
if (!defined('BLARG')) {
    die();
}

require __DIR__.'/security/bots.php';
require __DIR__.'/browsers.php';

$mobileLayout = false;
$isBot = 0;
$isAndroid = false;

$detect = new Mobile_Detect();
if ($detect->isMobile()) {
    $mobileLayout = true;
}

if ($detect->isAndroidOS()) {
    $isAndroid = true;
}

if (str_replace($bots, 'x', $_SERVER['HTTP_USER_AGENT']) != $_SERVER['HTTP_USER_AGENT']) { // stristr()/stripos()?
    $isBot = 1;
}

//Check the amount of users right now for the records
$misc = $db->row('misc');

$rOnlineUsers = Query('SELECT id FROM {users} WHERE lastactivity > {0} or lastposttime > {0} ORDER BY name', (time() - 300));

$_qRecords = [];
$onlineUsers = '';
$onlineUserCt = 0;
while ($onlineUser = Fetch($rOnlineUsers)) {
    $onlineUsers .= ':'.$onlineUser['id'];
    $onlineUserCt++;
}

if ($onlineUserCt > $misc['maxusers']) {
    $_qRecords['maxusers'] = $onlineUserCt;
    $_qRecords['maxusersdate'] = $db->date();
    $_qRecords['maxuserstext'] = $onlineUsers;
}

//Check the amount of posts for the record
$newToday = FetchResult('SELECT COUNT(*) FROM {posts} WHERE date > {0}', (time() - 86400));
if ($newToday > $misc['maxpostsday']) {
    $_qRecords['maxpostsday'] = $newToday;
    $_qRecords['maxpostsdaydate'] = $db->date();
}

$newLastHour = FetchResult('SELECT COUNT(*) FROM {posts} WHERE date > {0}', (time() - 3600));
if ($newLastHour > $misc['maxpostshour']) {
    $_qRecords['maxpostshour'] = $newLastHour;
    $_qRecords['maxpostshourdate'] = $db->date();
}

if (count($_qRecords)) {
    $db->update('misc', $_qRecords);
}

//Delete oldies visitor FROM the guest list. We may re-add him/her later.
Query('DELETE FROM {guests} WHERE date < {0}', (time() - 300));

//Lift dated Tempbans
Query("UPDATE {users} set primarygroup = tempbanpl, tempbantime = 0, title='' WHERE tempbantime != 0 and tempbantime < {0}", time());

//Lift dated IP Bans
Query('delete FROM {ipbans} WHERE date != 0 and date < {0}', time());

//Delete expired sessions
Query('delete FROM {sessions} WHERE expiration != 0 and expiration < {0}', time());

function isIPBanned($ip)
{
    $rIPBan = Query('SELECT * FROM {ipbans} WHERE instr({0}, ip)=1', $ip);

    while ($ipban = Fetch($rIPBan)) {
        // check if this IP ban is actually good
        // if the last character is a number, IPs have to match precisely
        if (ctype_alnum(substr($ipban['ip'], -1)) && ($ip !== $ipban['ip'])) {
            continue;
        }

        return $ipban;
    }

    return false;
}

$ipban = isIPBanned($_SERVER['REMOTE_ADDR']);

if ($ipban) {
    $adminemail = Settings::get('ownerEmail');

    $ipbantext = 'You have been IP-banned '.($ipban['date'] ? ' until '.gmdate('M jS Y, G:i:s', $ipban['date'])." (GMT). That's ".TimeUnits($ipban['date'] - time()).' left' : '').'. Attempting to get around this in any way will result in worse things.';
    $ipbantext .= '<br>Reason: '.$ipban['reason'];
    if ($adminemail) {
        $ipbantext .= '<br><br>If you were erroneously banned, contact the board owner at: '.$adminemail;
    }
    die($ipbantext);
}

function doHash($data)
{
    return hash('sha256', $data, false);
}

$loguser = null;

if (isset($_COOKIE['logsession']) && !$ipban) {
    $session = $db->row('sessions', ['id' => doHash($_COOKIE['logsession'].SALT)]);
    if ($session) {
        $loguser = $db->row('users', ['id' => $session['user']]);
        if ($session['autoexpire']) {
            $db->updateId('sessions', ['expiration' => $db->time() + 10 * 60], 'id', $session['id']); //10 minutes
        }
    }
}

if ($loguser) {
    $loguser['token'] = hash('sha256', "{$loguser['id']},{$loguser['pss']},".SALT.',dr567hgdf546guol89ty,'.$session.',896rd7y56gvers9t');
    $loguserid = $loguser['id'];
    $logusername = htmlspecialchars($loguser['name']);

    $sessid = doHash($_COOKIE['logsession'].SALT);
    $db->updateId('sessions', ['lasttime' => $db->time()], 'id', $sessid);
    Query('DELETE FROM {sessions} WHERE user={0} AND lasttime<={1}', $loguserid, time() - 2592000);
} else {
    $loguser = [
        'id'             => 0,
        'name'           => '',
        'displayname'    => '',
        'sex'            => 0,
        'primarygroup'   => Settings::get('defaultGroup'),
        'threadsperpage' => 50,
        'postsperpage'   => 20,
        'theme'          => Settings::get('defaultTheme'),
        'dateformat'     => 'm-d-y',
        'timeformat'     => 'h:i A',
        'fontsize'       => 80,
        'timezone'       => 0,
        'blocklayouts'   => !Settings::get('guestLayouts'),
        'flags'          => 0,
        'layout'         => 'modern',
        'token'          => hash('sha1', rand()),
    ];
    $loguserid = 0;
    $logusername = '';
}

$acmlmboardLayout = false;
$defaultLayout = false;

if (($loguser['layout'] == 'acmlm' || (!isset($loguser['layout']) && Settings::get('defaultLayout') == 'acmlm')) && !$mobileLayout) {
    $acmlmboardLayout = true;
} elseif (($loguser['layout'] == 'modern' || (!isset($loguser['layout']) && Settings::get('defaultLayout') == 'modern')) && !$mobileLayout) {
    $defaultLayout = true;
} elseif ($loguser['layout'] == 'mobile' || (!isset($loguser['layout']) && Settings::get('defaultLayout') == 'mobile')) {
    $mobileLayout = true;
}

if ($loguser['flags'] & 0x1) {
    $db->insert('ipbans', ['ip' => $_SERVER['REMOTE_ADDR'], 'reason' => '['.$logusername.'] Account IP-banned', 'date' => 0]);
    die(header('Location: '.$_SERVER['REQUEST_URI']));
}

if ($mobileLayout) {
    $loguser['blocklayouts'] = 1;
    $loguser['fontsize'] = 100;
}

function setLastActivity()
{
    global $loguserid, $isBot, $lastKnownBrowser, $ipban, $db;

    Query('DELETE FROM {guests} WHERE ip = {0}', $_SERVER['REMOTE_ADDR']);

    if ($ipban) {
        return;
    }

    if ($loguserid == 0) {
        $ua = '';
        if (isset($_SERVER['HTTP_USER_AGENT'])) {
            $ua = $_SERVER['HTTP_USER_AGENT'];
        }

        $lasturl = getRequestedURL();
        $db->insert('guests', ['date' => $db->time(), 'ip' => $_SERVER['REMOTE_ADDR'], 'lasturl' => $lasturl, 'useragent' => $ua, 'bot' => $isBot]);
    } else {
        $db->updateId(
            'users', [
				'lastactivity'     => $db->time(),
				'lastip'           => $_SERVER['REMOTE_ADDR'],
				'lasturl'          => getRequestedURL(),
				'lastknownbrowser' => $lastKnownBrowser,
				'loggedin'         => 1,
            ], 'id', $loguserid
        );
    }
}
