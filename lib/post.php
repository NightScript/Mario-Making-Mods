<?php

if (!defined('BLARG')) {
    die();
}

function ParseThreadTags($title)
{
    preg_match_all("/\[(.*?)\]/", $title, $matches);
    foreach ($matches[1] as $tag) {
        $title = str_replace('['.$tag.']', '', $title);
        $tag = htmlspecialchars(strtolower($tag));

        //Start at a hue that makes "18" red.
        $hash = -105;
        for ($i = 0; $i < strlen($tag); $i++) {
            $hash += ord($tag[$i]);
        }

        //That multiplier is only there to make "nsfw" and "18" the same color.
        $color = 'hsl('.(($hash * 57) % 360).', 70%, 40%)';

        $tags .= '<span class="threadTag" style="background-color: '.$color.';">'.$tag.'</span>';
    }
    if ($tags) {
        $tags = ' '.$tags;
    }

    $title = str_replace('<', '&lt;', $title);
    $title = str_replace('>', '&gt;', $title);

    return [trim($title), $tags];
}

function filterPollColors($input)
{
    return preg_replace('@[^#0123456789abcdef]@si', '', $input);
}

function loadBlockLayouts()
{
    global $blocklayouts, $loguserid;

    if (isset($blocklayouts)) {
        return;
    }

    $rBlocks = Query('SELECT * FROM {blockedlayouts} WHERE blockee = {0}', $loguserid);
    $blocklayouts = [];

    while ($block = Fetch($rBlocks)) {
        $blocklayouts[$block['user']] = 1;
    }
}

function getSyndrome($activity)
{
    include __DIR__.'/syndromes.php';
    $soFar = '';
    foreach ($syndromes as $minAct => $syndrome) {
        if ($activity >= $minAct) {
            $soFar = '<em style="color: '.$syndrome[1].';">'.$syndrome[0].'</em><br>';
        }
    }

    return $soFar;
}

function applyTags($text, $tags)
{
    if (!stristr($text, '&')) {
        return $text;
    }

    $s = $text;
    foreach ($tags as $tag => $val) {
        $s = str_replace("&$tag&", $val, $s);
    }

    if (is_numeric($tags['numposts'])) {
        $s = preg_replace_callback('@&(\d+)&@si', [new MaxPosts($tags), 'max_posts_callback'], $s);
    } else {
        $s = preg_replace("'&(\d+)&'si", 'preview', $s);
    }

    return $s;
}

// hax for anonymous function
class MaxPosts
{
    public $tags;

    public function __construct($tags)
    {
        $this->tags = $tags;
    }

    public function max_posts_callback($results)
    {
        return max($results[1] - $this->tags['postcount'], 0);
    }
}

$activityCache = [];
function getActivity($id)
{
    global $activityCache;

    if (!isset($activityCache[$id])) {
        $activityCache[$id] = FetchResult('SELECT COUNT(*) FROM {posts} WHERE user = {0} and date > {1}', $id, (time() - 86400));
    }

    return $activityCache[$id];
}

function makePostText($post, $poster)
{
    $noSmilies = $post['options'] & 2;
    $exp = calcexp($poster['posts'], (time() - $poster['regdate']) / 86400);
    $lvl = calclvl($exp);

    //Do Ampersand Tags
    $tags = [
        'postnum'     => $post['num'],
        'postcount'   => $poster['posts'],
        'numdays'     => floor((time() - $poster['regdate']) / 86400),
        'date'        => formatdate($post['date']),
        'rank'        => GetRank($poster['rankset'], $poster['posts']),
        'level'       => $lvl,
        'exp'         => $exp,
        'lvlexp'      => calclvlexp($lvl + 1),
        'lvllen'      => lvlexp($lvl),
        'expgain'     => calcexpgainpost($poster['posts'], (time() - $poster['regdate']) / 86400),
        'expgaintime' => calcexpgaintime($poster['posts'], (time() - $poster['regdate']) / 86400),
        'exppct'      => sprintf('%d', $edone * 100 / lvlexp($lvl)),
        'exppct2'     => sprintf('%d', $eleft * 100 / lvlexp($lvl)),
        'lvlbar'      => drawrpglevelbar($exp, '166'),
    ];
    $bucket = 'amperTags';
    include __DIR__.'/pluginloader.php';

    if ($poster['signature']) {
        if (!$poster['signsep']) {
            $separator = '<br>_________________________<br>';
        } else {
            $separator = '<br>';
        }
    }

    $attachblock = '';
    if ($post['has_attachments']) {
        if (isset($post['preview_attachs'])) {
            $ispreview = true;
            $fileids = array_keys($post['preview_attachs']);
            $attachs = Query(
                'SELECT id,filename,physicalname,description,downloads 
				FROM {uploadedfiles}
				WHERE id IN ({0c})',
                $fileids
            );
        } else {
            $ispreview = false;
            $attachs = Query(
                'SELECT id,filename,physicalname,description,downloads 
				FROM {uploadedfiles}
				WHERE parenttype={0} AND parentid={1} AND deldate=0
				ORDER BY filename',
                'post_attachment',
                $post['id']
            );
        }

        while ($attach = Fetch($attachs)) {
            $url = URL_ROOT.'get.php?id='.htmlspecialchars($attach['id']);
            $linkurl = $ispreview ? '#' : $url;
            $filesize = filesize(DATA_DIR.'uploads/'.$attach['physicalname']);

            $attachblock .= '<br><div class="post_attachment">';

            $fext = strtolower(substr($attach['filename'], -4));
            if ($fext == '.png' || $fext == '.jpg' || $fext == 'jpeg' || $fext == '.gif') {
                $alt = htmlspecialchars($attach['filename']).' &mdash; '.BytesToSize($filesize).', viewed '.Plural($attach['downloads'], 'time');

                $attachblock .= '<a href="'.$linkurl.'"><img src="'.$url.'" alt="'.$alt.'" title="'.$alt.'" style="max-width:300px; max-height:300px;"></a>';
            } else {
                $link = '<a href="'.$linkurl.'">'.htmlspecialchars($attach['filename']).'</a>';

                $desc = htmlspecialchars($attach['description']);
                if ($desc) {
                    $desc .= '<br>';
                }

                $attachblock .= '<strong>'.__('Attachment: ').$link.'</strong><br>';
                $attachblock .= '<div class="smallFonts">'.$desc;
                $attachblock .= BytesToSize($filesize).__(' &mdash; Downloaded ').Plural($attach['downloads'], 'time').'</div>';
            }

            $attachblock .= '</div>';
        }
    }

    $postText = $poster['postheader'].$post['text'].$attachblock.$separator.$poster['signature'];
    $postText = ApplyTags($postText, $tags);
    $postText = CleanUpPost($postText, $noSmilies, false);

    return $postText;
}

define('POST_NORMAL', 0);           // standard post box
define('POST_PM', 1);               // PM post box
define('POST_DELETED_SNOOP', 2);    // post box with close/undelete (for mods 'view deleted post' feature)
define('POST_SAMPLE', 3);           // sample post box (profile sample post, newreply post preview, etc)
define('POST_DEPOT_REPLY', 4);      // standard depot-reply post box
define('POST_DEPOT_MAIN', 5);       // standard depot-main post box
define('POST_PROFILE', 6);          // Post box used for recent activity tab

// $post: post data (typically returned BY SQL queries or forms)
// $type: one of the POST_XXX constants
// $params: an array of extra parameters, depending on the post box type. Possible parameters:
//        * tid: the ID of the thread the post is in (POST_NORMAL and POST_DELETED_SNOOP only)
//        * fid: the ID of the forum the thread containing the post is in (POST_NORMAL and POST_DELETED_SNOOP only)
//        * threadlink: if set, a link to the thread is added next to 'Posted on blahblah' (POST_NORMAL and POST_DELETED_SNOOP only)
//        * noreplylinks: if set, no links to newreply.php (Quote/ID) are placed in the metabar (POST_NORMAL only)
function makePost($post, $type, $params = [])
{
    global $loguser, $loguserid, $usergroups, $isBot, $blocklayouts, $mobileLayout, $defaultLayout, $acmlmboardLayout;

    $poster = getDataPrefix($post, 'u_');
    $post['userlink'] = UserLink($poster, 'false');

    LoadBlockLayouts();
    $pltype = Settings::get('postLayoutType');
    $isBlocked = $poster['globalblock'] || $loguser['blocklayouts'] || $post['options'] & 1 || isset($blocklayouts[$poster['id']]);

    $post['type'] = $type;
    $post['formattedDate'] = formatdate($post['date']);

    if (HasPermission('admin.viewips')) {
        htmlspecialchars($post['ip']);
    } else {
        $post['ip'] = '';
    } // TODO IP formatting?

    $undeleteicon = '';
    $viewicon = '';
    $closeicon = '';
    $deleteicon = '';
    if ($mobileLayout) {
        $undeleteicon = '<i class="fa fa-undo"></i> ';
        $viewicon = '<i class="fa fa-chevron-down"></i> ';
        $closeicon = '<i class="fa fa-chevron-up"></i> ';
        $deleteicon = '<i class="fa fa-remove"></i> ';
    }

    if ($post['deleted'] && $type !== POST_DELETED_SNOOP && ($type == POST_NORMAL || $type == POST_DEPOT_REPLY || $type == POST_DEPOT_MAIN || $type == POST_PROFILE)) {
        $post['deluserlink'] = UserLink(getDataPrefix($post, 'du_'));
        $post['delreason'] = htmlspecialchars($post['reason']);

        $links = [];
        if (HasPermission('mod.deleteposts', $params['fid'])) {
            $links['undelete'] = actionLinkTag($undeleteicon.__('Undelete'), 'editpost', $post['id'], 'delete=2&key='.$loguser['token']);
            $links['view'] = '<a href="javascript:replacePost('.$post['id'].',true)">'.$viewicon.__('View').'</a>';
        }
        $post['links'] = $links;

        RenderTemplate('postbox_deleted', ['post' => $post]);

        return;
    }

    $links = [];

    if ($type != POST_SAMPLE) {
        $forum = $params['fid'];
        $thread = $params['tid'];

        $notclosed = (!$post['closed'] || HasPermission('mod.closethreads', $forum));

        $extraLinks = [];

        if (!$isBot) {
            if ($type == POST_DELETED_SNOOP) {
                if ($notclosed && HasPermission('mod.deleteposts', $forum)) {
                    $links['undelete'] = actionLinkTag($undeleteicon.__('Undelete'), 'editpost', $post['id'], 'delete=2&key='.$loguser['token']);
                }

                $links['close'] = '<a href="javascript:void(0);" onclick="replacePost('.$post['id'].',false); return false;">'.$closeicon.__('Close').'</a>';
            } elseif ($type == POST_NORMAL || $type == POST_DEPOT_REPLY || $type == POST_DEPOT_MAIN || $type == POST_PROFILE) {
                $links['post'] = ['link' => actionLink('post', $post['id']), 'text' => __('Link this post'), 'icon' => 'link'];

                if ($post['threadname']) {
                    $TwitterURL = getServerDomainNoSlash().actionLink('post', $post['id']).'/&via=MarioMakingMods';

                    if (!empty($poster['displayname'])) {
                        $TwitterPoster = $poster['displayname'];
                    } else {
                        $TwitterPoster = $poster['name'];
                    }

                    $twilink = "http://twitter.com/share?text=Look at this post (by $TwitterPoster) in \"$TwitterName\"&url=$TwitterURL";

                    $links['twitter'] = ['link' => urlencode($twilink), 'icon' => 'twitter', 'small' => __("Tweet"), 'text' => __('Share this post on Twitter')];
                }

                if ($notclosed) {
                    if ($loguserid && HasPermission('forum.postreplies', $forum) && !$params['noreplylinks']) {
                        $links['quote'] = ['link' => actionLink('newreply', $thread, 'quote='.$post['id']), 'icon' => 'quote-left', 'text' => __('Quote this post'), 'small' => __('Quote')];
                    }

                    if (($poster['id'] == $loguserid && HasPermission('forum.editownposts', $forum))
                        || HasPermission('mod.editposts', $forum)
                        || ($post['id'] == $post['firstpostid'] && HasPermission('mod.editfirstpost', $forum))
                    ) {
                        $links['edit'] = ['link' => actionLink('editpost', $post['id']), 'icon' => 'pencil', 'text' => __('Edit this post'), 'small' => __("Edit")];
                    }

                    if ((($poster['id'] == $loguserid && HasPermission('forum.deleteownposts', $forum))
                        || HasPermission('mod.deleteposts', $forum)) && $post['id'] != $post['firstpostid']
                    ) {
                        $delink = actionLink('editpost', $post['id'], 'delete=1&key='.$loguser['token']);
                        $links['delete'] = ['onclick' => "deletePost(this);return false;", 'btn' => 'danger', 'link' => $delink, 'text' => __('Delete This Post'), 'icon' => 'remove', 'small' => __("Delete")];
                    }

                    if ($loguserid && HasPermission('forum.reportposts', $forum)) {
                        $links['report'] = ['icon' => 'flag', 'btn' => 'danger', 'link' => pageLink('reportpost', ['id' => $post['id']]), 'text' => __('Report this post'), 'small' => __("Report")];
                    }
                }

                $bucket = 'post-topbar';
                include __DIR__.'/pluginloader.php';
            }

            $post['links_extra'] = $extraLinks;
        }

        //Threadlinks for listpost.php
        if ($params['threadlink']) {
            $thread = [];
            $thread['id'] = $post['thread'];
            $thread['title'] = $post['threadname'];
            $thread['forum'] = $post['fid'];

            $post['threadlink'] = makeThreadLink($thread);
        } else {
            $post['threadlink'] = '';
        }

        //Revisions
        if ($post['revision']) {
            $ru_link = UserLink(getDataPrefix($post, 'ru_'));
            $revdetail = ' '.format(__('by {0} on {1}'), $ru_link, formatdate($post['revdate']));

            if (HasPermission('mod.editposts', $forum)) {
                $post['revdetail'] = '<a href="javascript:void(0);" onclick="showRevisions('.$post['id'].')">'.Format(__('rev. {0}'), $post['revision']).'</a>'.$revdetail;
            } else {
                $post['revdetail'] = Format(__('rev. {0}'), $post['revision']).$revdetail;
            }
        }
        //</revisions>
    }

    $post['links'] = $links;

    // POST SIDEBAR

    $sidebar = [];

    $sidebar['rank'] = GetRank($poster['rankset'], $poster['posts']);

    if ($poster['title']) {
        $sidebar['title'] = strip_tags(CleanUpPost($poster['title'], '', true), '<b><strong><i><em><span><s><del><img><a><br/><br><small>');
    } elseif ($loguser['layout'] == 'acmlm') {
        $sidebar['title'] = htmlspecialchars($usergroups[$poster['primarygroup']]['title']);
    }

    $sidebar['group'] = htmlspecialchars($usergroups[$poster['primarygroup']]['title']);
    $sidebar['syndrome'] = GetSyndrome(getActivity($poster['id']));

    if ($post['mood'] > 0) {
        $mExt = FetchResult('SELECT ext FROM {moodavatars} WHERE uid={0} and mid = {1}', $poster['id'], $post['mood']);
        $ft = ['', 'gif', 'jpg', 'png'];

        if (file_exists(DATA_DIR.'avatars/'.$poster['id'].'_'.$post['mood'].'.'.$ft[$mExt])) {
            if ($acmlmboardLayout) {
                $sidebar['avatar'] = '<img src="'.DATA_URL.'avatars/'.$poster['id'].'_'.$post['mood'].'.'.$ft[$mExt].'" alt="" style="max-width: 200px; max-height: 200px;">';
            } else if ($mobileLayout) {
                $sidebar['avatar'] = '<img src="'.DATA_URL.'avatars/'.$poster['id'].'_'.$post['mood'].'.'.$ft[$mExt].'" alt="" style="max-width: 50px; max-height: 50px;">';
            } else {
                $sidebar['avatar'] = '<img src="'.DATA_URL.'avatars/'.$poster['id'].'_'.$post['mood'].'.'.$ft[$mExt].'" alt="" class="card-img-top" style="width:100%">';
            }
        }
    } elseif ($poster['picture']) {
        $pic = str_replace('$root/', DATA_URL, $poster['picture']);
        if ($acmlmboardLayout) {
            $sidebar['avatar'] = '<img src="'.htmlspecialchars($pic).'" alt="" style="max-width: 200px; max-height: 200px;">';
        } else if ($mobileLayout) {
            $sidebar['avatar'] = '<img src="'.htmlspecialchars($pic).'" alt="" style="max-width: 50px; max-height: 50px;">';
        } else {
            $sidebar['avatar'] = '<img src="'.htmlspecialchars($pic).'" alt="" class="card-img-top" style="width:100%">';
        }
    }

	$lastpost = ($poster['lastposttime'] ? timeunits(time() - $poster['lastposttime']) : 'none');
    $lastview = timeunits(time() - $poster['lastactivity']);

    $sidebar['lastpost'] = $lastpost;
    $sidebar['lastview'] = $lastview;
    $sidebar['posterID'] = $poster['id'];

	$sidebar['stars'] = $poster['postplusones'];
    $sidebar['level'] = getstats2($poster['id'])['lvl'];
    $sidebar['exp'] = getstats2($poster['id'])['exp'];
    $sidebar['next'] = calcexpleft(getstats2($poster['id'])['exp']);
    $sidebar['bar'] = drawrpglevelbar(getstats2($poster['id'])['exp']);

    if ($loguser['layout'] == 'acmlm') {
        if ($poster['lastactivity'] > time() - 300) {
            $sidebar['isonline'] = __('User is <strong>online</strong>');
        }
    } else {
        if ($poster['lastactivity'] > time() - 300) {
            $sidebar['isonline'] = '<img src="/img/online.png" title="User is Online" alt="User is Online">';
        } else {
            $sidebar['isonline'] = '<img src="/img/offline.png" title="User is Offline" alt="User is Offline">';
        }
    }

	$sidebar['posts'] = $poster['posts'];

    if (!$post['num']) {
        $sidebar['postsoutof'] = $poster['posts'];
    } else {
        $sidebar['postsoutof'] = $post['num'].'/'.$poster['posts'];
    }

    $sidebar['since'] = cdate($loguser['dateformat'], $poster['regdate']);
    if (!empty($poster['location'])) {
        $sidebar['from'] = htmlspecialchars($poster['location']);
    }

    $sidebarExtra = [];
    $bucket = 'sidebar';
    include __DIR__.'/pluginloader.php';
    $sidebar['extra'] = $sidebarExtra;

    $post['sidebar'] = $sidebar;

    // SIDEBAR LINKS

    $sidebarlinks = [];

    if (HasPermission('admin.banusers')
        && $loguserid != $poster['id']
        && $poster['primarygroup'] != Settings::get('bannedGroup')
    ) {
        $sidebarlinks[] = ['icon' => 'ban', 'text' => format(__('Ban {0}'), htmlentities($poster['name'])), 'link' => pageLink('ban', ['id' => $poster['id'], 'name' => slugify($poster['name'])])];
    }

    $sidebarlinks[] = ['icon' => 'copy', 'text' => format(__("List {0}'s posts"), htmlentities($poster['name'])), 'link' => pageLink('listposts', ['id' => $poster['id'], 'name' => slugify($poster['name'])])];
    $sidebarlinks[] = ['icon' => 'list', 'text' => format(__("List {0}'s threads"), htmlentities($poster['name'])), 'link' => pageLink('listthreads', ['id' => $poster['id'], 'name' => slugify($poster['name'])])];

    if (HasPermission('user.sendpms')) {
        $sidebarlinks[] = ['icon' => 'envelope', 'link' => actionLink('sendprivate', $poster['id'])];
    }
    $post['sidebarlinks'] = $sidebarlinks;

    // OTHER STUFF

    $post['haslayout'] = false;
    $post['fulllayout'] = false;

    if ($type == POST_DEPOT_REPLY || $type == POST_DEPOT_MAIN || $type == POST_PROFILE || $isBlocked || $mobileLayout) {
        $poster['postheader'] = '';
        $poster['signature'] = '';
    } elseif (!$isBlocked) {
        $poster['postheader'] = $pltype ? trim($poster['postheader']) : '';
        $poster['signature'] = trim($poster['signature']);

        $post['haslayout'] = $poster['postheader'] ? 1 : 0;
        $post['fulllayout'] = $poster['fulllayout'] && $post['haslayout'] && ($pltype == 2);

        if (!$post['haslayout'] && $poster['signature']) {
            $poster['signature'] = '<div class="signature">'.$poster['signature'].'</div>';
        }
    }

    $post['contents'] = makePostText($post, $poster);

    //PRINT THE POST!

	switch($type) {
		case POST_DEPOT_REPLY:
			RenderTemplate('postbox_comments', ['post' => $post]);
			break;
		case POST_DEPOT_MAIN:
			RenderTemplate('postbox_main', ['post' => $post]);
			break;
		case POST_PROFILE:
			return $post;
			break;
		default:
			RenderTemplate('postbox', ['post' => $post]);
	}
}
