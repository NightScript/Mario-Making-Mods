<?php

define('BLARG', 1);
$ajaxPage = true;
require __DIR__.'/../lib/common.php';
header('Cache-Control: no-cache');
header('Content-type: text/plain');

//Check if things are defined before using them, damnit!
if (isset($_GET['id'])) {
    $id = (int) $_GET['id'];
} else {
    $id = 0;
}

$rPost = Query(
    '
		SELECT
			p.id, p.date, p.num, p.deleted, p.deletedby, p.reason, p.options, p.mood, p.ip,
			pt.text, pt.revision, pt.user AS revuser, pt.date AS revdate,
			u.(_userfields), u.(rankset,title,posts,postheader,signature,signsep,lastposttime,lastactivity,regdate,globalblock,fulllayout),
			ru.(_userfields),
			du.(_userfields),
			t.forum fid
		FROM
			{posts} p
			LEFT JOIN {posts_text} pt ON pt.pid = p.id AND pt.revision = p.currentrevision
			LEFT JOIN {users} u ON u.id = p.user
			LEFT JOIN {users} ru ON ru.id=pt.user
			LEFT JOIN {users} du ON du.id=p.deletedby
			LEFT JOIN {threads} t ON t.id=p.thread
		WHERE p.id={0} AND t.forum IN ({1c})', $id, ForumsWithPermission('forum.viewforum')
);

if (!NumRows($rPost)) {
    die(__('Unknown post ID.'));
}
$post = Fetch($rPost);

if (!HasPermission('mod.deleteposts', $post['fid'])) {
    die(__('No.'));
}

die(json_encode(MakePost($post, POST_PROFILE, ['tid'=>$post['thread'], 'fid'=>$post['fid']])));
