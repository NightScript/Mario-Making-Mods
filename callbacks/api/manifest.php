<?php

define('BLARG', '1');
require __DIR__.'/../lib/common.php';

$manifest = [
    'name'        => htmlspecialchars(Settings::get('boardname')),
    'description' => htmlspecialchars(Settings::get('metaDescription')),
    'icon'        => [
        'src'  => BOARD_ROOT.'apple-touch-icon.png',
        'type' => 'image/png',
    ],
    'serviceworker' => [
        'src' => BOARD_ROOT.'serverworker.js',
    ],
    'display'   => 'standalone',
    'start_url' => pageLink('home'),
];

list($originalWidth, $originalHeight) = getimagesize($manifest['icon']['src']);
$manifest['icon']['size'] = $originalWidth.'x'.$originalHeight;

header('Content-type: text/json');
echo json_encode($manifest);
