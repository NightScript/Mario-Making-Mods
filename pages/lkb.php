<?php

CheckPermission('admin.viewips');

$title = __('Last known browsers');

$isMod = HasPermission('admin.ipsearch');
$sort = 'id ASC';
$ual = '';
if (isset($_GET['byua'])) {
    $sort = 'lastknownbrowser ASC';
    $ual .= 'byua&amp;';
}

MakeCrumbs([actionLink('admin') => __('Admin'), actionLink('lkb') => __('Last Known Browsers')]);

$numUsers = FetchResult('SELECT count(*) FROM {users}');

$ppp = $loguser['threadsperpage'];
if ($ppp < 1) {
    $ppp = 50;
}

if (isset($_GET['from'])) {
    $from = $_GET['from'];
} else {
    $from = 0;
}

$peeps = Query('SELECT u.(_userfields), u.lastip, u.lastknownbrowser, u.id FROM {users} u ORDER BY {0} LIMIT {1u}, {2u}', $sort, $from, $ppp);

$numonpage = NumRows($peeps);
$pagelinks = PageLinks(actionLink('lkb', '', $ual.'from='), $ppp, $from, $numUsers);
RenderTemplate('pagelinks', ['pagelinks' => $pagelinks, 'position' => 'top']);

$items = [];
while ($user = Fetch($peeps)) {
    $itemlist = [];
    $lip = $user['lastip'];
    $lkb = $user['lastknownbrowser'];
    $lkb = str_replace('-->', '', str_replace('<!--', ' &mdash;', $lkb));

    if ($isMod) {
        $itemlist['ip'] = formatIP($lip);
    } else {
        $itemlist['ip'] = IP2C($lip);
    }

    $itemlist['name'] = UserLink(getDataPrefix($user, 'u_'));
    $itemlist['id'] = $user['id'];
    $itemlist['lkb'] = $lkb;

    $items[] = $itemlist;
}

RenderTemplate('admin/lastknownbrowser', ['items' => $items]);
