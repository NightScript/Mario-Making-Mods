<?php

if (!defined('BLARG')) {
    die();
}

if (!$loguserid) {
	Kill(__("Please log in before trying to manage secondary groups"));
}

CheckPermission('admin.editusers');

if (!empty($_GET['option'])) {
	if ($_GET['option'] == 'add') {
		addUser($_GET['userid'], $_GET['groupid']);
		if ($_GET['userid'] && $_GET['groupid']) {
			$secgroup = $db->row('usergroups', ['id' => $_GET['groupid'], 'type' => 1]);
			if ($secgroup) {
				$user = $db->row('users', ['id' => $_GET['userid']]);
				if ($user) {
					$db->insert('secondarygroups', ['userid' => $_GET['userid'], 'groupid' => $_GET['groupid']]);
					Alert(__('Secondary group successfully added.'), __('Notice'));

					Report(format(__("{0}(#{1}) successfully added {1}(#{2}) to the {3} secondary group (#{4})"),
						$loguser['name'], $loguserid, $user['name'], $_GET['userid'], $secgroup['name'], $_GET['groupid']
					), false);
				} else {
					Alert(__("User does not exist"));
				}
			} else {
				Alert(__("Secondary group does not exist"));
			}
		} elseif (!$_GET['userid'] && $_GET['groupid']) {
			Report('[b]'.$loguser['name'].'[/] tried to add a secondary group (ID: '.$_GET['groupid'].') to someone.', false);
			Alert(__('Please enter a User ID and try again.'), __('Notice'));
		} elseif ($_GET['userid'] && !$_GET['groupid']) {
			Report('[b]'.$loguser['name'].'[/] tried to add a secondary group from user ID #'.$_GET['userid'].'.', false);
			Alert(__('Please enter a Group ID and try again.'), __('Notice'));
		}
	} elseif ($_GET['option'] == 'remove') {
		if ($_GET['userid'] && $_GET['groupid']) {
			Query(
				'DELETE FROM {secondarygroups} (userid,groupid) VALUES ({0},{1})',
				$_GET['userid'],
				$_GET['groupid']
			);
			Report('[b]'.$loguser['name'].'[/] successfully removed a secondary group (ID: '.$_GET['groupid'].') from user ID #'.$_GET['userid'].'', false);
			Alert(__('Secondary group successfully removed.'), __('Notice'));
		} elseif (!$_GET['userid'] && $_GET['groupid']) {
			Report('[b]'.$loguser['name'].'[/] tried to remove a secondary group (ID: '.$_GET['groupid'].') from someone.', false);
			Alert(__('Please enter a User ID and try again.'), __('Notice'));
		} elseif ($_GET['userid'] && !$_GET['groupid']) {
			Report('[b]'.$loguser['name'].'[/] tried to remove a secondary group from user ID #'.$_GET['userid'].'.', false);
			Alert(__('Please enter a Group ID and try again.'), __('Notice'));
		}
	} else {
		
	}
} else {
    Alert(__('Welcome to the Secondary groups page, where you can not only see all the current secondary groups, but you can also manage them.'), __('Welcome'));
}

$rSecGroups = Query('select userid, {secondarygroups}.groupid, {users}.name username from {secondarygroups} JOIN {users} WHERE userid = id order by userid desc');

$secList = '';
while ($sec = Fetch($rSecGroups)) {
    $cellClass = ($cellClass + 1) % 2;
    $secList .= "
	<tr class=\"cell$cellClass\">
		<td>".$sec['username'].'('.htmlspecialchars($sec['userid']).')</td>
		<td>'.htmlspecialchars($sec['groupid']).'</td>
	</tr>';
}

echo '
<table class="outline margin">
	<tr class="header1">
		<th>'.__('User ID').'</th>
		<th>'.__('Group ID').'</th>
	</tr>
	'.$secList.'
</table>

<table class="outline"><tr class="header1"><th colspan="2" class="center">Secondary Groups Manager</th></tr>
<form action="'.actionLink('secgroups').'" method="GET" onsubmit="submit.disabled = true; return true;">
<tr class="cell2"><td>User ID</td><td><input type="text" name="userid"></td></tr>
<tr class="cell1"><td>Group ID</td><td><input type="text" name="groupid"></td></tr>
<tr class="cell2"><td>Add/Remove</td><td><select name="option">
    <option value="add">Add</option>
    <option value="remove">Remove</option>
  </select></td></tr>
<tr><td colspan="2" class="cell2"><input type="submit" name="submit" value="Add"></td></tr>
</form>
</table>';

function addSecGroup($uid, $gid) {
	global $db;

	// If nothing is filled, kill it early on. No need to report or something like that, as it could have been an accident.
	if (empty($uid) && empty($gid)) {
		return;
	}

	// Now, if something else is filled in, now we obviously know it's a mistake.
	// Run checks to see if the thing actually exists too and if the administrator is drunk. If it's the case, let the other staff members know.
	// Run an elseif on this entire check as well since this is part of one step
	if (!empty($uid) && empty($gid)) {
		$user = $db->row('users', ['id' => $uid]);
		$validuser = false;

		// The checks here will be used for reporting to the logs
		if ($user) {
			Report(format(__('{0} (#{1}) tried to add an invalid secondary group to {2} (#{3}).'),
			$loguser['name'], $loguserid, $user['name'], $uid), false);

			$validuser = true;
		} else {
			Report(format(__('{0} (#{1}) tried to add an invalid secondary group to an invalid user (#{2}).'),
			$loguser['name'], $loguserid, $uid), false);
		}

		if ($validuser) {
			Alert(__('Please enter a Group ID and try again.'), __('The secondary group was not added'));
		} else {
			Alert(__('Please enter a Group ID with a valid user and try again.'), __('The secondary group was not added'));
		}

		return;
	} elseif (empty($uid) && !empty($gid)) {
		$group = $db->row('usergroups', ['id' => $gid]);
		$validgroup = false;

		// Unlike the user checks, we also need to check whether that group can be added in the first place
		if (!$group) {
			Report(format(__('{0} (#{1}) tried to invite an invalid user to an invalid secondary group (#{2}).'),
			$loguser['name'], $loguserid, $gid), false);
		} elseif ($group['type'] !== 1) {
			Report(format(__('{0} (#{1}) tried to add the {2} primary group (#{3}) as a secondary group to an invalid user.'),
			$loguser['name'], $loguserid, $group['name'], $gid), false);
		} else {
			Report(format(__('{0} (#{1}) tried to invite an invalid user to the {2} secondary group (#{3}).'),
			$loguser['name'], $loguserid, $group['name'], $gid), false);

			$validgroup = true;
		}

		if ($validgroup) {
			Alert(__('Please specify a user and try again.'), __('The secondary group was not added'));
		} else {
			Alert(__('Please specify a user and a valid Group ID and try again.'), __('The secondary group was not added'));
		}

		return;
	}

	// (No need to add an if check, since I already added them for the stuff above)
	// Final part of the main NightYoshi step formula: Since we know the stuff is there, now we can easily check if it's valid or not.
	$validgroup = False;
	$validuser = True;

	
}