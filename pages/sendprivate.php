<?php

if (!defined('BLARG')) {
    die();
}

$title = __('Private messages');

MakeCrumbs([actionLink('private') => __('Private messages'), '' => __('Send PM')]);

if (!$loguserid) { //Not logged in?
    Kill(__('You must be logged in to send private messages.'));
}

CheckPermission('user.sendpms');

$draftID = 0;
$replyTo = 0;
$convStart = 0;
$urlargs = [];

$pid = (int) $_GET['pid'];
if ($pid) {
    $urlargs[] = 'pid='.$pid;

    // this shouldn't select drafts someone else is preparing for us
    // those drafts will have recipients stored in draft_to, with userto set to 0
    $rPM = Query('SELECT * FROM {pmsgs} LEFT JOIN {pmsgs_text} ON pid = {pmsgs}.id WHERE (userfrom={0} OR userto={0}) AND {pmsgs}.id = {1}', $loguserid, $pid);
    if (NumRows($rPM)) {
        $pm = Fetch($rPM);
        $user = $db->row('users', ['id' => $pm['userfrom']]);
        if (!$user) {
            Kill(__('Unknown user.'));
        }

        $prefill = $pm['text'];
        $trefill = $pm['title'];

        if (!$pm['drafting']) {
            $convStart = $pm['conv_start'] ?: $pm['id'];
            $replyTo = $pm['userfrom'];

            $prefill = '[reply="'.$user['name'].'"]'.$prefill.'[/reply]';

            if (strpos($pm['title'], 'Re: Re: Re: ') !== false) {
                $trefill = str_replace('Re: Re: Re: ', 'Re*4: ', $pm['title']);
            } elseif (preg_match("'Re\*([0-9]+): 'se", $pm['title'], $reeboks)) {
                $trefill = 'Re*'.((int) $reeboks[1] + 1).': '.substr($pm['title'], strpos($pm['title'], ': ') + 2);
            } else {
                $trefill = 'Re: '.$pm['title'];
            }

            if (!isset($_POST['to'])) {
                $_POST['to'] = $user['name'];
            }
        } else {
            $draftID = $pid;
            $convStart = $pm['conv_start'];

            $_POST['to'] = $pm['draft_to'];
        }
    } else {
        Kill(__('Unknown PM.'));
    }
}

$uid = (int) $_GET['id'];
if ($uid && !$_POST['to']) {
    $rUser = Query('SELECT name FROM {users} WHERE id = {0}', $uid);
    if (NumRows($rUser)) {
        $user = Fetch($rUser);
        $_POST['to'] = $user['name'];
    } else {
        Kill(__('Unknown user.'));
    }
}

if ($_POST['actiondelete'] && $draftID) {
    Query('DELETE FROM {pmsgs} WHERE id={0} AND drafting=1', $draftID);
    Query('DELETE FROM {pmsgs_text} WHERE pid={0}', $draftID);

    die(header('Location: '.actionLink('private')));
}

$userMySQL = Query('SELECT name FROM {users} WHERE id <> {0} ORDER BY name ASC', $loguserid);
$mySQLnames = [];

while ($row = fetch($userMySQL)) {
    $mySQLnames[] = htmlspecialchars($row['name']);
}

$recipIDs = [];
if ($_POST['to']) {
    $recipients = explode(';', $_POST['to']);
    foreach ($recipients as $to) {
        $to = trim(htmlentities($to));
        if (empty($to)) {
            continue;
        }

        $user = $db->row('users', [['name' => $to, 'displayname' => $to]]);
        if ($user) {
            $id = $user['id'];

            if (!in_array($id, $recipIDs)) {
                $recipIDs[] = $id;
            }
        } else {
            $errors .= format(__('Unknown user "{0}"'), $to).'<br />';
        }
    }

    $maxRecips = 5;
    if (count($recipIDs) > $maxRecips) {
        $errors .= __('Too many recipients.');
    }
    if (!empty($errors)) {
        Alert($errors);
        unset($_POST['actionsend']);
        unset($_POST['actionsave']);
    }
} else {
    if ($_POST['actionsend'] || $_POST['actionsave']) {
        Alert(__('Please enter a recipient and try again.'), __('Your Private Message has no recipient.'));
        unset($_POST['actionsend']);
        unset($_POST['actionsave']);
    }
}

if ($_POST['actionsend'] || $_POST['actionsave']) {
    if ($_POST['title']) {
        $_POST['title'] = $_POST['title'];

        if (!empty(strip_html_tags(parseBBCode($_POST['text'])))) {
            $wantDraft = ($_POST['actionsave'] ? 1 : 0);

            $bucket = 'checkPost';
            include BOARD_ROOT.'lib/pluginloader.php';

            $post = $_POST['text'];
            $post = preg_replace("'/me '", '[b]* '.htmlspecialchars($loguser['name']).'[/b] ', $post); //to prevent identity confusion

            if ($wantDraft) {
                if ($draftID) {
                    $db->updateId('pmsgs_text', ['title' => $_POST['title'], 'text' => $post], 'pid', $draftID);
                    $db->update('pmsgs', ['conv_start' => $convStart, 'draft_to' => $_POST['to']], ['id' => $_POST['to'], 'drafting' => 1]);
                } else {
                    // Post Random ID
                    $pid = mt_rand();
                    $pxist = FetchResult('SELECT * FROM {posts} WHERE id = {0}', $pid);
                    $pnuke = FetchResult("SELECT * FROM {nuked} WHERE id = {0} AND type = 'pm'", $pid);
                    while ($pnuke == $pid or $pxist == $pid) {
                        while ($pxist == $pid) {
                            $pid = mt_rand();
                            $pxist = FetchResult('SELECT * from {posts} WHERE id = {0}', $pid);
                        }
                        $pid = mt_rand();
                        $pxist = FetchResult('SELECT * from {posts} WHERE id = {0}', $pid);
                        $pnuke = FetchResult("SELECT * from {nuked} WHERE id = {0} AND type = 'pm'", $pid);
                    }

                    $db->insert('pmsgs_text', ['pid' => $pid, 'title' => $_POST['title'], 'text' => $post]);
                    $db->insert('pmsgs', [
                        'id' => $pid, 'userto' => 0, 'userfrom' => $loguserid, 'conv_start' => $convStart, 'date' => $db->time(),
                        'ip' => $_SERVER['REMOTE_ADDR'], 'drafting' => 1, 'draft_to' => $_POST['to'],
                    ]);
                }

                if ($acmlmboardLayout == true) {
                    OldRedirect(__('Draft saved!'), actionLink('private', '', 'show=2'), __('your drafts box'));
                } else {
                    die(header('Location: '.actionLink('private', '', 'show=2')));
                }
            } else {
                if ($draftID) {
                    $pid = $draftID;
                    Query('DELETE FROM {pmsgs} WHERE id={0} AND drafting=1', $pid);
                } else {
                    // Post Random ID
                    $pid = mt_rand();
                    $pxist = FetchResult('SELECT * FROM {posts} WHERE id = {0}', $pid);
                    $pnuke = FetchResult("SELECT * FROM {nuked} WHERE id = {0} AND type = 'pm'", $pid);
                    while ($pnuke == $pid or $pxist == $pid) {
                        while ($pxist == $pid) {
                            $pid = mt_rand();
                            $pxist = FetchResult('SELECT * from {posts} WHERE id = {0}', $pid);
                        }
                        $pid = mt_rand();
                        $pxist = FetchResult('SELECT * from {posts} WHERE id = {0}', $pid);
                        $pnuke = FetchResult("SELECT * from {nuked} WHERE id = {0} AND type = 'pm'", $pid);
                    }

                    $db->insert('pmsgs_text', ['pid' => $pid, 'title' => $_POST['title'], 'text' => $post]);
                }

                foreach ($recipIDs as $recipient) {
                    $cs = ($recipient == $replyTo) ? $convStart : 0;

                    $db->insert('pmsgs', [
                        'id'   => $pid, 'userto' => $recipient, 'userfrom' => $loguserid, 'conv_start' => $cs,
                        'date' => $db->time(), 'ip' => $_SERVER['REMOTE_ADDR'], 'msgread' => 0, 'drafting' => 0,
                    ]);

                    SendNotification('pm', $pid, $recipient);
                }

                if ($acmlmboardLayout == false) {
                    die(header('Location: /'.actionLink('private', '', 'show=1')));
                } else {
                    OldRedirect(__('Private Message sent!'), actionLink('private', '', 'show=1'), __('your PM outbox'));
                }
            }
        } else {
            Alert(__('Enter a message and try again.'), __('Your PM is empty.'));
        }
    } else {
        Alert(__('Enter a title and try again.'), __('Your PM is untitled.'));
    }
}

if ($_POST['to'])		$tofill    = htmlspecialchars($_POST['to']);
if ($_POST['text'])		$postfill  = htmlspecialchars($_POST['text']);
if ($_POST['title'])	$titlefill = htmlspecialchars($_POST['title']);

if (($_POST['actionpreview'] || $draftID) && $prefill) {
    $previewPost['text'] = $prefill;
    $previewPost['num'] = 0;
    $previewPost['posts'] = $loguser['posts'];
    $previewPost['id'] = 0;
    $previewPost['options'] = 0;

    foreach ($loguser as $key => $value) {
        $previewPost['u_'.$key] = $value;
    }

    MakePost($previewPost, POST_SAMPLE);
}

LoadPostToolbar();

$fields = [
    'to'			=> '<input type="text" placeholder="'.__('To').'"      class="form-control" name="to"    list="nameList" size=40 maxlength="128" value="'.$tofill.'">',
    'title'			=> '<input type="text" placeholder="'.__('Title').'"   class="form-control" name="title" id="tags"       size=80 maxlength="60"  value="'.$titlefill.'">',
    'text'			=> '<textarea          placeholder="'.__('Message').'" class="form-control" name="text"  id="text"       rows="16">'.$postfill.'</textarea>',

    'btnSend'		=> '<input class="btn btn-primary" type="submit" name="actionsend"    value="'.__('Send').'">',
    'btnPreview'	=> '<input class="btn btn-default" type="submit" name="actionpreview" value="'.__('Preview').'">',
    'btnSaveDraft'	=> '<input class="btn btn-success" type="submit" name="actionsave"    value="'.__('Save draft').'">',
];

if ($draftID) {
    $fields['btnDeleteDraft'] = '<input class="btn btn-danger"  type="submit" name="actiondelete"  value="'.__('Delete draft')."\" onclick=\"if(!confirm('Really delete this draft?'))return false;\">";
}

echo '<form name="postform" action="'.pageLink('sendprivate').'" method="post">';
RenderTemplate('form_sendprivate', ['fields' => $fields, 'draftMode' => $draftID ? true : false, 'maxRecipients' => 5, 'mySQLnames' => $mySQLnames]);
echo '
	</form>
	<script type="text/javascript">
		document.postform.text.focus();
	</script>
';
