<?php

if (!defined('BLARG')) {
    die();
}

$title = __('Edit Smilies');
MakeCrumbs([pageLink('admin') => __('Admin'), pageLink('smilies') => __('Edit smilies')]);

CheckPermission('admin.editsmilies');

if (isset($_POST['action']) && $loguser['token'] != $_POST['key']) {
    Kill(__('No.'));
}

$rSmilies = Query('SELECT * FROM {smilies}');
if ($_POST['action'] == 'Apply') {
    $numSmilies = NumRows($rSmilies);

    for ($i = 0; $i <= $numSmilies; $i++) {
        if ($_POST['code_'.$i] != $_POST['oldcode_'.$i] || $_POST['image_'.$i] != $_POST['oldimage_'.$i]) {
            if (!empty($_POST['code_'.$i])) {
                Report($loguser['name'].' deleted the "'.$_POST['oldcode_'.$i].'" smiley', 1);
                $act = 'deleted';
                $rSmiley = Query('DELETE FROM {smilies} WHERE code={0}', $_POST['oldcode_'.$i]);
            } else {
                Report($loguser['name'].' changed the smiley name "'.$_POST['oldcode_'.$i].'" to "'.$_POST['image_'.$i].'"', 1);
                $act = 'edited to "'.$_POST['image_'.$i].'"';
                $db->updateId('smilies', ['code' => $_POST['code_'.$i], 'image' => $_POST['image_'.$i]], 'id', $_POST['oldcode_'.$i]);
            }
            $log .= $_POST['oldcode_'.$i].' was '.$act;
        }
    }

    if ($_POST['code_add'] && $_POST['image_add']) {
        $db->insert('smilies', ['code' => $_POST['code_add'], 'image' => $_POST['image_add']]);
        Report($loguser['name'].' added the "'.$_POST['code_add'].'" smiley', 1);
        $log .= 'Smiley "'.$_POST['code_add'].'" added.<br />';
    }

    if ($log) {
        Alert($log, 'Log');
    }
}

$smileyList = '';
while ($smiley = Fetch($rSmilies)) {
    $i++;

    $smileyList .= format(
        '
			<tr>
				<td>
					<input type="text" name="code_{0}" value="{1}" />
					<input type="hidden" name="oldcode_{0}" value="{1}" />
				</td>
				<td>
					<input type="text" name="image_{0}" value="{2}" />
					<input type="hidden" name="oldimage_{0}" value="{2}" />
				</td>
				<td>
					<img src="{3}" alt="{4}" title="{4}">
				</td>
			</tr>
', $i, $smiley['code'], $smiley['image'], htmlspecialchars(resourceLink('img/smilies/'.$smiley['image'])), $smiley['code']
    );
}

echo '<form method="post" action="'.pageLink('smilies').'">';

RenderTemplate(
    'editsmilies', [
    'smileyList' => $smileyList,
    'code_add'   => '<input type="text" name="code_add" />',
    'image_add'  => '<input type="text" name="image_add" />',
    'apply'      => '<input type="submit" name="action" value="Apply" />',
    'token'      => '<input type="hidden" name="key" value="'.$loguser['token'].'" />',
    ]
);

echo '</form>';
