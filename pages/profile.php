<?php

if (!defined('BLARG')) {
    die();
}

$id = $pageParams['id'];

if (!isset($id)) {
    Kill(__('Please specify a user ID.'));
} elseif (!is_numeric($id)) {
    $user = $db->row('users', [['displayname' => $id, 'name' => $id]]);
} else {
    $user = $db->row('users', ['id' => $id]);
}

if (!$user) {
    Kill(__('Unknown user ID.'));
}

$uname = $user['displayname'] ?: $user['name'];

$ugroup = $usergroups[$user['primarygroup']];
$usgroups = [];

$res = Query('SELECT groupid FROM {secondarygroups} WHERE userid={0}', $user['id']);
while ($sg = Fetch($res)) {
    $usgroups[] = $usergroups[$sg['groupid']];
}

if ($user['id'] == $loguserid) {
    $db->updateId('users', ['lastprofileview' => $db->time()], 'id', $loguserid);
    DismissNotification('profilecomment', $loguserid, $loguserid);
}

$canShowPost = !empty(trim($user['bio'])) || !empty(trim($user['postheader'])) || !empty(trim($user['signature'])) || !empty(trim($user['picture']));
$canDeleteComments = ($user['id'] == $loguserid && HasPermission('user.deleteownusercomments')) || HasPermission('admin.adminusercomments');
$canComment = (HasPermission('user.postusercomments') && $user['primarygroup'] != Settings::get('bannedGroup')) || HasPermission('admin.adminusercomments');

if ($loguserid && $_REQUEST['token'] == $loguser['token']) {
    if ($http->get('action') == 'block') {
        $isBlocked = $db->row('blockedlayouts', ['user' => $user['id'], 'blockee' => $loguserid]);
        if (!$isBlocked) {
			$db->insert('blockedlayouts', ['user' => $user['id'], 'blockee' => $loguserid]);
            $blocksentence = __('Layout Blocked!');
        } else {
            Query('DELETE FROM {blockedlayouts} WHERE user={0} and blockee={1}', $user['id'], $loguserid);
            $blocksentence = __('Layout Unblocked!');
        }

        if ($acmlmboardLayout) {
            OldRedirect($blocksentence, pageLink(
                    'profile', [
                        'id'   => $user['id'],
                        'name' => slugify($user['name']),
                    ]
                ), format(__("{0}'s profile"), htmlspecialchars($user['displayname'] ? $user['displayname'] : $user['name']))
            );
        } else {
            newRedir(pageLink('profile', ['id' => $user['id'], 'name' => slugify($user['name'])]));
        }
    }

    if ($http->get('action') == 'delete') {
        $postedby = FetchResult('SELECT cid FROM {usercomments} WHERE uid={0} AND id={1}', $user['id'], (int) $http->get('cid'));
        if ($canDeleteComments || ($postedby == $loguserid && HasPermission('user.deleteownusercomments'))) {
            Query('DELETE FROM {usercomments} WHERE uid={0} AND id={1}', $user['id'], (int) $http->get('cid'));
            if ($loguserid != $user['id']) {
                // dismiss any new comment notification that has been sent to that user, unless there are still new comments
                $lastcmt = FetchResult('SELECT date FROM {usercomments} WHERE uid={0} ORDER BY date DESC LIMIT 1', $user['id']);
                if ($lastcmt < $user['lastprofileview']) {
                    DismissNotification('profilecomment', $user['id'], $user['id']);
                }
            }

            if ($acmlmboardLayout) {
                OldRedirect(
                    __('Comment Deleted'), pageLink(
                        'profile', [
                        'id'   => $user['id'],
                        'name' => slugify($user['name']),
                        ]
                    ), format(__("{0}'s profile"), htmlspecialchars($user['displayname'] ? $user['displayname'] : $user['name']))
                );
            } else {
                newRedir(pageLink('profile', ['id' => $user['id'], 'name' => slugify($user['name'])]));
            }
        }
    }

    if (isset($_POST['actionpost']) && !empty($_POST['text']) && $canComment) {
        $text = utfmb4String($_POST['text']);
        $db->insert('usercomments', ['uid' => $user['id'], 'cid' => $loguserid, 'text' => $text, 'date' => $db->time()]);
        if ($loguserid != $user['id']) {
            SendNotification('profilecomment', $user['id'], $user['id']);
        }

		if ($acmlmboardLayout) {
            OldRedirect(__('Comment Deleted'), pageLink(
                    'profile', [
                        'id'   => $user['id'],
                        'name' => slugify($user['name']),
                    ]
                ), format(__("{0}'s profile"), htmlspecialchars($user['displayname'] ? $user['displayname'] : $user['name']))
            );
		} else {
            newRedir(pageLink('profile', ['id' => $user['id'], 'name' => slugify($user['name'])]));
		}
    }
}

if ($loguserid) {
    if (Settings::get('postLayoutType')) {
        $blocktext = ['text' => __('Block layout')];
        $unblocktext = ['text' => __('Unblock layout')];
    } else {
        $blocktext = ['text' => __('Block signature')];
        $unblocktext = ['text' => __('Unblock signature')];
    }

    $isBlocked = $db->row('blockedlayouts', ['user' => $user['id'], 'blockee' => $loguserid]);

    $blockLink = pageLink(
        'profile', [
            'id'   => $user['id'],
            'name' => slugify($user['name']),
        ], "token={$loguser['token']}&action=block"
    );
}

$daysKnown = ($db->time() - $user['regdate']) / 86400;
if (!$daysKnown) {
    $daysKnown = 1;
}

$minipic = getMinipicTag($user);

if ($user['rankset']) {
    $currentRank = GetRank($user['rankset'], $user['posts']);
    $toNextRank = GetToNextRank($user['rankset'], $user['posts']);
    if ($toNextRank) {
        $toNextRank = Plural($toNextRank, 'post');
    }
}

if ($user['title']) {
    $title = preg_replace('@<br.*?>\s*(\S)@i', ' &bull; $1', strip_tags(CleanUpPost($user['title'], '', true), '<b><strong><i><em><span><s><del><img><a><br><br/><small>'));
}

$homepage = '';
if ($user['homepageurl']) {
    $nofollow = '';
    if (Settings::get('nofollow')) {
        $nofollow = 'rel="nofollow"';
    }

    if ($user['homepagename']) {
        $homepage = "<a $nofollow target=\"_blank\" href=\"".htmlspecialchars($user['homepageurl']).'">'.htmlspecialchars($user['homepagename']).'</a> - '.htmlspecialchars($user['homepageurl']);
    } else {
        $homepage = "<a $nofollow target=\"_blank\" href=\"".htmlspecialchars($user['homepageurl']).'">'.htmlspecialchars($user['url']).'</a>';
    }
    $homepage = securityPostFilter($homepage);
}

if ($user['showemail']) {
    $emailField = '<span id="emailField">'.__('Public')." <button onclick=\"$(this.parentNode).load('".URL_ROOT.'ajaxcallbacks.php?a=em&id='.$user['id']."');\">".__('Show').'</button></span>';
} elseif (HasPermission('admin.editusers')) {
    $emailField = '<span id="emailField">'.__('Private')." <button onclick=\"$(this.parentNode).load('".URL_ROOT.'ajaxcallbacks.php?a=em&id='.$user['id']."');\">".__('Snoop').'</button></span>';
}

$profileParts = [];

$temp = [];
addField(__('Name'), $minipic.htmlspecialchars($user['displayname'] ? $user['displayname'].' ('.htmlspecialchars($user['name']).')' : $user['name']));
addField(__('Title'), $title);
addField(__('User ID'), $user['id']);

$glist = '<strong class="userlink" style="color: '.htmlspecialchars($ugroup['color_unspec']).';">'.htmlspecialchars($ugroup['name']).'</strong>';
foreach ($usgroups as $sgroup) {
    if ($sgroup['display'] > -1) {
        $glist .= ', '.htmlspecialchars($sgroup['name']);
    }
}
addField(__('Groups'), $glist);
addField(__('Rank'), $currentRank);
addField(__('To next rank'), $toNextRank);

if ($user['posts']) {
	$averagePosts = sprintf('%1.02f', $user['posts'] / $daysKnown);

	addField(pageLinkTag(__('Total posts'), 'listposts', ['id' => $user['id'], 'name' => slugify($user['name'])]),
	format(__('{0} ({1} per day)'), $user['posts'], $averagePosts));
} else {
	addField(__('Total posts'), $user['posts']);
}

$user['threads'] = FetchResult("select count(*) from {threads} where user={0}", $user['id']);
if ($user['threads']) {
	$averageThreads = sprintf('%1.02f', $user['threads'] / $daysKnown);

	addField(pageLinkTag(__('Total threads'), 'listthreads', ['id' => $user['id'], 'name' => slugify($user['name'])]),
	format(__('{0} ({1} per day)'), $user['threads'], $averageThreads));
} else {
	addField(__('Total threads'), $user['threads']);
}

addField(__('Registered on'), format(__('{0} ({1} ago)'), formatdate($user['regdate']), TimeUnits($daysKnown * 86400)));

$lastPost = Fetch(
    Query(
        '
	SELECT
		p.id as pid, p.date as date,
		{threads}.title AS ttit, {threads}.id AS tid,
		{forums}.title AS ftit, {forums}.id AS fid
	FROM {posts} p
		LEFT JOIN {users} u on u.id = p.user
		LEFT JOIN {threads} on {threads}.id = p.thread
		LEFT JOIN {forums} on {threads}.forum = {forums}.id
	WHERE p.user={0}
	ORDER BY p.date DESC
	LIMIT 0, 1', $user['id']
    )
);

if ($lastPost) {
    $thread = [];
    $thread['title'] = $lastPost['ttit'];
    $thread['id'] = $lastPost['tid'];
    $thread['forum'] = $lastPost['fid'];
    $tags = ParseThreadTags($thread['title']);

    if (!HasPermission('forum.viewforum', $lastPost['fid'])) {
        $place = __('a restricted forum');
    } else {
        $ispublic = HasPermission('forum.viewforum', $lastPost['fid'], true);
        $pid = $lastPost['pid'];
        $place = actionLinkTag($tags[0], 'post', $pid).' ('.actionLinkTag($lastPost['ftit'], 'forum', $lastPost['fid'], '', $ispublic ? $lastPost['ftit'] : '').')';
    }

	if($user['posts']) {
		addToField(pageLinkTag(__('Total posts'), 'listposts', ['id' => $user['id'], 'name' => slugify($user['name'])]),
		format(__('(last post {0} ago in {1})'), TimeUnits($db->time() - $lastPost['date']), $place));
	} else {
		addToField(__('Total posts'), format(__('(last post {0} ago in {1})'), TimeUnits($db->time() - $lastPost['date']), $place));
	}
}

addField(__('Last view'), format(__('{0} ({1} ago)'), formatdate($user['lastactivity']), TimeUnits($db->time() - $user['lastactivity'])));
addToField(__('Last view'), 'at: <a>'.$user['lasturl'].'</a>');

addField(__('Last user agent'), htmlspecialchars($user['lastknownbrowser']), 'admin.viewips');
addField(__('Last IP address'), formatIP($user['lastip']), 'admin.viewips');

addCategory(__('General information'));

$temp = [];
addField(__('Email address'), $emailField);
addField(__('Homepage'), $homepage);
addCategory(__('Contact information'));

$temp = [];
$infofile = 'themes/'.$user['theme'].'/themeinfo.txt';

if (file_exists($infofile)) {
    $themeinfo = file_get_contents($infofile);
    $themeinfo = explode("\n", $themeinfo, 2);

    $themename = htmlspecialchars(trim($themeinfo[0]));
    $themeauthor = htmlspecialchars(trim($themeinfo[1]));
    $themeauthor = parseBBCode($themeauthor);

    $theme = $user['theme'];
} else {
    $themename = htmlspecialchars($user['theme']);
    $themeauthor = '';
}

$userlayout = $user['layout'];
if (!empty(trim($userlayout))) {
	$userlayout = Settings::get('defaultLayout');
}
if (!empty(trim($userlayout))) {
	$userlayout = 'modern';
}

$layoutinfofile = 'layout/'.$user['layout'].'/info.txt';
if (file_exists($layoutinfofile)) {
    $layoutinfo = file_get_contents($layoutinfofile);
    $layoutinfo = explode("\n", $layoutinfo, 2);

    $layoutname = htmlspecialchars(trim($layoutinfo[0]));
} else {
    $layoutname = htmlspecialchars($user['layout']);
}

addField(__('Theme'), $themename.' &middot; '.$themeauthor);
addField(__('Items per page'), Plural($user['postsperpage'], __('post')).', '.Plural($user['threadsperpage'], __('thread')));
addField(__('Layout'), $layoutname);
addCategory(__('Presentation'));

//Gaming Profile Stuff added here
$temp = [];
addField(__('Nintendo Network ID'), htmlspecialchars($user['NNID']));
addExtraToField(__('Nintendo Network ID'), $user['NNID'], '(<a href="https://supermariomakerbookmark.nintendo.net/profile/'.htmlspecialchars($user['NNID']).'?type=posted">Super Mario Maker Bookmark Profile</a>)');
addField(__('Nintendo Switch Friend Code'), htmlspecialchars($user['NX']));
addField(__('Nintendo 3DS Friend Code'), htmlspecialchars($user['3DS']));
addCategory(__('Gaming Profiles'));

$temp = [];
addField(__('Real name'), htmlspecialchars($user['realname']));
addField(__('Location'), htmlspecialchars($user['location']));
addExtraToField(__('Birthday'), $user['birthday'], formatBirthday($user['birthday']));
addCategory(__('Personal information'));

$bucket = 'profileTable';
require BOARD_ROOT.'lib/pluginloader.php';

$cpp = $loguser['postsperpage'];
if (!$cpp) {
    $cpp = 20;
}
$total = FetchResult(
    'SELECT
						COUNT(*)
					FROM {usercomments}
					WHERE uid={0}', $user['id']
);

$from = (int) $http->get('from');
if ($http->get('from') === null) {
    $from = 0;
}
$realFrom = $total - $from - $cpp;
$realLen = $cpp;
if ($realFrom < 0) {
    $realLen += $realFrom;
    $realFrom = 0;
}
$rComments = Query(
    'SELECT
		u.(_userfields),
		uc.id, uc.cid, uc.text, uc.date
		FROM {usercomments} uc
		LEFT JOIN {users} u ON u.id = uc.cid
		WHERE uc.uid={0}
		ORDER BY uc.date DESC LIMIT {1u},{2u}', $user['id'], $from, $cpp
);

$pagelinks = PageLinksInverted(pageLink('profile', ['id' => $user['id'], 'name' => slugify($user['name'])], 'from='), $cpp, $from, $total);

$comments = [];
while ($comment = Fetch($rComments)) {
    $cmt = [];

    $deleteLink = '';
    if ($canDeleteComments || ($comment['cid'] == $loguserid && HasPermission('user.deleteownusercomments'))) {
        $deleteLink = '<span style="float: right; margin: 0px 4px;">'.
            actionLinkTag('Delete', 'profile', $user['id'], 'action=delete&cid='.$comment['id']."&token={$loguser['token']}").'</span>';
    }

    $cmt['deleteLink'] = $deleteLink;

    $cmt['userlink'] = UserLink(getDataPrefix($comment, 'u_'));
    $cmt['formattedDate'] = relativedate($comment['date']);
    $cmt['text'] = CleanUpPost($comment['text']);
    if ($comment['picture']) {
        $cmt['avatar'] = str_replace('$root/', DATA_URL, $comment['picture']);
    }

    $comments[] = $cmt;
}

$commentField = '';
if ($canComment) {
    $commentField = '
		<form name="commentform" method="post" action="'.htmlentities(pageLink('profile', [
            'id'   => $user['id'],
            'name' => slugify($user['name']),
        ])).'">
			<input type="hidden" name="id" value="'.$user['id'].'">
			<input type="text" class="form-control" style="width: 90%; display: inline;" name="text" maxlength="255"></textarea>
			<input type="submit" name="actionpost" class="btn btn-primary" value="'.__('Post')."\">
			<input type=\"hidden\" name=\"token\" value=\"{$loguser['token']}\">
		</form>";
}

$rpgstatus = resourceLink('gfx/rpgstatus.php').'?u='.$user['id'];

$rEQ = Query('SELECT * FROM {usersrpg} WHERE id={0}', $user['id']);
if (NumRows($rEQ)) {
    $equipitems = [];

    $shops = Query('SELECT * FROM {itemcateg} ORDER BY corder');
    $eq = Fetch($rEQ);
    $eqitems = Query('SELECT * FROM {items} WHERE id IN ({0c})', $eq);

    while ($item = fetch($eqitems)) {
        $items[$item['id']] = $item;
    }

    while ($shop = fetch($shops)) {
        $itemdesc = [];

        $id = $eq['eq'.$shop['id']];

        $itemdesc['name'] = $shop['name'];
        $itemdesc['desc'] = actionLinkTag($items[$id]['name'], 'shop', $id, 'action=desc');
        $equipitems[] = $itemdesc;
    }
}

$badgersR = Query('SELECT * FROM {badges} WHERE owner={0} order by color', $user['id']);
if (NumRows($badgersR)) {
    $colors = ['default', 'primary', 'success', 'info'];
    $badgers = [];
    while ($badger = Fetch($badgersR)) {
        if ($badger['color'] == '0' || $badger['color'] == '1' || $badger['color'] == '2' || $badger['color'] == '3') {
            $badgers[] = Format('<span class="badge badge-{0}">{1}</span>', $colors[$badger['color']], $badger['name']);
        } else {
            $badgers[] = Format('<img src="{0}" alt="{1}">', $badger['color'], $badger['name']);
        }
    }
}

if (!$acmlmboardLayout && $canShowPost) {
    if (trim($user['bio'])) {
        $previewPost['text'] = $user['bio'];
    } elseif (trim($user['postheader'])) {
        $previewPost['text'] = Settings::get('profilePreviewText');
    } else {
        $previewPost['text'] = '';
    }

    $previewPost['num'] = 0;
    $previewPost['id'] = 0;

    foreach ($user as $key => $value) {
        $previewPost['u_'.$key] = $value;
    }

    MakePost($previewPost, POST_SAMPLE);
}

$rRecent = Query(
    "
			SELECT
				p.*,
				pt.text, pt.revision, pt.user AS revuser, pt.date AS revdate,
				u.(_userfields), u.(lastposttime,lastactivity,regdate,globalblock,fulllayout),
				ru.(_userfields),
				t.id thread, t.title threadname,
				f.id fid
			FROM
				{posts} p
				LEFT JOIN {posts_text} pt ON pt.pid = p.id AND pt.revision = p.currentrevision
				LEFT JOIN {users} u ON u.id = p.user
				LEFT JOIN {users} ru ON ru.id=pt.user
				LEFT JOIN {threads} t ON t.id=p.thread
				LEFT JOIN {forums} f ON f.id=t.forum
				LEFT JOIN {categories} c ON c.id=f.catid
			WHERE u.id={1} AND p.deleted <> 1 AND f.id IN ({2c}){$extrashit}
			ORDER BY date DESC LIMIT 0, 20", $loguserid, $user['id'], ForumsWithPermission('forum.viewforum')
);

$lastactivity = [];
if (NumRows($rRecent)) {
    while ($post = Fetch($rRecent)) {
        $lastactivity[] = MakePost($post, POST_PROFILE, ['threadlink'=>1, 'tid'=>$post['thread'], 'fid'=>$post['fid'], 'noreplylinks'=>1]);
    }
}

RenderTemplate('profile', [
    'username'     => htmlspecialchars($uname),
    'userlink'     => UserLink($user),
    'profileParts' => $profileParts,
    'commentField' => $commentField,
    'comments'     => $comments,
    'pagelinks'    => $pagelinks,
    'rpgstatus'    => $rpgstatus,
    'badgers'      => $badgers,
    'equipitems'   => $equipitems,
    'lastactivity' => $lastactivity
]);

if (!$mobileLayout && $acmlmboardLayout && $canShowPost) {
    if (trim($user['bio'])) {
        $previewPost['text'] = $user['bio'];
    } elseif (trim($user['postheader'])) {
        $previewPost['text'] = Settings::get('profilePreviewText');
    } else {
        $previewPost['text'] = '';
    }

    $previewPost['num'] = 0;
    $previewPost['id'] = 0;

    foreach ($user as $key => $value) {
        $previewPost['u_'.$key] = $value;
    }

    MakePost($previewPost, POST_SAMPLE);
}

$links = [];
if (HasPermission('admin.banusers')
    && $loguserid != $user['id']
    && $user['primarygroup'] != Settings::get('bannedGroup')
) {
    $links[actionLink('banhammer', $user['id'])] = ['icon' => 'hammer', 'text' => __('Ban user')];
}

if (HasPermission('user.editprofile') && $loguserid == $user['id']) {
    $links[actionLink('editprofile')] = ['icon' => 'pencil', 'text' => __('Edit my profile')];
} elseif (HasPermission('admin.editusers')) {
    $links[pageLink('editprofile', ['id' => $user['id'], 'name' => slugify($user['name'])])] = ['icon' => 'pencil', 'text' => format(__("Edit {0}'s profile"), htmlspecialchars($uname))];
}

if (HasPermission('admin.editusers')) {
    $links[actionlink('editperms', '', 'uid='.$user['id'])] = ['text' => __('Edit permissions')];
}

if ($loguserid == $user['id']) {
    $links[actionLink('private')] = ['icon' => 'eye-open', 'text' => __('Show my PMs')];
} elseif (HasPermission('admin.viewpms')) {
    $links[actionlink('private', $user['id'])] = ['icon' => 'eye-open', 'text' => format(__("Show {0}'s PMs"), htmlspecialchars($uname))];
}

if (HasPermission('user.sendpms')) {
    $links[actionLink('sendprivate', $user['id'])] = ['icon' => 'envelope', 'text' => format(__('Send {0} a PM'), htmlspecialchars($uname))];
}

if ($loguserid && isset($blockLink)) {
    $links[$blockLink] = $blocktext;
}

MakeCrumbs(
    [actionLink('memberlist') => __('Members'),
        pageLink('profile', [
            'id'   => $user['id'],
            'name' => slugify($user['name']),
        ]) => htmlspecialchars($uname),
    ], $links
);

$title = format(__('Profile for {0}'), htmlspecialchars($uname));

if ($user['bio']) {
    $metaStuff['description'] = htmlspecialchars(strip_html_tags($user['bio']));
} else {
    $metaStuff['description'] = format(__("This is {0}'s profile"), htmlspecialchars($uname));
}

function addField($name, $value, $permission='', $icon='') {
	global $temp, $mobileLayout;

	if (!isset($value) || empty($value)) {
		return;
	}

	if (!empty($permission) && !HasPermission($permission)) {
		return;
	}

	$iconame = '<i class="fa fa-'.$icon.'"></i> '.$name;
	if (!empty($temp[$name]) || ($mobileLayout && !empty($icon) && !empty($temp[$iconame]))) {
		if ($temp[$name] == $value) {
			return;
		}

		$temp[$name] .= '<br>'.$value;
	} else {
		if (!empty($icon) && $mobileLayout) {
			$name = '<i class="fa fa-'.$icon.'"></i> '.$name;
		}
		$temp[$name] = $value;
	}
}

function addToField($name, $value, $permission='', $icon='') {
	return addField($name, $value, $permission, $icon);
}

function addExtraToField($name, $value, $text) {
	if (isset($value) && !empty($value) && !empty($text)) {
		return addField($name, $text);
	}
}

function addCategory($name, $icon='') {
	global $temp, $profileParts, $mobileLayout;

	if (!empty($icon) && $mobileLayout) {
		$name = '<i class="fa fa-'.$icon.'"></i> '.$name;
	}

	if (count($temp) && !empty($name)) {
		$profileParts[$name] = $temp;
	}
}