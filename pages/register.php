<?php

//  AcmlmBoard XD - User account registration page
//  Access: Guests

if (!defined('BLARG')) {
    die();
}

$title = __('Register');
MakeCrumbs([actionLink('register') => __('Register')], [actionLink('login') => ['text' => __('Login')]]);

$sexes = [__('Male'), __('Female'), __('N/A')];

//If its using a VPN, alert the user of rules regarding them
if (IsProxy()) {
    Alert(__('It is against our rules to use a VPN for malicious purposes (like reregistering).'));
}

if ($loguserid && !$loguser['root']) {
    Kill(__('You don\'t need to make an account when you already have one'));
    Report('User '.$ipKnown['name'].' tried accessing the register page (logged in).');
}

$ipKnown = $db->row('users', ['lastip' => $_SERVER['REMOTE_ADDR']]);
if ($ipKnown) {
    $errors[] = __('You already have an account.');
    Report('User '.$ipKnown['name'].' tried accessing the register page (not logged in).');
}

if ($_POST['register']) {
    $name = trim($_POST['name']);
    $cname = str_replace(' ', '', strtolower($name));

    $email = trim($_POST['email']);
    $cemail = str_replace(' ', '', strtolower($email));

    $rUsers = Query('SELECT id, name, displayname, email FROM {users}');
    while ($user = Fetch($rUsers)) {
        $uid = $user['id'];

        $uname = trim(str_replace(' ', '', strtolower($user['name'])));
        if ($uname == $cname) {
            break;
        }
        $uname = trim(str_replace(' ', '', strtolower($user['displayname'])));
        if ($uname == $cname) {
            break;
        }

        $uemail = trim(str_replace(' ', '', strtolower($user['email'])));
        if ($uemail == $cemail) {
            break;
        }
    }

    $errors = [];

    // Go through our first protection: Basic robot check
    if ($_POST['likesCake']) {
        $errors[] = __('Robots are not allowed.');
        Report('Bot from IP '.$_SERVER['REMOTE_ADDR'].' tried to register');
    }

    //Go through our second list of registration errors: If any mandatory field has not been filled out yet
    if (!$cname) {
        $errors[] = __('Enter a username and try again.');
    }
    if (!$_POST['pass']) {
        $errors[] = __('Please enter a password & try again');
    }
    if (!$_POST['pass2']) {
        $errors[] = __("You'll need to enter your password twice");
    }
    if (!$cemail) {
        $errors[] = __('Please enter an email & try again.');
    }
    if (!$_POST['readFaq']) {
        $errors[] = __("In order to join our site, you'll need to agree to the Terms of Services.");
    }

    // Now check to make sure that the limits are passed
    if (strlen($cname) > 20) {
        $errors[] = __('The username is too long. The limit is 20');
    }
    if (strlen($cemail) > 60) {
        $errors[] = __('The email is too long. The limit is 60');
    }

    // 3rd step: User error
    if ($uname == $cname) {
        $errors[] = __('This user name is already taken. Please choose another.');
    }

    if (strlen($_POST['pass']) < 4 || strlen($_POST['pass2']) < 4) {
        $errors[] = __('Your password must be at least four characters long.');
    }

    if ($_POST['pass'] !== $_POST['pass2']) {
        $errors[] = __("The passwords you entered don't match.");
    }

    if (!filter_var($cemail, FILTER_VALIDATE_EMAIL)) {
        $errors[] = __('This email is not formatted correctly. Please try again.');
    }
    //Final step: Email checks
    if ($uemail == $cemail) {
        $errors[] = __('You already have an account');
        Report('User '.$uname." (#$uid) tried to reregister");
    }

    foreach ($emaildomainsblock as $evildomain) {
        if (strpos($cemail, $evildomain) !== false) {
            $errors[] = __('Registration failed. Try again later.');
            Report('User '.$cemail.' tried to register');
            break;
        }
    }

    if (count($errors)) {
        $errmsg = '<ul>';
        foreach ($errors as $error) {
            $errmsg .= '<li>'.$error.'</li>';
        }
        $errmsg .= '</ul>';
        Alert($errmsg, __('There are a few errors with your registration'));
    } else {
        $newsalt = Shake();
        $sha = doHash($_POST['pass'].SALT.$newsalt);
        // New user id generation, based on the 32bit integer limit rng
        $uid = mt_rand();
        $UIDexistcheck = FetchResult('SELECT * from {users} WHERE id = {0}', $uid);
        $checkifnuked = FetchResult("SELECT * from {nuked} WHERE id = {0} AND type = 'user'", $uid);
        while ($checkifnuked == $uid or $UIDexistcheck == $uid) {
            while ($UIDexistcheck == $uid) {
                $uid = mt_rand();
                $UIDexistcheck = FetchResult('SELECT * from {users} WHERE id = {0}', $uid);
            }
            $uid = mt_rand();
            $UIDexistcheck = FetchResult('SELECT * from {users} WHERE id = {0}', $uid);
            $checkifnuked = FetchResult("SELECT * from {nuked} WHERE id = {0} AND type = 'user'", $uid);
        }

        $db->insert('users', [
            'id'           => $uid,
            'name'         => $name,
            'password'     => $sha,
            'pss'          => $newsalt,
            'primarygroup' => Settings::get('defaultGroup'),
            'regdate'      => $db->time(),
            'lastip'       => $_SERVER['REMOTE_ADDR'],
            'email'        => $email,
            'sex'          => (int) $_POST['sex'],
            'theme'        => Settings::get('defaultTheme') || '',
            'layout'       => Settings::get('defaultLayout') || '',
            'rankset'      => Settings::get('defaultRankset') || '',
        ]);

        Report('New user: '.$_POST['name']." (#$uid) -> ".getServerDomainNoSlash().pageLink('profile', ['id' => $user['id'], 'name' => slugify($user['name'])]));

        $user = Fetch(Query('SELECT * FROM {users} WHERE id={0}', $uid));
        $user['rawpass'] = $_POST['pass']; // - Why do we need this?

        $bucket = 'newuser';
        include BOARD_ROOT.'lib/pluginloader.php';

        $rLogUser = Query('SELECT id, pss, password FROM {users} WHERE id <> {0}', $user['id']);
        $matches = [];

        while ($testuser = Fetch($rLogUser)) {
            $sha = doHash($_POST['pass'].SALT.$testuser['pss']);
            if ($testuser['password'] === $sha) {
                $matches[] = $testuser['id'];
            }
        }

        if (count($matches) > 0) {
            Query('INSERT INTO {passmatches} (date,ip,user,matches) VALUES (UNIX_TIMESTAMP(),{0},{1},{2})', $_SERVER['REMOTE_ADDR'], $user['id'], implode(',', $matches));
        }

        // mark threads older than 15min as read
        Query('INSERT INTO {threadsread} (id,thread,date) SELECT {0}, id, {1} FROM {threads} WHERE lastpostdate<={2} ON DUPLICATE KEY UPDATE date={1}', $uid, time(), time() - 900);

        if ($_POST['autologin']) {
            $sessionID = Shake();
            setcookie('logsession', $sessionID, 0, URL_ROOT, '', false, true);
            $db->insert('sessions', ['id' => doHash($sessionID.SALT), 'user' => $user['id'], 'autoexpire' => 0]);
            newRedir(pageLink('profile', ['id' => $user['id'], 'name' => slugify($user['name'])]));
        } else {
            newRedir(pageLink('login'));
        }
    }
} else {
    $_POST['name'] = '';
    $_POST['email'] = '';
    $_POST['sex'] = 2;
    $_POST['autologin'] = 0;
}

$fields = [
    'username'  => '<input class="form-control" placeholder="'.__('User Name').'" type="text" name="name" maxlength=20 size=24 value="'.htmlspecialchars($_POST['name']).'" class="required">',
    'password'  => '<input class="form-control" placeholder="'.__('Password').'" type="password" name="pass" size=24 class="required">',
    'password2' => '<input class="form-control" placeholder="'.__('Confirm your Password').'" type="password" name="pass2" size=24 class="required">',
    'email'     => '<input type="email" class="form-control" placeholder="'.__('Email adress').'" name="email" value="'.htmlspecialchars($_POST['email']).'" maxlength="60" size=24>',
    'sex'       => MakeOptions('sex', $_POST['sex'], $sexes),
    'readfaq'   => checkbox('readFaq', format(__('I agree to the {0}Terms of Service{1}'), '<a href="'.actionLink('faq').'">', '</a>')),
    'autologin' => checkbox('autologin', __('Log in afterwards'), $_POST['autologin']),

    'btnRegister' => '<input type="submit" class="btn btn-primary" name="register" value="'.__('Register').'">',
];

echo '<form action="'.htmlentities(actionLink('register')).'" method="post">';

RenderTemplate('form_register', ['fields' => $fields]);

echo '<span style="display: none;"><input type="checkbox" name="likesCake"> I am a robot</span></form>';

function MakeOptions($fieldName, $checkedIndex, $choicesList)
{
    $checks[$checkedIndex] = ' checked="checked"';
    foreach ($choicesList as $key=>$val) {
        $result .= format(
            '
				<div class="form-check-inline">
					<label class="form-check-label">
						<input type="radio" class="form-check-input" name="{1}" value="{0}"{2}>
						{3}
					</label>
				</div>', $key, $fieldName, $checks[$key], $val
        );
    }

    return $result;
}

function IsProxy()
{
    if ($_SERVER['HTTP_X_FORWARDED_FOR'] && $_SERVER['HTTP_X_FORWARDED_FOR'] != $_SERVER['REMOTE_ADDR']) {
        return true;
    }

    //init and set cURL options
    $ipintel = QueryURL('http://check.getipintel.net/check.php?ip='.$_SERVER['REMOTE_ADDR'].'&flags=m');
    $banOnProbability = 0.99; //if getIPIntel returns a value higher than this, function returns true, set to 0.99 by default

    if ($ipintel > $banOnProbability) {
        return true;
    }

    //Stop-Forum-Spam Checks
    $SFSLink = 'http://api.stopforumspam.org/api?ip='.$_SERVER['REMOTE_ADDR'].($_POST['email'] ? '&email='.$_POST['email'] : '').'&json';

    $SFSpage = file_get_contents($SFSLink.'&notorexit');
    if ($SFSpage) {
        $a = json_decode($SFSpage);
        if ($a->ip->torexit == 1) {
            return true;
        }
    }

    $SFSpage = file_get_contents($SFSLink);
    if ($SFSpage) {
        $a = json_decode($SFSpage);
        if ($a->ip->appears == 1) {
            return true;
        }
    }

    return false;
}
