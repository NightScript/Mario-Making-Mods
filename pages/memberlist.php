<?php

//  AcmlmBoard XD - Member list page
//  Access: all
if (!defined('BLARG')) {
    die();
}

$title = __('Member list');

if (!isset($_GET['group'])) {
    $_GET['group'] = 'none';
} elseif ($_GET['group'] !== 'staff' && $_GET['group'] !== 'none') {
    $_GET['group'] = (int) $_GET['group'];
}

$allgroups = [];
$allgroups['none'] = __('(any)');
$g = Query('SELECT id,name,type FROM {usergroups} WHERE display>-1 ORDER BY type, rank');

$allgroups[__('Primary')] = null;
$s = false;
while ($group = Fetch($g)) {
    if (!$s && $group['type'] == 1) {
        $s = true;
        $allgroups['staff'] = __('(all staff)');
        $allgroups[__('Secondary')] = null;
    }

    $allgroups[$group['id']] = $group['name'];
}

if (!$s) {
    $allgroups['staff'] = __('(all staff)');
    $s = true;
}

$lastviewlist = [];
$spans = ['', 60, 300, 900, 3600, 86400, 259200, 604800];
foreach ($spans as $span) {
	if ($span == '') {
		$lastviewlist[''] = __('All time');
	} else {
		$lastviewlist[$span] = timeunits($span);
	}
}

MakeCrumbs([actionLink('memberlist') => __('Member list')]);

$fields = [
    'sortBy' => makeSelect(
        'sort', [
            ''         => __('Post count'),
            'exp'      => __('EXP'),
            'name'     => __('Name'),
            'reg'      => __('Registration date'),
			'lastview' => __('Last View')
        ]
    ),
    'order' => makeSelect(
        'order', [
            'desc' => __('Descending'),
            'asc'  => __('Ascending'),
        ]
    ),
	'lastview'  => makeSelect('lastactivity', $lastviewlist),
    'group'     => makeSelect('group', $allgroups),
    'name'      => '<input type="text" name="name" id="name" placeholder="'.__('Find in name').'" class="form-control" maxlength=20 value="'.htmlspecialchars($_GET['name']).'">',
    'btnSearch' => '<input class="btn btn-primary" type="submit" value="'.__('Search').'">',
];

echo getForm('memberlist');

RenderTemplate('form_memberlist', ['fields' => $fields]);

echo '
	</form>';

$getArgs = [];

$tpp = $loguser['threadsperpage'];
if ($tpp < 1) {
    $tpp = 50;
}

if (isset($_GET['from'])) {
    $from = (int) $_GET['from'];
} else {
    $from = 0;
}

if (isset($_GET['order'])) {
    $dir = $_GET['order'];
    if ($dir != 'asc' && $dir != 'desc') {
        unset($dir);
    } else {
        $getArgs[] = 'order='.$dir;
    }
}

$sort = $_GET['sort'];
if (!in_array($sort, ['', 'exp', 'name', 'reg', 'lastview'])) {
    unset($sort);
}

if ($sort) {
    $getArgs[] = 'sort='.$sort;
}

$pow = null;
if ($_GET['group'] !== 'none') {
    if ($_GET['group'] === 'staff') {
        $pow = [];
        foreach ($usergroups as $g) {
            if ($g['display'] == 1) {
                $pow[] = $g['id'];
            }
        }
    } else {
        $pow = (int) $_GET['group'];
    }

    if ($pow !== null) {
        $getArgs[] = 'group='.$pow;
    }
}

$order = '';

switch ($sort) {
	case 'exp':
		$order = 'exp '.(isset($dir) ? $dir : 'asc');
		break;
	case 'name':
		$order = 'name '.(isset($dir) ? $dir : 'asc');
		break;
	case 'reg':
		$order = 'regdate '.(isset($dir) ? $dir : 'desc');
		break;
	case 'lastview':
		$order = 'lastactivity '.(isset($dir) ? $dir : 'asc');
		break;
	default:
		$order = 'posts '.(isset($dir) ? $dir : 'desc');
}

$where = '1';

if ($pow !== null) {
    if (is_array($pow)) {
        $where .= ' AND primarygroup IN ({2c})';
    } elseif ($usergroups[$pow]['type'] == 0) {
        $where .= ' AND primarygroup={2}';
    } else {
        $where .= ' AND (SELECT COUNT(*) FROM {secondarygroups} sg WHERE sg.userid=id AND sg.groupid={2})>0';
    }
}

$query = $_GET['name'];

if (!empty($query)) {
    $where .= ' AND (name like {3} or displayname like {3})';
    $getArgs[] = 'name='.urlencode($query);
}

if (!empty($_GET['lastactivity'])) {
	$where .= ' AND lastactivity > '.(time() - $_GET['lastactivity']);
	$getArgs[] = 'lastactivity='.urlencode($_GET['lastview']);
}

$numUsers = FetchResult('SELECT count(*) FROM {users} WHERE '.$where, null, null, $pow, "%{$query}%");
$rUsers = Query('SELECT *, '.sqlexp().' FROM {users} WHERE '.$where.' order by '.$order.', name asc limit {0u},{1u}', $from, $tpp, $pow, "%{$query}%");

$users = [];
$i = $from + 1;
while ($user = Fetch($rUsers)) {
    $udata = [];

    $daysKnown = (time() - $user['regdate']) / 86400;
    $udata['average'] = sprintf('%1.02f', $user['posts'] / $daysKnown);

    if ($user['picture']) {
        $udata['avatar'] = htmlspecialchars(str_replace('$root/', DATA_URL, $user['picture']));
    }

    $udata['num'] = $i;

    $udata['link'] = UserLink($user, 'false');
    $udata['posts'] = ($user['posts'] == 0 ? $user['posts'] : pageLinkTag($user['posts'], 'listposts', ['id' => $user['id'], 'name' => $user['name']]));
    $udata['id'] = $user['id'];
    $udata['birthday'] = ($user['birthday'] ? cdate('M jS', $user['birthday']) : '');
    $udata['regdate'] = cdate('M jS Y', $user['regdate']);

    if ($poster['lastactivity'] > time() - 300) {
        $udata['isonline'] = '<img src="/img/online.png" title="User is Online" alt="User is Online">';
    } else {
        $udata['isonline'] = '<img src="/img/offline.png" title="User is Offline" alt="User is Offline">';
    }
	
	$udata['title'] = htmlspecialchars($user['title']);
	$udata['group'] = htmlspecialchars($usergroups[$user['primarygroup']]['title']);
	if (!empty($user['location'])) {
        $udata['from'] = htmlspecialchars($user['location']);
    }

    if (HasPermission('admin.banusers') && $loguserid != $user['id'] && $user['primarygroup'] != Settings::get('bannedGroup')) {
        $udata['quicklinks'][] = ['icon' => 'ban', 'text' => __("Ban user"), 'link' => actionLink('banhammer', $user['id'])];
    }

    if (HasPermission('user.editprofile') && $loguserid == $user['id']) {
        $udata['quicklinks'][] = ['icon' => 'pencil', 'text' => __('Edit my profile'), 'link' => actionLink('editprofile')];
    } elseif (HasPermission('admin.editusers')) {
        $udata['quicklinks'][] = ['icon' => 'pencil', 'text' => __('Edit user'), 'link' => actionLink('editprofile', $user['id'])];
    }

	$udata['quicklinks'][] = ['icon' => 'copy', 'text' => format(__("List {0}'s posts"), htmlentities($user['name'])), 'link' => pageLink('listposts', ['id' => $user['id'], 'name' => slugify($user['name'])])];
    $udata['quicklinks'][] = ['icon' => 'list', 'text' => format(__("List {0}'s threads"), htmlentities($user['name'])), 'link' => pageLink('listthreads', ['id' => $user['id'], 'name' => slugify($user['name'])])];

    if ($loguserid == $user['id']) {
        $udata['quicklinks'][] = ['icon' => 'eye', 'text' => __('Show my PMs'), 'link' => actionLink('private')];
    } elseif (HasPermission('admin.viewpms')) {
        $udata['quicklinks'][] = ['icon' => 'eye', 'text' => __('Show PMs'), 'link' => actionLink('private', '', 'user='.$user['id'])];
    }

    if (HasPermission('user.sendpms')) {
        $udata['quicklinks'][] = ['icon' => 'envelope', 'text' => __('Send PM'), 'link' => actionLink('sendprivate', '', 'uid='.$user['id'])];
    }

    $users[] = $udata;
    $i++;
}

$getArgs[] = 'from=';

$pagelinks = PageLinks(actionLink('memberlist', '', implode('&', $getArgs)), $tpp, $from, $numUsers);

RenderTemplate('memberlist', ['pagelinks' => $pagelinks, 'numUsers' => $numUsers, 'users' => $users]);

function makeSelect($name, $options)
{
    $result = '<select name="'.$name.'" id="'.$name.'" class="browser-default custom-select">';

    $i = 0;
    $hasgroups = false;
    foreach ($options as $key => $value) {
        if ($value == null) {
            if ($hasgroups) {
                $result .= "\n\t</optgroup>";
            }
            $result .= "\n\t<optgroup label=\"".$key.'">';
            $hasgroups = true;
            continue;
        }

        $result .= "\n\t<option".($key == $_GET[$name] ? ' selected="selected"' : '').' value="'.$key.'">'.$value.'</option>';
    }

    if ($hasgroups) {
        $result .= "\n\t</optgroup>";
    }
    $result .= "\n</select>";

    return $result;
}
