<?php
if (!defined('BLARG')) die();
if (!$loguserid) Kill("You must be signed in to view the Administration panel.");

$title = __('Administration');
MakeCrumbs([actionLink('admin') => __('Admin')]);

if (function_exists('curl_init')) {
    $protstatus = __('Enabled (using cURL)');
} elseif (ini_get('allow_url_fopen')) {
    $protstatus = __('Enabled (using fopen)');
} else {
    $protstatus = __('Disabled');
}

$adminInfo = [];
$adminInfo[__('Proxy protection')] = $protstatus;
$adminInfo[__('Last viewcount milestone')] = $misc['milestone'];

$adminLinks = [];

adminLink(actionLink('recalc'), __('Recalculate statistics'), 'root');
adminLink(actionLink('lkb'), __('Last Known Browser'), 'admin.ipsearch');
adminLink(actionLink('ipbans'), __('Manage IP bans'), 'admin.manageipbans');
adminLink(actionLink('editfora'), __('Manage forum list'), 'admin.editforums');
adminLink(actionLink('pluginmanager'), __('Manage plugins'), 'admin.editsettings');
adminLink(actionLink('editsettings'), __('Edit settings'), 'admin.editsettings');
adminLink(actionLink('editsettings', '', 'field=homepageText'), __('Edit home page'), 'admin.editsettings');
adminLink(actionLink('editsettings', '', 'field=faqText'), __('Edit FAQ'), 'admin.editsettings');
adminLink(actionLink('editsmilies'), __('Edit smilies'), 'admin.editsmilies');
adminLink(actionLink('optimize'), __('Optimize tables'), 'root');
adminLink(actionLink('log'), __('View log'), 'admin.viewlog');
adminLink(actionLink('reregs'), __('Rereg radar'), 'admin.ipsearch');

$bucket = 'adminpanel';
require BOARD_ROOT.'lib/pluginloader.php';

if (sizeof($adminLinks) == 0) {
	Kill("You must have administrator permissions in order to view this page.");
}

RenderTemplate('admin/panel', ['adminInfo' => $adminInfo, 'adminLinks' => $adminLinks]);

function adminLink($link, $text, $permission)
{
    global $adminLinks, $loguser;

    if ($permission == 'root') {
        if ($loguser['root']) {
            $adminLinks[$link] = $text;
        }
    } else {
        if (HasPermission($permission)) {
            $adminLinks[$link] = $text;
        }
    }
}
